#!/bin/sh
cp package.json dist/package.json
cd dist
npm publish --access public
cd ..
rm dist/package.json