const CopyWebpackPlugin = require('copy-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path')

const paths = require('./paths')

module.exports = {
  // Where webpack looks to start building the bundle
  entry: [paths.src + '/demo/index.ts'],

  // Where webpack outputs the assets and bundles
  output: {
    path: paths.build,
    filename: '[name].bundle.js',
    publicPath: '/',
  },

  // Customize the webpack build process
  plugins: [
    // Copies files from target to destination folder
    new CopyWebpackPlugin({
      patterns: [
        {
          from: paths.demoAssets,
          to: 'assets',
          globOptions: {
            ignore: ['*.DS_Store'],
          },
          noErrorOnMissing: true,
        },
      ],
    }),

    new CopyWebpackPlugin({
      patterns: [
        {
          context: "node_modules/@webcomponents/webcomponentsjs",
          from: "**/*.js",
          to: "webcomponents",
          globOptions: {
            ignore: ['*.DS_Store'],
          },
          noErrorOnMissing: true,
        },
      ],
    }),

    // Generates an HTML file from a template
    // Generates deprecation warning: https://github.com/jantimon/html-webpack-plugin/issues/1501
    new HtmlWebpackPlugin({
      title: 'KODE4 UI Demo',
      htmlBase: 'http://localhost:8080',
      // favicon: paths.src + '/images/favicon.png',
      template: paths.src + '/demo/index.html', // template file
      filename: 'index.html', // output file,
      ignoreCustomComments: [/^!/],
    }),
  ],

  // Determine how modules within the project are treated
  module: {
    rules: [
      // // JavaScript: Use Babel to transpile JavaScript files
      // { test: /\.js$/, use: ['babel-loader'] },

      {
        test: /\.tsx?$/, use: "ts-loader", exclude: /node_modules/
      },

      // Images: Copy image files to build folder
      { test: /\.(?:ico|gif|png|jpg|jpeg)$/i, type: 'asset/resource' },

      // Fonts and SVGs: Inline files
      { test: /\.(woff(2)?|eot|ttf|otf|svg|)$/, type: 'asset/inline' },
    ],
  },

  resolve: {
    modules: [paths.src, 'node_modules'],
    extensions: ['.ts', '.tsx', '.js', '.jsx', '.json'],
    alias: {
      '@': paths.src,
      assets: paths.public,
      'lit-html/lib/shady-render.js': path.resolve(__dirname, './node_modules/lit-html/lit-html.js')
    },
  },
}
