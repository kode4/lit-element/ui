import { LitElement, PropertyValues } from "lit";
import Kode4Card from "./kode4-card";
export declare const kode4Dialogs: Map<string, Kode4Dialog>;
export default class Kode4Dialog extends LitElement {
    protected body?: Kode4Card;
    private name?;
    private open;
    private toggleCssClass;
    set left(value: boolean);
    get left(): boolean;
    set right(value: boolean);
    get right(): boolean;
    set top(value: boolean);
    get top(): boolean;
    set bottom(value: boolean);
    get bottom(): boolean;
    transparent: boolean;
    modal: boolean;
    private modalResolve?;
    private modalReject?;
    show(): Promise<boolean>;
    protected onConfirm(e: MouseEvent): void;
    protected onCancel(e: MouseEvent): void;
    protected onError(e: MouseEvent): void;
    hide(): void;
    private handleBackdropClick;
    private headerItems;
    private mainItems;
    private footerItems;
    private resizeContentObserver;
    get contentWidth(): number;
    get contentHeight(): number;
    protected firstUpdated(_changedProperties: PropertyValues): void;
    connectedCallback(): void;
    disconnectedCallback(): void;
    handleSlotchange(): void;
    __render(): import("lit-html").TemplateResult<1>;
    render(): import("lit-html").TemplateResult<1>;
    static styles: import("lit").CSSResult[];
    _render(): import("lit-html").TemplateResult<1>;
}
