import { IListStateObserver, ListStateHandler } from "./ListStateHandler";
import { IApplicationPersistenceService } from "@kode4/core/persistence/ApplicationPersistenceService";
import { ConvertableDynamicCustomParams } from "@kode4/core/http/HttpService";
export interface IListObserver {
    onListChange(list: Array<unknown>): void;
    onStateChange?(state: Map<string, any>): void;
}
export interface IListRequest<T> {
    total: number;
    list: Array<T>;
}
export interface ISearchParams {
    [key: string]: any;
}
export declare abstract class ListHandler<T> implements IListStateObserver {
    currentHash?: string;
    private observers;
    protected list: Array<any>;
    private lsh;
    storage?: IApplicationPersistenceService;
    storageKey?: string;
    constructor(params: {
        listStateHandler: ListStateHandler;
        storage?: IApplicationPersistenceService;
        storageKey?: string;
        observer?: IListObserver;
    });
    loadList(forceReload?: boolean): Promise<void>;
    private debouncedLoadList;
    loadItem(searchParams: ISearchParams): Promise<T>;
    loadItemAndUpdateInMemory(searchParams: ISearchParams): Promise<void>;
    updateItem(searchParams: ISearchParams, newData: ConvertableDynamicCustomParams, refreshList?: boolean): Promise<void>;
    deleteItem(searchParams: ISearchParams, refreshList?: boolean): Promise<void>;
    observe(observer: IListObserver): void;
    unobserve(observer?: IListObserver): void;
    private fireListChanged;
    onFilterChange(): void;
    onOrderChange(): void;
    onPaginationChange(): void;
    onRestore(): void;
    onReset(): void;
    onRefresh(forceReload?: boolean): void;
    protected itemMatchesSearchParams(item: T, searchParams: ISearchParams): boolean;
    findItem(searchParams: ISearchParams): T | undefined;
    protected updateItemInMemory(searchParams: ISearchParams, newItem: T): void;
    protected deleteItemInMemory(searchParams: ISearchParams): void;
    protected abstract performLoadList(params: {}, filters: Map<string, any>): Promise<IListRequest<T>>;
    protected abstract performLoadItem(searchParams: ISearchParams): Promise<T>;
    protected abstract performUpdateItem(searchParams: ISearchParams, newData: ConvertableDynamicCustomParams): Promise<any>;
    protected abstract performDeleteItem(searchParams: ISearchParams): Promise<any>;
}
