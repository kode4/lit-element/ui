import { __decorate } from "tslib";
import { LitElement, html, css } from "lit";
import { customElement, property, query, state } from "lit/decorators.js";
import kode4CssTheme from "./kode4-css";
import './kode4-backdrop';
import './kode4-list';
export const kode4Menus = new Map();
let Kode4Menu = class Kode4Menu extends LitElement {
    constructor() {
        super(...arguments);
        this.isOpen = false;
        this.transparent = false;
        this._backdrop = false;
        this.floating = false;
        this.bound = false;
        this.boundToParent = false;
        this.cover = false;
        this.handleParentClick = (e) => {
            if (e.defaultPrevented) {
                return;
            }
            if (this.isOpen) {
                return;
            }
            e.preventDefault();
            this.show({
                x: e.pageX,
                y: e.pageY,
            });
        };
    }
    set open(value) {
        if (value && !this.isOpen) {
            this.show();
        }
    }
    toggleCssClass(show, cssClass) {
        show ? this.classList.add(cssClass) : this.classList.remove(cssClass);
    }
    set visible(value) {
        this.toggleCssClass(value, 'kode4-menu-visible');
    }
    get visible() {
        return this.classList.contains('kode4-menu-visible');
    }
    get backdrop() {
        return this._backdrop;
    }
    set backdrop(value) {
        this.toggleCssClass(value, 'kode4-list-backdrop');
        this._backdrop = value;
    }
    set parent(value) {
        // this.toggleCssClass(value, 'kode4-list-parent');
        this.boundToParent = value;
    }
    set left(value) {
        this.toggleCssClass(value, 'kode4-list-align-left');
    }
    get left() {
        return this.classList.contains('kode4-list-align-left');
    }
    set right(value) {
        this.toggleCssClass(value, 'kode4-list-align-right');
    }
    get right() {
        return this.classList.contains('kode4-list-align-right');
    }
    set top(value) {
        this.toggleCssClass(value, 'kode4-list-align-top');
    }
    get top() {
        return this.classList.contains('kode4-list-align-top');
    }
    set bottom(value) {
        this.toggleCssClass(value, 'kode4-list-align-bottom');
    }
    get bottom() {
        return this.classList.contains('kode4-list-align-bottom');
    }
    set stretch(value) {
        this.toggleCssClass(value, 'kode4-list-align-stretch-x');
    }
    get stretch() {
        return this.classList.contains('kode4-list-align-stretch-x');
    }
    getCallerParams() {
        return this.callerParams;
    }
    onShow(e, preventDefault = true) {
        if (e.defaultPrevented) {
            return;
        }
        if (preventDefault) {
            e.preventDefault();
        }
        if (e instanceof MouseEvent) {
            this.show({
                x: e.pageX,
                y: e.pageY,
            });
        }
        else {
            this.show();
        }
    }
    show(params) {
        if (params === null || params === void 0 ? void 0 : params.callerParams) {
            this.callerParams = params.callerParams;
        }
        this.visible = true;
        this.isOpen = true;
        if (this.floating) {
            this.findFloatingPosition({
                x: params === null || params === void 0 ? void 0 : params.x,
                y: params === null || params === void 0 ? void 0 : params.y,
            });
        }
        if (this.bound || this.boundToParent) {
            this.findBoundToParentPosition((params === null || params === void 0 ? void 0 : params.alignToElement) || this.parentElement);
        }
        this.requestUpdate();
    }
    findFloatingPosition(pos) {
        const screenW = window.innerWidth;
        const screenH = window.innerHeight;
        const menuBox = this.menu.getBoundingClientRect();
        let desiredX = (pos === null || pos === void 0 ? void 0 : pos.x) || 10;
        let desiredY = (pos === null || pos === void 0 ? void 0 : pos.y) || 10;
        if (desiredX + menuBox.width > screenW) {
            desiredX = screenW - menuBox.width;
        }
        if (desiredY + menuBox.height > screenH) {
            desiredY = screenH - menuBox.height;
        }
        this.x = `${Math.max(desiredX, 0)}px`;
        this.y = `${Math.max(desiredY, 0)}px`;
    }
    findBoundToParentPosition(alignToElement) {
        const screenW = window.innerWidth;
        const screenH = window.innerHeight;
        const parentBox = alignToElement.getBoundingClientRect();
        const menuBox = this.menu.getBoundingClientRect();
        let desiredX;
        let desiredY;
        let desiredWidth = 0;
        // const correctionX = this.cover ? parentBox.width : 0;
        const correctionY = this.cover ? parentBox.height : 0;
        // align y
        switch (true) {
            case this.top:
                desiredY = parentBox.y - menuBox.height + correctionY;
                if (desiredY < 0) {
                    desiredY = parentBox.y + parentBox.height - correctionY;
                    if (desiredY + menuBox.height > screenH) {
                        desiredY = 0;
                    }
                }
                break;
            default:
            case this.bottom:
                desiredY = parentBox.y + parentBox.height - correctionY;
                if (desiredY + menuBox.height > screenH) {
                    desiredY = parentBox.y - menuBox.height + correctionY;
                    if (desiredY < 0) {
                        desiredY = screenH - menuBox.height;
                    }
                }
                break;
        }
        // align x
        switch (true) {
            case menuBox.width > parentBox.width && this.left:
                desiredX = parentBox.x;
                if (desiredX + menuBox.width > screenW) {
                    desiredX = parentBox.x + parentBox.width - menuBox.width;
                    if (desiredX < 0) {
                        desiredX = screenW - menuBox.width;
                    }
                }
                break;
            case menuBox.width > parentBox.width && this.right:
                desiredX = parentBox.x + parentBox.width - menuBox.width;
                if (desiredX < 0) {
                    desiredX = parentBox.x;
                    if (desiredX + menuBox.width > screenW) {
                        desiredX = 0;
                    }
                }
                break;
            default:
            case this.stretch:
                desiredX = parentBox.x;
                if (menuBox.width < parentBox.width) {
                    desiredWidth = parentBox.width;
                }
                break;
        }
        this.x = `${Math.max(desiredX, 0)}px`;
        this.y = `${Math.max(desiredY, 0)}px`;
        if (desiredWidth > 0) {
            this.minWidth = `${desiredWidth}px`;
        }
    }
    hide() {
        this.visible = false;
        this.isOpen = false;
        this.callerParams = undefined;
        this.requestUpdate();
    }
    handleBackdropClick(e) {
        e.stopPropagation();
        e.preventDefault();
        this.hide();
    }
    /**
     * Automatically hide menu if a menu-item is clicked an defaultPrevented.
     *
     * @param e
     * @private
     */
    handleListClick(e) {
        if (e.defaultPrevented) {
            this.hide();
        }
    }
    connectedCallback() {
        if (this.name) {
            kode4Menus.set(this.name, this);
        }
        if (this.boundToParent) {
            this.parentElement.addEventListener('click', this.handleParentClick);
        }
        super.connectedCallback();
    }
    disconnectedCallback() {
        if (this.boundToParent) {
            this.parentElement.removeEventListener('click', this.handleParentClick);
        }
        if (this.name && kode4Menus.has(this.name)) {
            kode4Menus.delete(this.name);
        }
        super.disconnectedCallback();
    }
    // style="top: ${this.y || 'auto'}; left: ${this.x || 'auto'}; ${this.minWidth ? `min-width: ${this.minWidth}` : undefined}; ${this.width ? `width: ${this.width};` : undefined}"
    render() {
        return html `
            ${this.bound || this.boundToParent || this.floating ? html `
                <kode4-backdrop ?visible="${this.isOpen}" @click="${this.handleBackdropClick}" @contextmenu="${this.handleBackdropClick}" ?transparent="${this.transparent}"></kode4-backdrop>
            ` : undefined}
            <kode4-list style="left: ${this.x || 'auto'}; top: ${this.y || 'auto'}; ${this.minWidth ? `min-width: ${this.minWidth};` : undefined} ${this.width ? `width: ${this.width};` : undefined}" inline @click="${this.handleListClick}">
                <slot></slot>
            </kode4-list>
        `;
    }
};
// <kode4-list style=${styleMap(this.listStyles)} inline @click="${this.handleListClick}">
Kode4Menu.styles = [
    kode4CssTheme,
    css `
            * {
                box-sizing: border-box;
            }
            
            :host  {
                padding: 0;
                margin: 0;
                border: none 0;
            }

            :host(:not(.kode4-menu-visible)) {
                display: none;
            }
            
            :host kode4-list,
            :host([floating]) kode4-list,
            :host([bound]) kode4-list,
            :host([parent]) kode4-list {
                position: fixed;
                z-index: 10100;
            }
        `,
];
__decorate([
    property({ type: String, attribute: 'name' })
], Kode4Menu.prototype, "name", void 0);
__decorate([
    state()
], Kode4Menu.prototype, "isOpen", void 0);
__decorate([
    property({ type: Boolean, attribute: 'open' })
], Kode4Menu.prototype, "open", null);
__decorate([
    property({ type: Boolean, attribute: true })
], Kode4Menu.prototype, "transparent", void 0);
__decorate([
    property({ type: Boolean, attribute: 'visible' })
], Kode4Menu.prototype, "visible", null);
__decorate([
    query('kode4-list')
], Kode4Menu.prototype, "menu", void 0);
__decorate([
    property({ type: Boolean, attribute: true })
], Kode4Menu.prototype, "_backdrop", void 0);
__decorate([
    property({ type: Boolean, attribute: true })
], Kode4Menu.prototype, "backdrop", null);
__decorate([
    query('.backdrop')
], Kode4Menu.prototype, "backdropElement", void 0);
__decorate([
    property({ type: Boolean, attribute: 'floating' })
], Kode4Menu.prototype, "floating", void 0);
__decorate([
    property({ type: Boolean, attribute: 'bound' })
], Kode4Menu.prototype, "bound", void 0);
__decorate([
    state()
], Kode4Menu.prototype, "boundToParent", void 0);
__decorate([
    property({ type: Boolean, attribute: 'parent' })
], Kode4Menu.prototype, "parent", null);
__decorate([
    property({ type: Boolean, attribute: 'left' })
], Kode4Menu.prototype, "left", null);
__decorate([
    property({ type: Boolean, attribute: 'right' })
], Kode4Menu.prototype, "right", null);
__decorate([
    property({ type: Boolean, attribute: 'top' })
], Kode4Menu.prototype, "top", null);
__decorate([
    property({ type: Boolean, attribute: 'bottom' })
], Kode4Menu.prototype, "bottom", null);
__decorate([
    property({ type: Boolean, attribute: 'stretch' })
], Kode4Menu.prototype, "stretch", null);
__decorate([
    property({ type: Boolean, attribute: 'cover' })
], Kode4Menu.prototype, "cover", void 0);
__decorate([
    state()
], Kode4Menu.prototype, "x", void 0);
__decorate([
    state()
], Kode4Menu.prototype, "y", void 0);
__decorate([
    state()
], Kode4Menu.prototype, "minWidth", void 0);
__decorate([
    property({ type: String, attribute: 'w' })
], Kode4Menu.prototype, "width", void 0);
Kode4Menu = __decorate([
    customElement('kode4-menu')
], Kode4Menu);
export default Kode4Menu;
