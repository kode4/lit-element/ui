import { LitElement, HTMLTemplateResult } from "lit";
export default class Kode4Tabs extends LitElement {
    private tabContentElements?;
    spacing: boolean;
    stretch: boolean;
    tabs: Map<string, HTMLTemplateResult>;
    selected?: string;
    changeTo: string;
    private get selectedTab();
    selectTab(tabname: string): void;
    handleSlotchange(): void;
    render(): import("lit-html").TemplateResult<1>;
    static styles: import("lit").CSSResult[];
}
