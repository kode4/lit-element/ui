import { LitElement, TemplateResult } from "lit";
export interface Kode4GrowlMessage {
    id?: string;
    message: TemplateResult | string;
    color?: string;
    timeout?: number;
    pos?: string;
}
export declare const dispathGrowlAlert: (el: HTMLElement, position: string, message: Kode4GrowlMessage) => void;
export interface IKode4GrowlMessengerObserver {
    onGrowlAlert(message: Kode4GrowlMessage): void;
}
export declare class Kode4GrowlMessenger {
    private observers;
    observe(observer: IKode4GrowlMessengerObserver): void;
    unobserve(observer?: IKode4GrowlMessengerObserver): void;
    alert(message: Kode4GrowlMessage): void;
    private fireGrowlAlert;
}
export declare const getGrowlMessenger: () => Kode4GrowlMessenger;
export default class Kode4Growl extends LitElement {
    messages: Array<Kode4GrowlMessage>;
    horizontalAlign: string;
    verticalAlign: string;
    alert(growlMessage: Kode4GrowlMessage): void;
    closeMessage(id?: String): void;
    render(): TemplateResult<1>;
    static styles: import("lit").CSSResult[];
}
