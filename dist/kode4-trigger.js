import { __decorate } from "tslib";
import { LitElement, html } from "lit";
import { customElement, property } from "lit/decorators.js";
import './kode4-backdrop';
import './kode4-list';
import { kode4Menus } from "./kode4-menu";
import { kode4Dialogs } from "./kode4-dialog";
import { kode4Drawers } from "./kode4-drawer";
let Kode4Trigger = class Kode4Trigger extends LitElement {
    constructor() {
        super(...arguments);
        this.mode = 'toggle';
        // @state()
        this.boundToParent = false;
        // @property({ type: Boolean, attribute: 'parent' })
        // public set parent(value:boolean) {
        //     this.boundToParent = value;
        // }
        this.handleParentClick = (e) => {
            if (this.menu && kode4Menus.has(this.menu)) {
                e.preventDefault();
                const menu = kode4Menus.get(this.menu);
                menu.show({
                    alignToElement: this.parentElement,
                    callerParams: this.callerParams,
                });
            }
            else if (this.drawer && kode4Drawers.has(this.drawer)) {
                e.preventDefault();
                const drawer = kode4Drawers.get(this.drawer);
                drawer.show();
                // drawer.open = true;
            }
            else if (this.dialog && kode4Dialogs.has(this.dialog)) {
                e.preventDefault();
                const dialog = kode4Dialogs.get(this.dialog);
                dialog.show();
            }
            else {
                console.log('[Kode4Trigger] unable to connect');
            }
        };
    }
    connectedCallback() {
        if (this.boundToParent) {
            this.parentElement.addEventListener('click', this.handleParentClick);
        }
        super.connectedCallback();
    }
    disconnectedCallback() {
        if (this.boundToParent) {
            this.parentElement.removeEventListener('click', this.handleParentClick);
        }
        super.disconnectedCallback();
    }
    render() {
        return html `
        `;
    }
};
__decorate([
    property({ type: String, attribute: true })
], Kode4Trigger.prototype, "menu", void 0);
__decorate([
    property({ type: String, attribute: true })
], Kode4Trigger.prototype, "drawer", void 0);
__decorate([
    property({ type: String, attribute: true })
], Kode4Trigger.prototype, "dialog", void 0);
__decorate([
    property({ type: String, attribute: true })
], Kode4Trigger.prototype, "mode", void 0);
__decorate([
    property({ type: String, attribute: 'caller-params' })
], Kode4Trigger.prototype, "callerParams", void 0);
__decorate([
    property({ type: Boolean, attribute: 'parent' })
], Kode4Trigger.prototype, "boundToParent", void 0);
Kode4Trigger = __decorate([
    customElement('kode4-trigger')
], Kode4Trigger);
export default Kode4Trigger;
