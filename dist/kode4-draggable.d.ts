export interface Kode4DraggableData {
    type?: string;
    payload?: any;
}
export interface Kode4DropzoneOptions {
    accepts?: Map<string, string>;
    acceptsFiles?: Boolean;
    acceptsExternal?: Boolean;
}
export declare class Kode4DragDropManager {
    private static instance;
    static getInstance(): Kode4DragDropManager;
    dropSuccessful: boolean;
    dataTransfer?: Kode4DraggableData;
    draggableElement?: HTMLElement;
}
export declare const dragDropManager: Kode4DragDropManager;
export declare const dragstart: (e: DragEvent, data: Kode4DraggableData, renderClone?: false | CallableFunction | undefined, handler?: CallableFunction | undefined) => void;
export declare const dragend: (e: DragEvent, handler?: CallableFunction | undefined) => void;
export declare const dragover: (e: DragEvent, options?: Kode4DropzoneOptions | undefined, handler?: CallableFunction | undefined) => void;
export declare const dragleave: (e: DragEvent, handler?: CallableFunction | undefined) => void;
export declare const drop: (e: DragEvent, handler: CallableFunction, callbackOptions?: any) => void;
/**
 * I tried to modify the ifDefined directive here. But
 * directives declared HERE just don't work in my project.
 * If I declare them directly in the project, they work fine.
 */
