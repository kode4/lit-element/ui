import { __decorate } from "tslib";
import { LitElement, html, css } from "lit";
import { customElement, property, query, queryAssignedElements, state } from "lit/decorators.js";
import kode4CssTheme from "./kode4-css";
let Kode4Table = class Kode4Table extends LitElement {
    constructor() {
        super(...arguments);
        this.observerFix = false;
        this.listEndObserverOptions = {
            rootMargin: '0px',
            threshold: 1,
        };
        this.observerIsRunning = false;
    }
    get empty() {
        return !this.listBodyElements || !!this.listBodyElements.length;
    }
    async firstUpdated(_changedProperties) {
        super.firstUpdated(_changedProperties);
        // let parent:HTMLElement|null = this;
        // let theRoot:HTMLElement|null = null;
        // while (!theRoot && parent) {
        //     const hasOverflow = getComputedStyle(parent).overflow;
        //     const hasOverflowY = getComputedStyle(parent).overflowY;
        //     console.log(' => OVERFLOW', hasOverflow, hasOverflowY, parent);
        //     if (['auto', 'scroll'].includes(hasOverflowY)) {
        //         console.log(' => FOUND THE ELEMENT!');
        //         theRoot = parent;
        //     }
        //     parent = parent.parentElement;
        // }
        // console.log('----------------------------------------')
        // console.log(theRoot);
        /*if (this.observerRoot) {
            this.listEndObserverOptions.root = this.observerRoot;
        } else*/
        if (this.listEndObserverRootSelector) {
            this.listEndObserverOptions.root = this.closest(this.listEndObserverRootSelector);
        }
        this.listEndObserver = new IntersectionObserver(async (entries) => {
            if (entries[0].intersectionRatio >= this.listEndObserverOptions.threshold) {
                await this.triggerObserverHandler();
                // if (this.more instanceof Function) {
                //     this.stopObserver();
                //     await this.more();
                //     this.startObserver();
                //
                // } else {
                //     this.dispatchEvent(new CustomEvent('kode4-table-more', {
                //         bubbles: true,
                //         composed: true,
                //     }));
                // }
            }
        }, this.listEndObserverOptions);
        this.startObserver();
    }
    async triggerObserverHandler() {
        if (this.more instanceof Function) {
            // this.stopObserver();
            await this.more();
            // this.startObserver();
        }
        else {
            this.dispatchEvent(new CustomEvent('kode4-table-more', {
                bubbles: true,
                composed: true,
            }));
        }
    }
    stopObserver() {
        if (!this.observerIsRunning) {
            return;
        }
        if (!this.currentObservedElement) {
            return;
        }
        this.listEndObserver.unobserve(this.currentObservedElement);
        this.observerIsRunning = false;
        this.currentObservedElement = undefined;
    }
    startObserver() {
        var _a;
        if (this.observerIsRunning) {
            return;
        }
        if ((_a = this.listBodyElements) === null || _a === void 0 ? void 0 : _a.length) {
            this.currentObservedElement = this.listBodyElements[this.listBodyElements.length - 1];
        }
        else {
            this.currentObservedElement = this.listEndObserverTrigger;
        }
        this.listEndObserver.observe(this.currentObservedElement);
        this.observerIsRunning = true;
    }
    disconnectedCallback() {
        this.listEndObserver.disconnect();
        this.observerIsRunning = false;
        super.disconnectedCallback();
    }
    handleSlotchange() {
        this.stopObserver();
        if (this.changed instanceof Function) {
            this.changed();
        }
        this.startObserver();
    }
    render() {
        return html `
            <div class="header">
                <slot name="header"></slot>
            </div>

            <slot @slotchange=${this.handleSlotchange}></slot>

            <div class="empty ${this.empty ? 'hidden' : ''}" style="display: table-row-group !important; width:100%;">
                <slot name="empty">
                    <div class="kode4-center-h" part="empty">
                        --
                    </div>
                </slot>
            </div>

            <div id="listEndObserver" class="${this.observerFix ? 'absolute' : undefined}" part="end-observer"></div>
        `;
    }
};
Kode4Table.styles = [
    kode4CssTheme,
    css `
            * {
                box-sizing: border-box;
            }

            :host {
                position: relative;
                display: table;
                table-layout: fixed;
                width: 100%;
                background-color: #fff;
            }
            
            .header {
                display: table-row;
                position: sticky;
                top: 0;
                background-color: inherit;
            }

            .header > ::slotted(*) {
                display: table-cell;
            }

            #listEndObserver {
                display: block;
                height: 0;
                width: 100%;
            }

            #listEndObserver.absolute {
                display: block;
                position: absolute;
                height: 0;
                width: 0;
                left: 0;
                bottom: 20px;
            }
            
            .empty:not(.hidden) {
                display: block;
                visibility: visible;
                position: absolute;
                left: 0;
                right: 0;
                top: 100%;
                width: 100%;
            }
            
            .empty.hidden {
                display: none;
                visibility: hidden;
            }
        `
];
__decorate([
    query('#listEndObserver')
], Kode4Table.prototype, "listEndObserverTrigger", void 0);
__decorate([
    property({ type: 'String', attribute: 'observer-root' })
], Kode4Table.prototype, "listEndObserverRootSelector", void 0);
__decorate([
    property({ type: 'String' })
], Kode4Table.prototype, "observerRoot", void 0);
__decorate([
    property({ type: Boolean, attribute: 'observer-fix' })
], Kode4Table.prototype, "observerFix", void 0);
__decorate([
    property({ attribute: false })
], Kode4Table.prototype, "more", void 0);
__decorate([
    property({ attribute: false })
], Kode4Table.prototype, "changed", void 0);
__decorate([
    queryAssignedElements()
], Kode4Table.prototype, "listBodyElements", void 0);
__decorate([
    state()
], Kode4Table.prototype, "empty", null);
__decorate([
    state()
], Kode4Table.prototype, "observerIsRunning", void 0);
Kode4Table = __decorate([
    customElement('kode4-table')
], Kode4Table);
export default Kode4Table;
