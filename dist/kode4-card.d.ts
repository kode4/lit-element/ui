import { LitElement, PropertyValues } from "lit";
export default class Kode4Card extends LitElement {
    private bodyElement;
    private headerItems;
    private mainItems;
    private footerItems;
    private fixedWidth;
    get contentWidth(): string;
    get contentHeight(): string;
    private resizeContentObserver;
    protected firstUpdated(_changedProperties: PropertyValues): void;
    scrollBodyTo(pos: number): void;
    onBodyScrolled(): void;
    connectedCallback(): void;
    disconnectedCallback(): void;
    render(): import("lit-html").TemplateResult<1>;
    static styles: import("lit").CSSResult[];
}
