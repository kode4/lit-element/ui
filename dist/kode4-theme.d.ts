import { CSSResult, LitElement } from "lit";
declare type Constructor<T = {}> = new (...args: any[]) => T;
export declare class IApplyKode4Theme {
    kode4ThemeType: string;
    primary: boolean;
    secondary: boolean;
    success: boolean;
    danger: boolean;
    warning: boolean;
    info: boolean;
    text: boolean;
    textPrimary: boolean;
    textSecondary: boolean;
    textSuccess: boolean;
    textDanger: boolean;
    textWarning: boolean;
    textInfo: boolean;
    bg: boolean;
    bgPrimary: boolean;
    bgSecondary: boolean;
    bgSuccess: boolean;
    bgDanger: boolean;
    bgWarning: boolean;
    bgInfo: boolean;
    static kode4Css: CSSResult;
}
export declare const applyKode4Theme: <T extends Constructor<LitElement>>(superClass: T) => Constructor<IApplyKode4Theme> & T;
export {};
