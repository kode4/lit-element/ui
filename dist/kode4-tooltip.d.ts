import { LitElement, PropertyValues } from "lit";
export default class Kode4Tooltip extends LitElement {
    open: boolean;
    closeDelay: number;
    x: string;
    y: string;
    mouseEntered: boolean;
    container: HTMLElement | null;
    private parendMouseenterEventListener;
    private parendMouseleaveEventListener;
    connectedCallback(): void;
    disconnectedCallback(): void;
    firstUpdated(changedProperties: PropertyValues): void;
    onParentMouseover(): void;
    onParentMouseleave(): void;
    render(): import("lit-html").TemplateResult<1>;
    private get arrowclass();
    static styles: import("lit").CSSResult[];
}
