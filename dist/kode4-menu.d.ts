import { LitElement } from "lit";
import './kode4-backdrop';
import './kode4-list';
export declare const kode4Menus: Map<string, Kode4Menu>;
export default class Kode4Menu extends LitElement {
    private name?;
    private isOpen;
    set open(value: boolean);
    private toggleCssClass;
    transparent: boolean;
    set visible(value: boolean);
    get visible(): boolean;
    private menu;
    _backdrop: boolean;
    get backdrop(): boolean;
    set backdrop(value: boolean);
    backdropElement?: HTMLElement;
    private floating;
    private bound;
    private boundToParent;
    set parent(value: boolean);
    set left(value: boolean);
    get left(): boolean;
    set right(value: boolean);
    get right(): boolean;
    set top(value: boolean);
    get top(): boolean;
    set bottom(value: boolean);
    get bottom(): boolean;
    set stretch(value: boolean);
    get stretch(): boolean;
    private cover;
    x?: string;
    y?: string;
    minWidth?: string;
    width?: string;
    private callerParams?;
    getCallerParams(): any;
    onShow(e: Event | MouseEvent, preventDefault?: boolean): void;
    show(params?: {
        x?: number;
        y?: number;
        alignToElement?: HTMLElement;
        callerParams?: any;
    }): void;
    private findFloatingPosition;
    private findBoundToParentPosition;
    hide(): void;
    private handleBackdropClick;
    /**
     * Automatically hide menu if a menu-item is clicked an defaultPrevented.
     *
     * @param e
     * @private
     */
    private handleListClick;
    handleParentClick: (e: MouseEvent) => void;
    connectedCallback(): void;
    disconnectedCallback(): void;
    render(): import("lit-html").TemplateResult<1>;
    static styles: import("lit").CSSResult[];
}
