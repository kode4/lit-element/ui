import { __decorate } from "tslib";
import { LitElement, html, css } from "lit";
import { customElement, property } from "lit/decorators.js";
let Kode4Backdrop = class Kode4Backdrop extends LitElement {
    toggleCssClass(show, cssClass) {
        show ? this.classList.add(cssClass) : this.classList.remove(cssClass);
    }
    set visible(value) {
        this.toggleCssClass(value, 'kode4-backdrop-visible');
    }
    get visible() {
        return this.classList.contains('kode4-backdrop-visible');
    }
    set transparent(value) {
        this.toggleCssClass(value, 'kode4-backdrop-transparent');
    }
    get transparent() {
        return this.classList.contains('kode4-backdrop-transparent');
    }
    render() {
        return html `
            <slot></slot>
        `;
    }
};
Kode4Backdrop.styles = [
    css `
            :host {
                position: fixed;
                top: 0;
                bottom: 0;
                left: 0;
                right: 0;
                z-index: 10000;
                overflow: hidden;
                background-color: rgba(0, 0, 0, 0.25);
                transition: background 0.25s ease-out;
            }
            
            :host(.kode4-backdrop-transparent) {
                background-color: transparent;
            }

            :host(:not(.kode4-backdrop-visible)) {
                bottom: 100%;
                background-color: transparent;
                transition: all 0s linear 0.25s, background 0.25s ease-out;
            }
        `
];
__decorate([
    property({ type: Boolean, attribute: 'visible' })
], Kode4Backdrop.prototype, "visible", null);
__decorate([
    property({ type: Boolean, attribute: 'transparent' })
], Kode4Backdrop.prototype, "transparent", null);
Kode4Backdrop = __decorate([
    customElement('kode4-backdrop')
], Kode4Backdrop);
export default Kode4Backdrop;
