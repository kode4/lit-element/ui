import { __decorate } from "tslib";
import { LitElement, html, css } from "lit";
import { customElement, query } from "lit/decorators.js";
import { getGrowlMessenger } from "./kode4-growl";
import kode4CssTheme from "./kode4-css";
let Kode4GrowlAreas = class Kode4GrowlAreas extends LitElement {
    connectedCallback() {
        getGrowlMessenger().observe(this);
        super.connectedCallback();
    }
    disconnectedCallback() {
        getGrowlMessenger().unobserve(this);
        super.disconnectedCallback();
    }
    onGrowlAlert(message) {
        this.alert(message);
    }
    alert(message) {
        var _a, _b, _c, _d, _e, _f;
        switch (message.pos) {
            case 't':
            case 'top':
                (_a = this.growlT) === null || _a === void 0 ? void 0 : _a.alert(message);
                break;
            case 'tl':
            case 'top-left':
                (_b = this.growlTl) === null || _b === void 0 ? void 0 : _b.alert(message);
                break;
            case 'tr':
            case 'top-right':
                (_c = this.growlTr) === null || _c === void 0 ? void 0 : _c.alert(message);
                break;
            case 'b':
            case 'bottom':
                (_d = this.growlB) === null || _d === void 0 ? void 0 : _d.alert(message);
                break;
            case 'bl':
            case 'bottom-left':
                (_e = this.growlBl) === null || _e === void 0 ? void 0 : _e.alert(message);
                break;
            default:
                (_f = this.growlBr) === null || _f === void 0 ? void 0 : _f.alert(message);
        }
    }
    render() {
        return html `
            <div>
                
            <kode4-growl id="growlTop" horizontal-align="stretch" vertical-align="top"></kode4-growl>
            <kode4-growl id="growlTopLeft" horizontal-align="left" vertical-align="top"></kode4-growl>
            <kode4-growl id="growlTopRight" horizontal-align="right" vertical-align="top"></kode4-growl>
            <kode4-growl id="growlBottom" horizontal-align="stretch" vertical-align="bottom"></kode4-growl>
            <kode4-growl id="growlBottomLeft" horizontal-align="left" vertical-align="bottom"></kode4-growl>
            <kode4-growl id="growlBottomRight" horizontal-align="right" vertical-align="bottom"></kode4-growl>
            </div>
        `;
    }
};
Kode4GrowlAreas.styles = [
    kode4CssTheme,
    css `
            kode4-growl {
                position: fixed;
                display: block;
                z-index: 12000;
            }
            
            #growlTop {
                top: 0;
                left: 0;
                right: 0;
                width: auto;
            }
            
            #growlTopLeft {
                top: 10px;
                left: 10px;
            }
            
            #growlTopRight {
                top: 10px;
                right: 10px;
            }
            
            #growlBottom {
                bottom: 0;
                left: 0;
                right: 0;
                width: auto;
            }
            
            #growlBottomLeft {
                bottom: 10px;
                left: 10px;
            }
            
            #growlBottomRight {
                bottom: 10px;
                right: 10px;
            }
        `,
];
__decorate([
    query('kode4-growl#growlTop')
], Kode4GrowlAreas.prototype, "growlT", void 0);
__decorate([
    query('kode4-growl#growlTopLeft')
], Kode4GrowlAreas.prototype, "growlTl", void 0);
__decorate([
    query('kode4-growl#growlTopRight')
], Kode4GrowlAreas.prototype, "growlTr", void 0);
__decorate([
    query('kode4-growl#growlBottom')
], Kode4GrowlAreas.prototype, "growlB", void 0);
__decorate([
    query('kode4-growl#growlBottomLeft')
], Kode4GrowlAreas.prototype, "growlBl", void 0);
__decorate([
    query('kode4-growl#growlBottomRight')
], Kode4GrowlAreas.prototype, "growlBr", void 0);
Kode4GrowlAreas = __decorate([
    customElement('kode4-growl-areas')
], Kode4GrowlAreas);
export default Kode4GrowlAreas;
