export class Kode4DragDropManager {
    constructor() {
        this.dropSuccessful = false;
    }
    static getInstance() {
        if (!this.instance) {
            this.instance = new Kode4DragDropManager();
        }
        return this.instance;
    }
}
export const dragDropManager = Kode4DragDropManager.getInstance();
// --- DRAGGABLE ----------------
export const dragstart = (e, data, renderClone, handler) => {
    var _a, _b;
    e.stopPropagation();
    dragDropManager.dropSuccessful = false;
    const draggableElement = e.target.closest('[draggable],[draggable="true"]');
    dragDropManager.draggableElement = draggableElement;
    // --- custom clone ---
    // var clone:HTMLElement = <HTMLElement>draggableElement.cloneNode(true);
    // clone.style.backgroundColor = "red";
    // clone.style.position = "fixed";
    // clone.style.top = "-5000px";
    // clone.style.right = "-5000px";
    // document.body.appendChild(clone);
    if (renderClone) {
        renderClone(e);
    }
    else if (renderClone === false) {
        (_a = e.dataTransfer) === null || _a === void 0 ? void 0 : _a.setDragImage(document.createElement('img'), 0, 0);
    }
    else {
        (_b = e.dataTransfer) === null || _b === void 0 ? void 0 : _b.setDragImage(draggableElement, 0, 0);
    }
    dragDropManager.dataTransfer = data;
    // e.dataTransfer?.setData("text", dragDropManager.encodeDataTransfer(data));
    if (handler) {
        handler(e, dragDropManager.dataTransfer);
    }
};
export const dragend = (e, handler) => {
    dragDropManager.draggableElement = undefined;
    if (handler) {
        handler(e, dragDropManager.dataTransfer);
    }
    dragDropManager.dropSuccessful = false;
    dragDropManager.dataTransfer = undefined;
};
// --- DROP-ZONE ----------------
export const dragover = (e, options, handler) => {
    var _a;
    if (dragDropManager.dataTransfer) {
        if (dragDropManager.dataTransfer.type && ((_a = options === null || options === void 0 ? void 0 : options.accepts) === null || _a === void 0 ? void 0 : _a.has(dragDropManager.dataTransfer.type))) {
            e.target.classList.add('kode4-dragover');
            if (e.dataTransfer) {
                e.dataTransfer.dropEffect = options.accepts.get(dragDropManager.dataTransfer.type);
            }
        }
        e.preventDefault();
    }
    else if ((options === null || options === void 0 ? void 0 : options.acceptsFiles) || (options === null || options === void 0 ? void 0 : options.acceptsExternal)) {
        e.target.classList.add('kode4-dragover');
        if (e.dataTransfer) {
            e.dataTransfer.dropEffect = 'copy';
        }
        e.preventDefault();
    }
    if (handler) {
        handler(e, dragDropManager.dataTransfer);
    }
};
export const dragleave = (e, handler) => {
    e.target.classList.remove('kode4-dragover');
    if (handler) {
        handler(e, dragDropManager.dataTransfer);
    }
};
export const drop = (e, handler, callbackOptions) => {
    e.preventDefault();
    e.stopPropagation();
    dragDropManager.dropSuccessful = true;
    e.target.classList.remove('kode4-dragover');
    handler(e, dragDropManager.dataTransfer, callbackOptions);
};
/**
 * I tried to modify the ifDefined directive here. But
 * directives declared HERE just don't work in my project.
 * If I declare them directly in the project, they work fine.
 */
// import {AttributePart, directive, Part} from 'lit-html';
// export const draggable = directive((value: unknown) => (part: Part) => {
//     if (value === undefined && part instanceof AttributePart) {
//       if (value !== part.value) {
//         const name = part.committer.name;
//         part.committer.element.removeAttribute(name);
//       }
//     } else {
//       part.setValue(value);
//     }
//   });
