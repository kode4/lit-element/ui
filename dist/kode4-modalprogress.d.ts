import { LitElement } from "lit";
export default class Kode4ModalProgress extends LitElement {
    color: String;
    protected _open: boolean;
    get open(): boolean;
    set open(open: boolean);
    protected opening: boolean;
    protected closing: boolean;
    render(): import("lit-html").TemplateResult<1>;
    static styles: import("lit").CSSResult[];
}
