import { __decorate } from "tslib";
import { LitElement, html, css } from "lit";
import { customElement, property } from "lit/decorators.js";
import kode4CssTheme from './kode4-css';
let Kode4Tooltip = class Kode4Tooltip extends LitElement {
    constructor() {
        super(...arguments);
        this.open = false;
        this.closeDelay = 150;
        this.x = 'right';
        this.y = 'center';
        this.mouseEntered = false;
        this.container = null;
    }
    connectedCallback() {
        var _a, _b;
        this.parendMouseenterEventListener = this.onParentMouseover.bind(this);
        this.parendMouseleaveEventListener = this.onParentMouseleave.bind(this);
        (_a = this.parentElement) === null || _a === void 0 ? void 0 : _a.addEventListener('mouseenter', this.parendMouseenterEventListener);
        (_b = this.parentElement) === null || _b === void 0 ? void 0 : _b.addEventListener('mouseleave', this.parendMouseleaveEventListener);
        super.connectedCallback();
    }
    disconnectedCallback() {
        var _a, _b;
        (_a = this.parentElement) === null || _a === void 0 ? void 0 : _a.removeEventListener('mouseenter', this.parendMouseenterEventListener);
        (_b = this.parentElement) === null || _b === void 0 ? void 0 : _b.removeEventListener('mouseleave', this.parendMouseleaveEventListener);
        super.disconnectedCallback();
    }
    firstUpdated(changedProperties) {
        var _a;
        super.firstUpdated(changedProperties);
        this.container = ((_a = this.shadowRoot) === null || _a === void 0 ? void 0 : _a.querySelector('.container')) || null;
    }
    onParentMouseover() {
        this.mouseEntered = true;
        // const openDelay = this.closeDelay > 1 ? this.closeDelay-1 : 0;
        setTimeout(() => {
            var _a;
            if (this.parentElement && this.container) {
                this.container.classList.add('opening');
                const parentRect = (_a = this.parentElement) === null || _a === void 0 ? void 0 : _a.getBoundingClientRect();
                const containerRect = this.container.getBoundingClientRect();
                const rect = this.getBoundingClientRect();
                // const offsetX = rect.x - parentRect.x;
                // const offsetY = rect.y - parentRect.y;
                let tooltipY;
                switch (this.y) {
                    case 'top':
                        tooltipY = 0 - (rect.y - parentRect.y) - containerRect.height - 10;
                        break;
                    case 'bottom':
                        tooltipY = parentRect.height + 10;
                        break;
                    case 'center':
                    default:
                        tooltipY = parentRect.height / 2 - (rect.y - parentRect.y) - containerRect.height / 2;
                }
                let tooltipX;
                switch (this.x) {
                    case 'center':
                        tooltipX = parentRect.width / 2 - (rect.x - parentRect.x) - containerRect.width / 2;
                        break;
                    case 'left':
                        tooltipX = 0 - (rect.x - parentRect.x) - containerRect.width - 10;
                        break;
                    case 'right':
                    default:
                        tooltipX = parentRect.width + 10;
                }
                this.container.style.top = `${tooltipY}px`;
                this.container.style.left = `${tooltipX}px`;
                this.container.classList.remove('opening');
            }
            this.open = true;
        }, this.closeDelay - 10);
    }
    onParentMouseleave() {
        this.mouseEntered = false;
        setTimeout(() => {
            if (!this.mouseEntered) {
                this.open = false;
            }
        }, this.closeDelay);
    }
    render() {
        return html `
            <div class="container ${this.open ? 'open' : ''} ${this.arrowclass}">
                <slot></slot>
            </div>
        `;
    }
    get arrowclass() {
        if (this.y === 'bottom' && this.x === 'center') {
            return 'arrowtop';
        }
        if (this.y === 'top' && this.x === 'center') {
            return 'arrowbottom';
        }
        if (this.x === 'left' && this.y === 'center') {
            return 'arrowleft';
        }
        if (this.x === 'right' && this.y === 'center') {
            return 'arrowright';
        }
        return '';
    }
};
Kode4Tooltip.styles = [
    kode4CssTheme,
    css `
            :host {
                position: relative;
                filter: none !important;
                background: transparent;
            }

            .container {
                display: none;
            }
            
            .container.open,
            .container.opening {
                display: block;
                position: absolute;
                z-index: 9000;
                top: 0;
                left: 0;
                padding-top: var(--kode4-tooltip-padding-top);
                padding-right: var(--kode4-tooltip-padding-right);
                padding-bottom: var(--kode4-tooltip-padding-bottom);
                padding-left: var(--kode4-tooltip-padding-left);
                filter: brightness(1) sepia(0) hue-rotate(0) saturate(100%);
                color: var(--kode4-tooltip-color);
                font-family: var(--kode4-tooltip-font-family);
                font-size: var(--kode4-tooltip-font-size);
                line-height: var(--kode4-tooltip-line-height);
                font-weight: var(--kode4-tooltip-font-weight);
                background: var(--kode4-tooltip-background-color);
                box-sizing: border-box;
                border-radius: var(--kode4-tooltip-border-radius);
            }

            .container.opening {
                visibility: hidden;
            }

            .container.open.arrowtop:before {
                content: " ";
                background: transparent;
                position: absolute;
                top: calc(var(--kode4-tooltip-arrow-size) * -1);
                left: calc(50% - var(--kode4-tooltip-arrow-size));
                display: block;
                width: 0;
                height: 0;
                border-style: solid;
                border-width: 0 var(--kode4-tooltip-arrow-size) var(--kode4-tooltip-arrow-size) var(--kode4-tooltip-arrow-size);
                border-color: transparent transparent var(--kode4-tooltip-background-color) transparent;
            }
            
            .container.open.arrowtop:before {
                content: " ";
                background: transparent;
                position: absolute;
                top: calc(var(--kode4-tooltip-arrow-size) * -1);
                left: calc(50% - var(--kode4-tooltip-arrow-size));
                display: block;
                width: 0;
                height: 0;
                border-style: solid;
                border-width: 0 var(--kode4-tooltip-arrow-size) var(--kode4-tooltip-arrow-size) var(--kode4-tooltip-arrow-size);
                border-color: transparent transparent var(--kode4-tooltip-background-color) transparent;
            }
            
            .container.open.arrowright:before {
                content: " ";
                background: transparent;
                position: absolute;
                top: calc(50% - var(--kode4-tooltip-arrow-size));
                left: calc(var(--kode4-tooltip-arrow-size) * -1);
                display: block;
                width: 0;
                height: 0;
                border-style: solid;
                border-width: var(--kode4-tooltip-arrow-size) var(--kode4-tooltip-arrow-size) var(--kode4-tooltip-arrow-size) 0;
                border-color: transparent var(--kode4-tooltip-background-color) transparent transparent;
            }
            
            .container.open.arrowbottom:before {
                content: " ";
                background: transparent;
                position: absolute;
                bottom: calc(var(--kode4-tooltip-arrow-size) * -1);
                left: calc(50% - var(--kode4-tooltip-arrow-size));
                display: block;
                width: 0;
                height: 0;
                border-style: solid;
                border-width: var(--kode4-tooltip-arrow-size) var(--kode4-tooltip-arrow-size) 0 var(--kode4-tooltip-arrow-size);
                border-color: var(--kode4-tooltip-background-color) transparent transparent transparent;
            }
            
            .container.open.arrowleft:before {
                content: " ";
                background: transparent;
                position: absolute;
                top: calc(50% - var(--kode4-tooltip-arrow-size));
                right: calc(var(--kode4-tooltip-arrow-size) * -1);
                display: block;
                width: 0;
                height: 0;
                border-style: solid;
                border-width: var(--kode4-tooltip-arrow-size) 0 var(--kode4-tooltip-arrow-size) var(--kode4-tooltip-arrow-size);
                border-color: transparent transparent transparent var(--kode4-tooltip-background-color);
            }
        `,
];
__decorate([
    property({ type: Boolean })
], Kode4Tooltip.prototype, "open", void 0);
__decorate([
    property({ type: Number })
], Kode4Tooltip.prototype, "closeDelay", void 0);
__decorate([
    property({ type: String })
], Kode4Tooltip.prototype, "x", void 0);
__decorate([
    property({ type: String })
], Kode4Tooltip.prototype, "y", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Tooltip.prototype, "mouseEntered", void 0);
__decorate([
    property()
], Kode4Tooltip.prototype, "container", void 0);
__decorate([
    property()
], Kode4Tooltip.prototype, "arrowclass", null);
Kode4Tooltip = __decorate([
    customElement('kode4-tooltip')
], Kode4Tooltip);
export default Kode4Tooltip;
