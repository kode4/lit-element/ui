import debounce from 'lodash-es/debounce';
import { cloneDeep } from "lodash-es";
export class ListLimitReachedError extends Error {
}
export class ListStateHandler {
    constructor(params = {}) {
        this.observers = [];
        this.restored = false;
        this._orderByFields = [];
        this._orderByDir = 'asc';
        this.filters = new Map();
        this.page = 0;
        this.lshState = 'initializing';
        this.pageSize = 0;
        this.size = -1; // -1 means unknown
        this.total = -1; // -1 means unknown
        this.cacheable = true;
        this.cacheLease = 60 * 10; // in seconds. default is 10 minutes
        this._hash = '';
        this.state = new Map();
        this.nullishValues = [null, undefined];
        this.fireOrderChanged = () => {
            this.store();
            this.observers.forEach((observer) => {
                if (observer.onOrderChange) {
                    observer.onOrderChange();
                }
            });
        };
        this.debouncedFireOrderChanged = debounce(this.fireOrderChanged, 50);
        this.fireFilterChanged = () => {
            this.store();
            this.observers.forEach((observer) => {
                if (observer.onFilterChange) {
                    observer.onFilterChange();
                }
            });
        };
        this.debouncedFireFilterChanged = debounce(this.fireFilterChanged, 50);
        this.firePaginationChanged = () => {
            this.store();
            this.observers.forEach((observer) => {
                if (observer.onPaginationChange) {
                    observer.onPaginationChange();
                }
            });
        };
        // private debouncedFirePaginationChanged = debounce(this.firePaginationChanged, 50);
        this.fireStateChanged = () => {
            this.store();
            this.observers.forEach((observer) => {
                if (observer.onStateChange) {
                    observer.onStateChange();
                }
            });
        };
        this.debouncedFireStateChanged = debounce(this.fireStateChanged, 50);
        this.fireRestore = () => {
            this.observers.forEach((observer) => {
                if (observer.onRestore) {
                    observer.onRestore();
                }
            });
        };
        this.fireReset = () => {
            this.reset();
            this.store();
            this.observers.forEach((observer) => {
                if (observer.onReset) {
                    observer.onReset();
                }
            });
        };
        this.fireRefresh = (forceReload = false) => {
            this.lshState = 'initializing';
            this.restored = true;
            this.observers.forEach((observer) => {
                if (observer.onRefresh) {
                    observer.onRefresh(forceReload);
                }
            });
        };
        if (params.filters) {
            params.filters = this.sanitizeFilterAndStateValues(params.filters);
        }
        if (params.state) {
            params.state = this.sanitizeFilterAndStateValues(params.state);
        }
        this._initialValues = cloneDeep(params);
        this.applyValues(params);
        if (params.observer) {
            this.observe(params.observer);
        }
        this.storage = params.storage;
        this.storageKey = params.storageKey;
        if (!this.canRestore()) {
            this.lshState = 'idle';
        }
        else {
            this.restore();
        }
    }
    loadPage(value) {
        if (this.lshState !== 'idle') {
            return;
        }
        switch (value) {
            case 'next':
                if (this.size === -1) {
                    this.page = 0;
                }
                else if (this.total !== -1 && this.size >= this.total) {
                    return;
                }
                else {
                    const expectedPage = Math.max(Math.floor(this.size / this.pageSize) - 1, 0);
                    if (expectedPage < this.page) {
                        const expectedSize = (this.page + 1) * this.pageSize;
                        if (this.total !== -1 && expectedSize > this.total) {
                            this.resetList();
                            return;
                        }
                        this.size = expectedSize;
                        this.refreshList();
                        return;
                    }
                    else {
                        this.page += 1;
                    }
                }
                this.firePaginationChanged();
                break;
            case 'prev':
            case 'previous':
                if (this.page > 0) {
                    this.page -= 1;
                    this.firePaginationChanged();
                }
                break;
            default:
                this.page = Number(value);
                this.firePaginationChanged();
        }
    }
    get hash() {
        return this.buildHash();
        if (!this._hash) {
            this.updateHash();
        }
        return this._hash;
    }
    // =====[ Internal State ]==========================================================================================
    reset() {
        this.applyValues(this._initialValues);
    }
    resetList() {
        this.fireReset();
    }
    refreshList(forceReload = true) {
        this.fireRefresh(forceReload);
    }
    store() {
        if (this.storage && this.storageKey) {
            this.storage.setItem(this.storageKey, this.toString());
        }
    }
    restore() {
        if (this.canRestore()) {
            this.fromString(this.storage.getItem(this.storageKey));
            this.fireRestore();
            return true;
        }
        return false;
    }
    canRestore() {
        return this.storage && this.storageKey && this.storage.hasItem(this.storageKey);
    }
    applyValues(params = {}) {
        if (params.fields) {
            this.orderByFields = params.fields;
        }
        if (params.field) {
            this.orderBy = params.field;
        }
        if (params.dir) {
            this.orderByDir = params.dir;
        }
        if (params.filters) {
            this.filters = this.sanitizeFilterAndStateValues(cloneDeep(params.filters));
        }
        if (params.page !== undefined) {
            this.page = params.page;
        }
        if (params.pageSize !== undefined) {
            this.pageSize = params.pageSize;
        }
        if (params.cacheable !== undefined) {
            this.cacheable = params.cacheable;
        }
        if (params.cacheLease !== undefined) {
            this.cacheLease = params.cacheLease;
        }
        if (params.state) {
            this.state = this.sanitizeFilterAndStateValues(cloneDeep(params.state));
        }
    }
    toString() {
        const serialize = {
            _orderByFields: this._orderByFields,
            _orderByField: this._orderBy,
            _orderByDir: this._orderByDir,
            filters: Array.from(this.filters.entries()),
            page: this.page,
            pageSize: this.pageSize,
            size: this.size,
            total: this.total,
            cacheable: this.cacheable,
            cacheLease: this.cacheLease,
            lastRefresh: this.lastRefresh,
            _hash: this._hash,
            state: Array.from(this.state.entries())
        };
        return JSON.stringify(serialize);
    }
    /**
     * Please re-register observers
     *
     * @param serialized
     */
    fromString(serialized) {
        const unserialized = JSON.parse(serialized);
        if (unserialized._fields) {
            this._orderByFields = unserialized._fields;
        }
        if (unserialized._field) {
            this._orderBy = unserialized._field;
        }
        if (unserialized._dir) {
            this._orderByDir = unserialized._dir;
        }
        if (unserialized.filters) {
            this.filters = new Map(unserialized.filters);
        }
        else {
            this.filters = new Map();
        }
        if (unserialized.page) {
            this.page = unserialized.page;
        }
        if (unserialized.page) {
            this.page = unserialized.page;
        }
        if (unserialized.pageSize) {
            this.pageSize = unserialized.pageSize;
        }
        if (unserialized.size) {
            this.size = unserialized.size;
        }
        if (unserialized.total) {
            this.total = unserialized.total;
        }
        if (unserialized.cacheable) {
            this.cacheable = unserialized.cacheable;
        }
        if (unserialized.cacheLease) {
            this.cacheLease = unserialized.cacheLease;
        }
        if (unserialized.lastRefresh) {
            this.lastRefresh = unserialized.lastRefresh;
        }
        if (unserialized._hash) {
            this._hash = unserialized._hash;
        }
        if (unserialized.state) {
            this.state = new Map(unserialized.state);
        }
        else {
            this.state = new Map();
        }
        this.restored = true;
    }
    buildHash() {
        return JSON.stringify({
            orderField: this._orderBy,
            orderDir: this._orderByDir,
            filter: Array.from(this.filters),
            size: this.size,
            page: this.page,
        });
    }
    updateHash() {
        this._hash = this.buildHash();
    }
    // =====[ Pagination ]==============================================================================================
    clearPagination() {
        this.page = 0;
        this.size = -1;
        this.total = -1;
    }
    // =====[ Order By ]================================================================================================
    get orderByFields() {
        return this._orderByFields;
    }
    set orderByFields(fields) {
        this._orderByFields = fields;
    }
    addOrderByField(field) {
        this.orderByFields = [
            ...this.orderByFields,
            field
        ];
    }
    get orderBy() {
        return this._orderBy;
    }
    set orderBy(field) {
        if (!field) {
            this._orderBy = field;
            this.clearPagination();
            this.debouncedFireOrderChanged();
            return;
        }
        if (this.orderByFields.includes(field)) {
            this._orderBy = field;
            this.clearPagination();
            this.debouncedFireOrderChanged();
        }
    }
    get orderByDir() {
        return this._orderByDir;
    }
    set orderByDir(dir) {
        if (dir === undefined) {
            this._orderByDir = this._orderByDir === 'asc' ? 'desc' : 'asc';
            this.debouncedFireOrderChanged();
        }
        else {
            this._orderByDir = dir;
            this.debouncedFireOrderChanged();
        }
    }
    isSortable(field) {
        return this._orderByFields.includes(field);
    }
    getOrderByDirForField(field) {
        return this._orderBy === field ? this.orderByDir : undefined;
    }
    activateOrderByFieldOrToggleDir(field) {
        this.clearPagination();
        if (this._orderBy === field) {
            this.orderByDir = undefined;
        }
        else {
            this.orderBy = field;
            this.orderByDir = 'asc';
        }
    }
    // =====[ Filters ]=================================================================================================
    setFilter(key, value) {
        if (this.filters.get(key) !== value) {
            this.filters.set(key, this.sanitizeFilterAndStateValue(value));
            this.clearPagination();
            this.debouncedFireFilterChanged();
        }
    }
    getFilter(key) {
        return this.filters.get(key);
    }
    hasFilter(key) {
        return this.filters.has(key);
    }
    hasFilters() {
        let hasFilters = false;
        for (const [_key, value] of this.filters) {
            if (value) {
                hasFilters = true;
            }
        }
        return hasFilters;
    }
    deleteFilter(key) {
        this.filters.delete(key);
    }
    clearFilters(keys) {
        if (keys) {
            keys.forEach((key) => {
                this.deleteFilter(key);
            });
        }
        this.filters.clear();
        this.clearPagination();
        this.debouncedFireFilterChanged();
    }
    hasActiveFilters() {
        var _a;
        if (!this.filters || !((_a = this._initialValues) === null || _a === void 0 ? void 0 : _a.filters)) {
            return false;
        }
        if (this._initialValues.filters.size !== this.filters.size) {
            return true;
        }
        let filterChanged = false;
        for (const [filter, value] of this.filters.entries()) {
            const initialValue = this._initialValues.filters.get(filter);
            if (this.nullishValues.includes(value) && !this.nullishValues.includes(initialValue)) {
                filterChanged = true;
            }
            else if (!this.nullishValues.includes(value) && value !== initialValue) {
                filterChanged = true;
            }
        }
        return filterChanged;
    }
    resetFilters() {
        var _a;
        if (((_a = this._initialValues) === null || _a === void 0 ? void 0 : _a.filters) && this.hasActiveFilters()) {
            this.filters = this.sanitizeFilterAndStateValues(cloneDeep(this._initialValues.filters));
            this.clearPagination();
            this.debouncedFireFilterChanged();
        }
    }
    // =====[ State ]===================================================================================================
    setState(key, value) {
        if (this.state.get(key) !== value) {
            this.state.set(key, this.sanitizeFilterAndStateValue(value));
            this.debouncedFireStateChanged();
        }
    }
    getState(key) {
        return this.state.get(key);
    }
    hasState(key) {
        return this.state.has(key);
    }
    deleteState(key) {
        this.state.delete(key);
        this.debouncedFireStateChanged();
    }
    clearState() {
        this.state.clear();
        this.debouncedFireStateChanged();
    }
    // =====[ Events ]==================================================================================================
    observe(observer) {
        if (!this.observers.includes(observer)) {
            this.observers.push(observer);
            if (observer.onOrderChange) {
                observer.onOrderChange();
            }
            if (observer.onFilterChange) {
                observer.onFilterChange();
            }
            if (observer.onStateChange) {
                observer.onStateChange();
            }
            if (observer.onPaginationChange) {
                observer.onPaginationChange();
            }
        }
    }
    unobserve(observer) {
        if (!observer) {
            this.observers = [];
        }
        else {
            this.observers = [
                ...this.observers.filter((el) => el !== observer)
            ];
        }
    }
    // =====[ Helpers ]=================================================================================================
    sanitizeFilterAndStateValues(data) {
        for (const [key, value] of data) {
            data.set(key, this.sanitizeFilterAndStateValue(value));
        }
        return data;
    }
    sanitizeFilterAndStateValue(value) {
        if (value === null) {
            return undefined;
        }
        return value;
    }
    // public reloadRequired() {
    //     if (!this.cacheable) {
    //         return true;
    //
    //     } else if (this.buildHash() !== this._hash) {
    //         return true;
    //
    //     } else if (this.lastRefresh === undefined) {
    //         return true;
    //
    //     } else if (this.lastRefresh + this.cacheLease < (Date.now() / 1000 | 0)) {
    //         return true;
    //
    //     }
    //
    //     return false;
    // }
    getRequestParameters() {
        if (!this.restored && this.total !== -1 && this.size >= this.total) {
            throw new ListLimitReachedError(`List limit reached: ${this.total}`);
        }
        // fix broken size
        if (this.size === -1 || this.size !== this.pageSize * (this.page + 1)) {
            this.size = this.pageSize * (this.page + 1);
        }
        const params = {
            page: this.restored ? 0 : this.page,
            pageSize: this.restored ? this.size : this.pageSize,
            filters: new Map([...this.filters].filter(([_key, value]) => value !== undefined && value !== null && value !== '' && value !== false)),
            state: this.state,
            restored: this.restored,
            hash: this.hash,
        };
        if (this.orderBy) {
            params.orderBy = `${this.orderBy}:${this.getOrderByDirForField(this.orderBy) || 'asc'}`;
        }
        if (this.restored) {
            this.restored = false;
        }
        return params;
    }
}
