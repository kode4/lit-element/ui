import { __decorate } from "tslib";
import { LitElement, html, css } from "lit";
import { customElement, property } from "lit/decorators.js";
import kode4CssTheme from './kode4-css';
import { v4 as uuidv4 } from "uuid";
import { faTimes } from "@fortawesome/free-solid-svg-icons/faTimes";
export const dispathGrowlAlert = (el, position, message) => {
    el.dispatchEvent(new CustomEvent("kode4-growl-alert", {
        bubbles: true,
        composed: true,
        detail: {
            ...message,
            pos: position,
        }
    }));
};
export class Kode4GrowlMessenger {
    constructor() {
        this.observers = [];
        this.fireGrowlAlert = (message) => {
            this.observers.forEach((observer) => {
                observer.onGrowlAlert(message);
            });
        };
    }
    observe(observer) {
        if (!this.observers.includes(observer)) {
            this.observers.push(observer);
        }
    }
    unobserve(observer) {
        if (!observer) {
            this.observers = [];
        }
        else {
            this.observers = [
                ...this.observers.filter((el) => el !== observer)
            ];
        }
    }
    alert(message) {
        this.fireGrowlAlert(message);
    }
}
const _growlMessenger = new Kode4GrowlMessenger();
export const getGrowlMessenger = () => {
    return _growlMessenger;
};
let Kode4Growl = class Kode4Growl extends LitElement {
    constructor() {
        super(...arguments);
        this.messages = [];
        this.horizontalAlign = 'right';
        this.verticalAlign = 'bottom';
    }
    alert(growlMessage) {
        if (!growlMessage.id) {
            growlMessage.id = `kode4growlmessage_${uuidv4()}`;
        }
        const id = growlMessage.id;
        this.messages = [
            ...this.messages,
            growlMessage
        ];
        if (growlMessage.timeout) {
            setTimeout(() => {
                this.closeMessage(id);
            }, growlMessage.timeout);
        }
    }
    closeMessage(id) {
        if (!id) {
            return;
        }
        this.messages = [
            ...this.messages.filter((el) => el.id !== id)
        ];
    }
    render() {
        return html `
            <div class="container ${this.horizontalAlign} ${this.verticalAlign}" part="container">
                ${this.messages.map((msg) => html `
                    <div class="message kode4-alert ${msg.color ? `kode4-bg-${msg.color}` : ''}"
                         id="${msg.id}">
                        <div class="closeButton kode4-text-hover" @click="${(e) => {
            e.preventDefault();
            this.closeMessage(msg.id);
        }}">
                            <kode4-fa-icon .icon="${faTimes}"></kode4-fa-icon>
                        </div>
                        ${msg.message}
                    </div>
                `)}
            </div>
        `;
    }
};
Kode4Growl.styles = [
    kode4CssTheme,
    css `
            :host {
                display: inline-block;
                position: relative;
                width: 0;
                height: 0;
                padding: 0;
                margin: 0;
                border: none;
                z-index: 10010;
            }

            .container {
                position: absolute;
                max-height: 600px;
                bottom: 0;
                right: 0;
                z-index: 10020;
            }

            .container.bottom {
                top: auto;
                bottom: 0;
            }

            .container.top {
                top: 0;
                bottom: auto;
            }

            .container.right {
                left: auto;
                right: 0;
                width: 300px;
                max-width: 80vw;
            }

            .container.left {
                left: 0;
                right: auto;
                width: 300px;
                max-width: 80vw;
            }

            .container.stretch {
                left: 0;
                right: 0;
                width: auto;
            }

            .message {
                width: auto;
                position: relative;
            }

            .message + .message {
                margin-top: 10px;
            }

            .closeButton {
                position: absolute;
                top: 5px;
                right: 5px;
                cursor: pointer;
                opacity: 0;
                transition: all 0.15s linear;
            }

            .message:hover .closeButton {
                opacity: 1;
            }
        `,
];
__decorate([
    property()
], Kode4Growl.prototype, "messages", void 0);
__decorate([
    property({ attribute: 'horizontal-align' })
], Kode4Growl.prototype, "horizontalAlign", void 0);
__decorate([
    property({ attribute: 'vertical-align' })
], Kode4Growl.prototype, "verticalAlign", void 0);
Kode4Growl = __decorate([
    customElement('kode4-growl')
], Kode4Growl);
export default Kode4Growl;
