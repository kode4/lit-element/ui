import { LitElement, PropertyValues, CSSResult } from "lit";
export default class Kode4Progress extends LitElement {
    type: string;
    color?: string;
    indeterminate: boolean;
    inline: boolean;
    showPercentage: boolean;
    protected progress: number;
    protected visible: boolean;
    firstUpdated(changedProperties: PropertyValues): void;
    render(): import("lit-html").TemplateResult<1>;
    protected renderProgessIcon(): import("lit-html").TemplateResult<1>;
    protected renderProgessBarIndeterminateIcon(): import("lit-html").TemplateResult<1>;
    protected renderProgessBarDeterminateIcon(): import("lit-html").TemplateResult<1>;
    protected getAngleForProgress(): number;
    protected renderProgessText(): import("lit-html").TemplateResult<1>;
    protected renderProgessBarIndeterminateText(): import("lit-html").TemplateResult<1>;
    protected renderProgessBarDeterminateText(): import("lit-html").TemplateResult<1>;
    protected renderProgressLinear(): import("lit-html").TemplateResult<1>;
    protected renderProgressBarIndeterminateLinear(): import("lit-html").TemplateResult<1>;
    protected renderProgressBarDeterminateLinear(): import("lit-html").TemplateResult<1>;
    static styles: CSSResult[];
    protected static getIconStyles(): CSSResult;
    protected static getTextStyles(): CSSResult;
    protected static getLinearStyles(): CSSResult;
}
