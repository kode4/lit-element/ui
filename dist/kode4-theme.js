import { __decorate } from "tslib";
import { property } from "lit/decorators.js";
export const applyKode4Theme = (superClass) => {
    class ApplyKode4Theme extends superClass {
        constructor() {
            super(...arguments);
            this.kode4ThemeType = 'text';
        }
        toggleCssClass(value, cssClass) {
            value ? this.classList.add(cssClass) : this.classList.remove(cssClass);
        }
        set primary(value) {
            this.toggleCssClass(value, `kode4-${this.kode4ThemeType}-primary`);
        }
        get primary() {
            return this.classList.contains(`kode4-${this.kode4ThemeType}-primary`);
        }
        set secondary(value) {
            this.toggleCssClass(value, `kode4-${this.kode4ThemeType}-secondary`);
        }
        get secondary() {
            return this.classList.contains(`kode4-${this.kode4ThemeType}-secondary`);
        }
        set success(value) {
            this.toggleCssClass(value, `kode4-${this.kode4ThemeType}-success`);
        }
        get success() {
            return this.classList.contains(`kode4-${this.kode4ThemeType}-success`);
        }
        set danger(value) {
            this.toggleCssClass(value, `kode4-${this.kode4ThemeType}-danger`);
        }
        get danger() {
            return this.classList.contains(`kode4-${this.kode4ThemeType}-danger`);
        }
        set warning(value) {
            this.toggleCssClass(value, `kode4-${this.kode4ThemeType}-warning`);
        }
        get warning() {
            return this.classList.contains(`kode4-${this.kode4ThemeType}-warning`);
        }
        set info(value) {
            this.toggleCssClass(value, `kode4-${this.kode4ThemeType}-info`);
        }
        get info() {
            return this.classList.contains(`kode4-${this.kode4ThemeType}-info`);
        }
        set text(value) {
            this.toggleCssClass(value, 'kode4-text');
        }
        get text() {
            return this.classList.contains('kode4-text');
        }
        set textPrimary(value) {
            this.toggleCssClass(value, 'kode4-text-primary');
        }
        get textPrimary() {
            return this.classList.contains('kode4-text-primary');
        }
        set textSecondary(value) {
            this.toggleCssClass(value, 'kode4-text-secondary');
        }
        get textSecondary() {
            return this.classList.contains('kode4-text-secondary');
        }
        set textSuccess(value) {
            this.toggleCssClass(value, 'kode4-text-success');
        }
        get textSuccess() {
            return this.classList.contains('kode4-text-success');
        }
        set textDanger(value) {
            this.toggleCssClass(value, 'kode4-text-danger');
        }
        get textDanger() {
            return this.classList.contains('kode4-text-danger');
        }
        set textWarning(value) {
            this.toggleCssClass(value, 'kode4-text-warning');
        }
        get textWarning() {
            return this.classList.contains('kode4-text-warning');
        }
        set textInfo(value) {
            this.toggleCssClass(value, 'kode4-text-info');
        }
        get textInfo() {
            return this.classList.contains('kode4-text-info');
        }
        set bg(value) {
            this.toggleCssClass(value, 'kode4-bg');
        }
        get bg() {
            return this.classList.contains('kode4-bg');
        }
        set bgPrimary(value) {
            this.toggleCssClass(value, 'kode4-bg-primary');
        }
        get bgPrimary() {
            return this.classList.contains('kode4-bg-primary');
        }
        set bgSecondary(value) {
            this.toggleCssClass(value, 'kode4-bg-secondary');
        }
        get bgSecondary() {
            return this.classList.contains('kode4-bg-secondary');
        }
        set bgSuccess(value) {
            this.toggleCssClass(value, 'kode4-bg-success');
        }
        get bgSuccess() {
            return this.classList.contains('kode4-bg-success');
        }
        set bgDanger(value) {
            this.toggleCssClass(value, 'kode4-bg-danger');
        }
        get bgDanger() {
            return this.classList.contains('kode4-bg-danger');
        }
        set bgWarning(value) {
            this.toggleCssClass(value, 'kode4-bg-warning');
        }
        get bgWarning() {
            return this.classList.contains('kode4-bg-warning');
        }
        set bgInfo(value) {
            this.toggleCssClass(value, 'kode4-bg-info');
        }
        get bgInfo() {
            return this.classList.contains('kode4-bg-info');
        }
    }
    __decorate([
        property({ type: String, attribute: 'theme' })
    ], ApplyKode4Theme.prototype, "kode4ThemeType", void 0);
    __decorate([
        property({ type: Boolean, attribute: 'primary' })
    ], ApplyKode4Theme.prototype, "primary", null);
    __decorate([
        property({ type: Boolean, attribute: 'secondary' })
    ], ApplyKode4Theme.prototype, "secondary", null);
    __decorate([
        property({ type: Boolean, attribute: 'success' })
    ], ApplyKode4Theme.prototype, "success", null);
    __decorate([
        property({ type: Boolean, attribute: 'danger' })
    ], ApplyKode4Theme.prototype, "danger", null);
    __decorate([
        property({ type: Boolean, attribute: 'warning' })
    ], ApplyKode4Theme.prototype, "warning", null);
    __decorate([
        property({ type: Boolean, attribute: 'info' })
    ], ApplyKode4Theme.prototype, "info", null);
    __decorate([
        property({ type: Boolean, attribute: 'text' })
    ], ApplyKode4Theme.prototype, "text", null);
    __decorate([
        property({ type: Boolean, attribute: 'text-primary' })
    ], ApplyKode4Theme.prototype, "textPrimary", null);
    __decorate([
        property({ type: Boolean, attribute: 'text-secondary' })
    ], ApplyKode4Theme.prototype, "textSecondary", null);
    __decorate([
        property({ type: Boolean, attribute: 'text-success' })
    ], ApplyKode4Theme.prototype, "textSuccess", null);
    __decorate([
        property({ type: Boolean, attribute: 'text-danger' })
    ], ApplyKode4Theme.prototype, "textDanger", null);
    __decorate([
        property({ type: Boolean, attribute: 'text-warning' })
    ], ApplyKode4Theme.prototype, "textWarning", null);
    __decorate([
        property({ type: Boolean, attribute: 'text-info' })
    ], ApplyKode4Theme.prototype, "textInfo", null);
    __decorate([
        property({ type: Boolean, attribute: 'bg' })
    ], ApplyKode4Theme.prototype, "bg", null);
    __decorate([
        property({ type: Boolean, attribute: 'bg-primary' })
    ], ApplyKode4Theme.prototype, "bgPrimary", null);
    __decorate([
        property({ type: Boolean, attribute: 'bg-secondary' })
    ], ApplyKode4Theme.prototype, "bgSecondary", null);
    __decorate([
        property({ type: Boolean, attribute: 'bg-success' })
    ], ApplyKode4Theme.prototype, "bgSuccess", null);
    __decorate([
        property({ type: Boolean, attribute: 'bg-danger' })
    ], ApplyKode4Theme.prototype, "bgDanger", null);
    __decorate([
        property({ type: Boolean, attribute: 'bg-warning' })
    ], ApplyKode4Theme.prototype, "bgWarning", null);
    __decorate([
        property({ type: Boolean, attribute: 'bg-info' })
    ], ApplyKode4Theme.prototype, "bgInfo", null);
    // Cast return type to the superClass type passed in
    return ApplyKode4Theme;
};
