import { LitElement } from "lit";
export declare class Kode4Row extends LitElement {
    spacing: boolean;
    private updateCssClass;
    protected updated(changedProperties: Map<PropertyKey, unknown>): void;
    render(): import("lit-html").TemplateResult<1>;
    static styles: import("lit").CSSResult[];
}
