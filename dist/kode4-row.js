import { __decorate } from "tslib";
import { LitElement, html, css } from "lit";
import { customElement, property } from "lit/decorators.js";
import kode4CssTheme from "./kode4-css";
let Kode4Row = class Kode4Row extends LitElement {
    constructor() {
        super(...arguments);
        this.spacing = false;
    }
    updateCssClass(changedProperties, prop, value, cssClass) {
        if (changedProperties.has(prop)) {
            value ? this.classList.add(cssClass) : this.classList.remove(cssClass);
        }
    }
    updated(changedProperties) {
        super.updated(changedProperties);
        this.updateCssClass(changedProperties, 'spacing', this.spacing, 'kode4-spacing');
    }
    render() {
        return html `
            <div class="center-title" part="title">
                <slot name="title"></slot>
            </div>
            <slot></slot>
        `;
    }
};
Kode4Row.styles = [
    kode4CssTheme,
    css `
            * {
                box-sizing: border-box;
            }

            :host {
                position: relative;
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;
                align-items: center;
            }

            :host(.kode4-spacing) {
                gap: var(--kode4-toolbar-spacing);
            }
            
            ::slotted(*:not(kode4-column)) {
                position: relative;
                //align-self: center;
                //display: flex;
                //flex-direction: row;
                //flex-wrap: nowrap;
                //align-items: center;
            }

            ::slotted(kode4-column) {
                align-self: stretch;
                height: auto;
            }

            ::slotted(.kode4-stretch) {
                flex: 1 0 1px;
            }
            
            .center-title {
                position: absolute;
                left: 0;
                right: 0;
                margin-left: auto;
                margin-right: auto;
                top: 0;
                bottom: 0;
                display: inline-flex;
                flex-direction: row;
                flex-wrap: nowrap;
                align-items: center;
                justify-content: center;
            }
        `,
];
__decorate([
    property({ type: Boolean, attribute: true })
], Kode4Row.prototype, "spacing", void 0);
Kode4Row = __decorate([
    customElement('kode4-row')
], Kode4Row);
export { Kode4Row };
