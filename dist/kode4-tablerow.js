import { __decorate } from "tslib";
import { LitElement, html, css } from "lit";
import { customElement } from "lit/decorators.js";
import kode4CssTheme from "./kode4-css";
let Kode4Tablerow = class Kode4Tablerow extends LitElement {
    // @queryAssignedElements()
    // private listBodyElements?: Array<HTMLElement>;
    // @state()
    // private get empty():boolean {
    //     if (!this.listBodyElements || !!this.listBodyElements.length) {
    //         return true;
    //     }
    //     return false;
    // }
    render() {
        return html `
            <slot></slot>
        `;
    }
};
Kode4Tablerow.styles = [
    kode4CssTheme,
    css `
            * {
                box-sizing: border-box;
            }

            :host {
                display: table-row;
                background-color: inherit;
            }
            
            :host(:nth-child(odd)) {
                background-color: var(--kode4-tablerow-background-odd);
            }
            
            :host(:nth-child(even)) {
                background-color: var(--kode4-tablerow-background-even);
            }
            
            :host(:hover) {
                background-color: var(--kode4-tablerow-background-hover);
            }
            
            :host([highlighted]) {
                background-color: var(--kode4-tablerow-background-highlighted);
            }
            
            ::slotted(*) {
                display: table-cell;
                padding-top: var(--kode4-tablerow-padding-top);
                padding-right: var(--kode4-tablerow-padding-right);
                padding-bottom: var(--kode4-tablerow-padding-bottom);
                padding-left: var(--kode4-tablerow-padding-left);
                border-bottom: var(--kode4-tablerow-border-bottom);
            }
            
            ::slotted(*) {
                display: table-cell;
            }
            
            ::slotted(:first-child) {
                padding-left: var(--kode4-tablerow-first-padding-left);
            }

            ::slotted(:last-child) {
                padding-right: var(--kode4-tablerow-last-padding-right);
                border: none;
            }
        `
];
Kode4Tablerow = __decorate([
    customElement('kode4-tablerow')
], Kode4Tablerow);
export default Kode4Tablerow;
