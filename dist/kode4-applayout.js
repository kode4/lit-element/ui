import { __decorate } from "tslib";
import { LitElement, html, css } from "lit";
import { customElement, query } from "lit/decorators.js";
let Kode4Applayout = class Kode4Applayout extends LitElement {
    scrollBodyTo(pos) {
        this.bodyElement.scrollTo(0, pos);
    }
    onBodyScrolled() {
        this.dispatchEvent(new CustomEvent('kode4-body-scrolled', {
            bubbles: true,
            composed: true,
            detail: {
                pos: this.bodyElement.scrollTop,
            }
        }));
    }
    render() {
        return html `
            <div class="kode4-applayout-header">
                <slot name="header"></slot>
            </div>
            
            <div class="kode4-applayout-progressbar-top">
                <div class="kode4-applayout-progressbar-top-content">
                    <slot name="progressbar-top"></slot>
                </div>
            </div>
            
            <div class="kode4-applayout-tray-left">
                <slot name="tray-left"></slot>
            </div>
            
            <div class="kode4-applayout-menu-left">
                <slot name="menu-left""></slot>
            </div>

            <div class="kode4-applayout-body" @scroll="${this.onBodyScrolled}">
                <slot name="body"></slot>
            </div>

            <div class="kode4-applayout-menu-right">
                <slot name="menu-right"></slot>
            </div>
            
            <div class="kode4-applayout-tray-right">
                <slot name="tray-right"></slot>
            </div>

            <div class="kode4-applayout-progressbar-bottom">
                <div class="kode4-applayout-progressbar-bottom-content">
                    <slot name="progressbar-bottom"></slot>
                </div>
            </div>

            <div class="kode4-applayout-footer">
                <slot name="footer"></slot>
            </div>
        `;
    }
};
Kode4Applayout.styles = [
    css `
            * {
                box-sizing: border-box;
            }

            :host {
                display: grid;
                //position: relative;
                width: 100vw;
                height: 100vh;
                overflow: hidden;
                grid-template-rows: [header] auto [progressbar-top] auto [body] 1fr [progressbar-bottom] auto [footer] auto [end];
                grid-template-columns: [tray-left] auto [menu-left] auto [body] 1fr [menu-right] auto [tray-right] auto;
            }
            
            .kode4-applayout-header {
                grid-column-start: tray-left;
                grid-column-end: end;
                grid-row: header;
                //z-index: 90;
            }

            .kode4-applayout-tray-left {
                grid-column: tray-left;
                grid-row: body;
                writing-mode: vertical-lr;
                text-orientation: mixed;
                //z-index: 70;
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;
                align-items: center;
            }

            .kode4-applayout-tray-left ::slotted(:not(.kode4-applayout-tray-left-norotate)) {
                display: inline-block;
                transform: rotate(180deg);
            }

            .kode4-applayout-menu-left {
                grid-column: menu-left;
                grid-row: body;
                //z-index: 50;
            }

            div.kode4-applayout-menu-left {
                display: flex;
                flex-direction: row;
                height: 100%;
            }

            .kode4-applayout-body {
                grid-column: body;
                grid-row: body;
                overflow-x: hidden;
                overflow-y: auto;
                //z-index: 100;
            }

            .kode4-applayout-menu-right {
                grid-column: menu-right;
                grid-row: body;
                //z-index: 50;
            }

            div.kode4-applayout-menu-right {
                display: flex;
                flex-direction: row;
                height: 100%;
            }

            .kode4-applayout-tray-right {
                grid-column: tray-right;
                grid-row: body;
                writing-mode: vertical-rl;
                text-orientation: mixed;
                //z-index: 70;
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;
                align-items: center;
            }

            .kode4-applayout-footer {
                grid-column-start: tray-left;
                grid-column-end: end;
                grid-row: footer;
                //z-index: 90;
            }

            .kode4-applayout-progressbar-top {
                grid-column-start: tray-left;
                grid-column-end: end;
                grid-row: progressbar-top;
                position: relative;
            }
            
            .kode4-applayout-progressbar-top-content {
                position: absolute;
                left: 0;
                right: 0;
                top: 0;
                //z-index: 150;
            }

            .kode4-applayout-progressbar-bottom {
                grid-column-start: tray-left;
                grid-column-end: end;
                grid-row: progressbar-bottom;
                position: relative;
            }
            
            .kode4-applayout-progressbar-bottom-content {
                position: absolute;
                left: 0;
                right: 0;
                bottom: 0;
                //z-index: 150;
            }
        `,
];
__decorate([
    query('.kode4-applayout-body')
], Kode4Applayout.prototype, "bodyElement", void 0);
Kode4Applayout = __decorate([
    customElement('kode4-applayout')
], Kode4Applayout);
export default Kode4Applayout;
