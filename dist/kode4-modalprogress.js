import { __decorate } from "tslib";
import { LitElement, html, css } from "lit";
import { customElement, property } from "lit/decorators.js";
import kode4CssTheme from './kode4-css';
let Kode4ModalProgress = class Kode4ModalProgress extends LitElement {
    constructor() {
        super(...arguments);
        this.color = '';
        // @property({ type: Boolean })
        // protected isOpen:Boolean = false;
        this._open = false;
        this.opening = false;
        this.closing = false;
    }
    get open() {
        return this._open;
    }
    set open(open) {
        this._open = open;
    }
    // public firstUpdated(changedProperties:PropertyValues) {
    //     super.firstUpdated(changedProperties);
    // }
    // protected onClose(e:Event) {
    //     let event = new CustomEvent("close", {
    //         detail: {
    //             open: this.open,
    //         }
    //     });
    //     this.dispatchEvent(event);
    // }
    // protected onOpen(e:Event) {
    //     let event = new CustomEvent("open", {
    //         detail: {
    //             open: this.isOpen,
    //         }
    //     });
    //     this.dispatchEvent(event);
    // }
    // public open() {
    //     this.isOpen = true;
    // }
    // public close() {
    //     this.isOpen = false;
    // }
    // public toggle() {
    //     this.isOpen ? this.close() : this.open();
    // }
    render() {
        return html `
            <div class="backdrop ${this.open ? 'visible' : ''} ${this.opening ? 'opening' : ''} ${this.closing ? 'closing' : ''}">
                ${this.open ? html `
                    <div class="content" @click="${(e) => e.stopPropagation()}">        
                        <slot>
                            <kode4-progress type="icon" color="${this.color}" indeterminate></kode4-progress>            
                        </slot>
                    </div>
                ` : ''}
            </div>
        `;
    }
};
Kode4ModalProgress.styles = [
    kode4CssTheme,
    css `
            :host {
                width: 0;
                height: 0;
            }

            .backdrop {
                display: flex;
                visibility: visible;
                opacity: 0;
                position: fixed;
                top: 0;
                bottom: auto;
                left: 0;
                right: auto;
                width: 0;
                height: 0;
                overflow: hidden;
                background-color: rgba(0, 0, 0, 0.25);
                align-items: center;
                justify-items: center;
                z-index: 5000;
                transition: opacity 0.25s linear, bottom 0s linear 0.25s, right 0s linear 0.25s, width 0s linear 0.25s, height 0s linear 0.25s;

                -moz-user-select: none;
                -khtml-user-select: none;
                -webkit-user-select: none;
                user-select: none;
                /* Required to make elements draggable in old WebKit */
                -khtml-user-drag: element;
                -webkit-user-drag: element;
            }

            .content {
                background-color: transparent;
                max-width: 90vw;
                max-height: 90vh;
                z-index: 5010;
                margin-left: auto;
                margin-right: auto;
                padding: 0;
                border-radius: 2px;
                text-shadow: 2px 2px 20px #888;
                font-size: 5vmin;
            }

            .backdrop.visible {
                opacity: 1;
            }

            .backdrop.visible,
            .backdrop.opening,
            .backdrop.closing {
                transition: opacity 0.25s linear, bottom 0s linear 0.25s, right 0s linear 0.25s, width 0s linear 0.25s, height 0s linear 0.25s;
                bottom: 0;
                right: 0;
                width: auto;
                height: auto;
            }
        `,
];
__decorate([
    property({ type: String })
], Kode4ModalProgress.prototype, "color", void 0);
__decorate([
    property({ type: Boolean })
], Kode4ModalProgress.prototype, "_open", void 0);
__decorate([
    property({ type: Boolean })
], Kode4ModalProgress.prototype, "open", null);
__decorate([
    property({ type: Boolean })
], Kode4ModalProgress.prototype, "opening", void 0);
__decorate([
    property({ type: Boolean })
], Kode4ModalProgress.prototype, "closing", void 0);
Kode4ModalProgress = __decorate([
    customElement('kode4-modalprogress')
], Kode4ModalProgress);
export default Kode4ModalProgress;
