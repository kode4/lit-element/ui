import { ListLimitReachedError } from "./ListStateHandler";
import debounce from "lodash-es/debounce";
export class ListHandler {
    constructor(params) {
        this.observers = [];
        this.list = [];
        this.debouncedLoadList = debounce(this.loadList, 50);
        this.fireListChanged = () => {
            this.observers.forEach((observer) => {
                observer.onListChange(this.list);
            });
        };
        this.lsh = params.listStateHandler;
        this.lsh.observe(this);
        this.storage = params.storage;
        this.storageKey = params.storageKey;
        if (params.observer) {
            this.observe(params.observer);
        }
    }
    // =====[ List and Item Operations ]================================================================================
    async loadList(forceReload = false) {
        try {
            this.lsh.lshState = 'busy';
            const params = {};
            const lshParams = this.lsh.getRequestParameters();
            if (forceReload || !this.currentHash || lshParams.hash !== this.currentHash) {
                if (lshParams.orderBy) {
                    params.orderBy = lshParams.orderBy;
                }
                params.limit = lshParams.pageSize;
                params.offset = lshParams.page;
                const newData = await this.performLoadList(params, lshParams.filters);
                if (newData.list) {
                    this.list = [
                        ...this.list,
                        ...newData.list,
                    ];
                }
                this.lsh.size = this.list.length;
                this.lsh.total = newData.total;
                this.currentHash = this.lsh.hash;
                this.lsh.store();
                this.fireListChanged();
            }
            else {
                this.fireListChanged();
            }
        }
        catch (e) {
            if (e instanceof ListLimitReachedError) {
            }
        }
        finally {
            this.lsh.lshState = 'idle';
        }
    }
    async loadItem(searchParams) {
        try {
            this.lsh.lshState = 'busy';
            return await this.performLoadItem(searchParams);
        }
        catch (e) {
            console.error(e);
            throw e;
        }
        finally {
            this.lsh.lshState = 'idle';
        }
    }
    async loadItemAndUpdateInMemory(searchParams) {
        this.updateItemInMemory(searchParams, await this.performLoadItem(searchParams));
    }
    async updateItem(searchParams, newData, refreshList = false) {
        try {
            this.lsh.lshState = 'busy';
            await this.performUpdateItem(searchParams, newData);
            const currentItem = this.findItem(searchParams);
            if (currentItem) {
                this.loadItemAndUpdateInMemory(searchParams);
            }
        }
        catch (e) {
            console.error(e);
            throw e;
        }
        finally {
            this.lsh.lshState = 'idle';
            if (!refreshList) {
                this.fireListChanged();
            }
            else {
                this.lsh.refreshList();
            }
        }
    }
    async deleteItem(searchParams, refreshList = true) {
        try {
            this.lsh.lshState = 'busy';
            await this.performDeleteItem(searchParams);
        }
        catch (e) {
            if (e instanceof ListLimitReachedError) {
            }
        }
        finally {
            this.lsh.lshState = 'idle';
            if (!refreshList) {
                this.deleteItemInMemory(searchParams);
                this.fireListChanged();
            }
            else {
                this.lsh.refreshList();
            }
        }
    }
    // =====[ Events ]==================================================================================================
    observe(observer) {
        if (!this.observers.includes(observer)) {
            this.observers.push(observer);
            observer.onListChange(this.list);
        }
    }
    unobserve(observer) {
        if (!observer) {
            this.observers = [];
        }
        else {
            this.observers = [
                ...this.observers.filter((el) => el !== observer)
            ];
        }
    }
    // private debouncedFireListChanged = debounce(this.fireListChanged, 50);
    // =====[ IListStateObserver ]======================================================================================
    onFilterChange() {
        this.list = [];
        this.fireListChanged();
    }
    onOrderChange() {
        this.list = [];
        this.fireListChanged();
    }
    onPaginationChange() {
        // this.loadList();
        this.debouncedLoadList();
    }
    onRestore() {
        // this.loadList();
        this.debouncedLoadList();
    }
    onReset() {
        this.list = [];
        // this.loadList();
        this.debouncedLoadList();
    }
    onRefresh(forceReload = false) {
        if (forceReload) {
            this.list = [];
        }
        this.loadList(forceReload);
    }
    // =====[ Helpers ]=================================================================================================
    itemMatchesSearchParams(item, searchParams) {
        if (!Object.keys(searchParams).length) {
            return false;
        }
        let match = false;
        for (const [key, value] of Object.entries(item)) {
            if (searchParams[key] && value == searchParams[key]) {
                match = true;
            }
        }
        return match;
    }
    findItem(searchParams) {
        return this.list.find((item) => this.itemMatchesSearchParams(item, searchParams));
    }
    updateItemInMemory(searchParams, newItem) {
        this.list = [
            ...this.list.map((item) => this.itemMatchesSearchParams(item, searchParams) ? newItem : item)
        ];
        this.fireListChanged();
    }
    deleteItemInMemory(searchParams) {
        this.list = [
            ...this.list.filter((item) => !this.itemMatchesSearchParams(item, searchParams))
        ];
    }
}
