import { __decorate } from "tslib";
import { LitElement, html, css } from "lit";
import { customElement, property, queryAssignedElements, state } from "lit/decorators.js";
import kode4CssTheme from "./kode4-css";
import { v4 as uuidv4 } from "uuid";
let Kode4Tab = class Kode4Tab extends LitElement {
    constructor() {
        super(...arguments);
        this.name = `kode4tab_${uuidv4()}`;
    }
    get tab() {
        var _a;
        if ((_a = this.titleElements) === null || _a === void 0 ? void 0 : _a.length) {
            return html `${this.titleElements}`;
        }
        return html `${this.name}`;
    }
    firstUpdated(_changedProperties) {
        super.firstUpdated(_changedProperties);
    }
    render() {
        return html `
            <div class="title">
                <slot name="title"></slot>
            </div>
                
            <slot></slot>
        `;
    }
};
Kode4Tab.styles = [
    kode4CssTheme,
    css `
            * {
                box-sizing: border-box;
            }

            :host {
                display: block;
            }
            
            .title {
                display: none;
                visibility: hidden;
                width: 0;
                height: 0;
                overflow: hidden;
            }
        `,
];
__decorate([
    queryAssignedElements({ slot: 'title' })
], Kode4Tab.prototype, "titleElements", void 0);
__decorate([
    property()
], Kode4Tab.prototype, "name", void 0);
__decorate([
    state()
], Kode4Tab.prototype, "tab", null);
Kode4Tab = __decorate([
    customElement('kode4-tab')
], Kode4Tab);
export default Kode4Tab;
