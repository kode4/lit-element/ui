import { __decorate } from "tslib";
import { LitElement, html, css } from "lit";
import { customElement, property } from "lit/decorators.js";
import kode4CssTheme from "./kode4-css";
let Kode4Column = class Kode4Column extends LitElement {
    constructor() {
        super(...arguments);
        this.spacing = false;
    }
    updateCssClass(changedProperties, prop, value, cssClass) {
        if (changedProperties.has(prop)) {
            value ? this.classList.add(cssClass) : this.classList.remove(cssClass);
        }
    }
    updated(changedProperties) {
        super.updated(changedProperties);
        this.updateCssClass(changedProperties, 'spacing', this.spacing, 'kode4-spacing');
    }
    render() {
        return html `
            <slot></slot>
        `;
    }
};
Kode4Column.styles = [
    kode4CssTheme,
    css `
            * {
                box-sizing: border-box;
            }

            :host {
                display: flex;
                flex-direction: column;
                flex-wrap: nowrap;
                height: 100%;
            }

            :host(.kode4-spacing) {
                gap: var(--kode4-toolbar-spacing);
            }

            ::slotted(*:not(kode4-row)) {
                position: relative;
                //align-self: stretch;
                //display: flex;
                //flex-direction: column;
                //flex-wrap: nowrap;
            }

            ::slotted(.kode4-stretch) {
                flex: 1 0 1px;
                place-self: flex-end stretch;
            }
        `,
];
__decorate([
    property({ type: Boolean, attribute: true })
], Kode4Column.prototype, "spacing", void 0);
Kode4Column = __decorate([
    customElement('kode4-column')
], Kode4Column);
export default Kode4Column;
