import { LitElement } from "lit";
export default class Kode4Applayout extends LitElement {
    private bodyElement;
    scrollBodyTo(pos: number): void;
    onBodyScrolled(): void;
    render(): import("lit-html").TemplateResult<1>;
    static styles: import("lit").CSSResult[];
}
