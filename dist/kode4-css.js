import { css } from "lit";
export default css `
    .kode4-text-primary {
        --main-color: var(--main-color-primary);
        --main-color-hover: var(--main-color-hover-primary);
    }

    .kode4-bg-primary {
        --main-color-filled: var(--main-color-filled-primary);
        --main-color-filled-hover: var(--main-color-filled-hover-primary);
        --background-color-filled: var(--background-color-filled-primary);
        --background-color-filled-hover: var(--background-color-filled-hover-primary);
    }

    .kode4-text-secondary {
        --main-color: var(--main-color-secondary);
        --main-color-hover: var(--main-color-hover-secondary);
    }

    .kode4-bg-secondary {
        --main-color-filled: var(--main-color-filled-secondary);
        --main-color-filled-hover: var(--main-color-filled-hover-secondary);
        --background-color-filled: var(--background-color-filled-secondary);
        --background-color-filled-hover: var(--background-color-filled-hover-secondary);
    }

    .kode4-text-success {
        --main-color: var(--main-color-success);
        --main-color-hover: var(--main-color-hover-success);
    }

    .kode4-bg-success {
        --main-color-filled: var(--main-color-filled-success);
        --main-color-filled-hover: var(--main-color-filled-hover-success);
        --background-color-filled: var(--background-color-filled-success);
        --background-color-filled-hover: var(--background-color-filled-hover-success);
    }

    .kode4-text-danger {
        --main-color: var(--main-color-danger);
        --main-color-hover: var(--main-color-hover-danger);
    }

    .kode4-bg-danger {
        --main-color-filled: var(--main-color-filled-danger);
        --main-color-filled-hover: var(--main-color-filled-hover-danger);
        --background-color-filled: var(--background-color-filled-danger);
        --background-color-filled-hover: var(--background-color-filled-hover-danger);
    }

    .kode4-text-warning {
        --main-color: var(--main-color-warning);
        --main-color-hover: var(--main-color-hover-warning);
    }

    .kode4-bg-warning {
        --main-color-filled: var(--main-color-filled-warning);
        --main-color-filled-hover: var(--main-color-filled-hover-warning);
        --background-color-filled: var(--background-color-filled-warning);
        --background-color-filled-hover: var(--background-color-filled-hover-warning);
    }

    .kode4-text-info {
        --main-color: var(--main-color-info);
        --main-color-hover: var(--main-color-hover-info);
    }

    .kode4-bg-info {
        --main-color-filled: var(--main-color-filled-info);
        --main-color-filled-hover: var(--main-color-filled-hover-info);
        --background-color-filled: var(--background-color-filled-info);
        --background-color-filled-hover: var(--background-color-filled-hover-info);
    }


    .kode4-text,
    .kode4-text-primary,
    .kode4-text-secondary,
    .kode4-text-success,
    .kode4-text-danger,
    .kode4-text-warning,
    .kode4-text-info {
        color: var(--main-color);
    }

    a.kode4-text:hover,
    a.kode4-text-primary:hover,
    a.kode4-text-secondary:hover,
    a.kode4-text-success:hover,
    a.kode4-text-danger:hover,
    a.kode4-text-warning:hover,
    a.kode4-text-info:hover,
    .kode4-text-hover:hover {
        color: var(--main-color-hover);
    }

    .kode4-bg,
    .kode4-bg-primary,
    .kode4-bg-secondary,
    .kode4-bg-success,
    .kode4-bg-danger,
    .kode4-bg-warning,
    .kode4-bg-info {
        color: var(--main-color-filled);
        background-color: var(--background-color-filled);
    }

    a.kode4-bg:hover,
    a.kode4-bg-primary:hover,
    a.kode4-bg-secondary:hover,
    a.kode4-bg-success:hover,
    a.kode4-bg-danger:hover,
    a.kode4-bg-warning:hover,
    a.kode4-bg-info:hover,
    .kode4-bg-hover:hover{
        color: var(--main-color-filled-hover);
        background-color: var(--background-color-filled-hover);
    }

    .kode4-alert {
        padding: 10px;
        color: var(--main-color-filled);
        background-color: var(--background-color-filled);
    }

    a.kode4-alert:hover,
    .kode4-alert-hover:hover {
        color: var(--main-color-filled-hover);
        background-color: var(--background-color-filled-hover);
    }



    /* HELPERS FOR CENTERING */
    .kode4-center-v {
        display: flex;
        flex-direction: row;
        flex-wrap: nowrap;
        align-items: center;
        height: 100%;
    }

    .kode4-center-h {
        display: flex;
        flex-direction: column;
        flex-wrap: nowrap;
        align-items: center;
    }

    .kode4-center {
        display: flex;
        flex-direction: row;
        flex-wrap: nowrap;
        align-items: center;
        justify-content: center;
    }

    .kode4-center-full {
        display: flex;
        flex-direction: row;
        flex-wrap: nowrap;
        align-items: center;
        justify-content: center;
        height: 100%;
    }

    .kode4-spacer {
        flex: 1 0 1px;
    }

`;
