import { LitElement } from "lit";
import { IKode4GrowlMessengerObserver, Kode4GrowlMessage } from "./kode4-growl";
export default class Kode4GrowlAreas extends LitElement implements IKode4GrowlMessengerObserver {
    private growlT?;
    private growlTl?;
    private growlTr?;
    private growlB?;
    private growlBl?;
    private growlBr?;
    connectedCallback(): void;
    disconnectedCallback(): void;
    onGrowlAlert(message: Kode4GrowlMessage): void;
    alert(message: Kode4GrowlMessage): void;
    render(): import("lit-html").TemplateResult<1>;
    static styles: import("lit").CSSResult[];
}
