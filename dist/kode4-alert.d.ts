import { LitElement } from "lit";
declare const Kode4Alert_base: (new (...args: any[]) => import("./kode4-theme").IApplyKode4Theme) & typeof LitElement;
export declare class Kode4Alert extends Kode4Alert_base {
    constructor();
    render(): import("lit-html").TemplateResult<1>;
    static styles: import("lit").CSSResult[];
}
export {};
