import { LitElement, HTMLTemplateResult, PropertyValues } from "lit";
export default class Kode4Tab extends LitElement {
    private titleElements?;
    name: string;
    get tab(): HTMLTemplateResult;
    protected firstUpdated(_changedProperties: PropertyValues): void;
    render(): import("lit-html").TemplateResult<1>;
    static styles: import("lit").CSSResult[];
}
