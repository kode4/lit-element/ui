import { LitElement, PropertyValues } from "lit";
export default class Kode4Table extends LitElement {
    listEndObserverTrigger: HTMLElement;
    listEndObserverRootSelector?: string;
    observerRoot?: HTMLElement;
    observerFix: boolean;
    more?: Function;
    changed?: Function;
    private listBodyElements?;
    private get empty();
    private listEndObserverOptions;
    private listEndObserver;
    protected firstUpdated(_changedProperties: PropertyValues): Promise<void>;
    triggerObserverHandler(): Promise<void>;
    private observerIsRunning;
    stopObserver(): void;
    private currentObservedElement?;
    startObserver(): void;
    disconnectedCallback(): void;
    handleSlotchange(): void;
    render(): import("lit-html").TemplateResult<1>;
    static styles: import("lit").CSSResult[];
}
