import { LitElement } from "lit";
export default class Kode4Nav extends LitElement {
    href: String | undefined;
    target: String | undefined;
    private handleLinkClick;
    render(): import("lit-html").TemplateResult<1>;
    protected renderSimple(): import("lit-html").TemplateResult<1>;
    protected renderLink(): import("lit-html").TemplateResult<1>;
    static styles: import("lit").CSSResult[];
}
