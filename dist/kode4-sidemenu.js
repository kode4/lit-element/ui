import { __decorate } from "tslib";
import { LitElement, html, css } from "lit";
import { customElement, property, state, queryAssignedElements } from "lit/decorators.js";
let Kode4Sidemenu = class Kode4Sidemenu extends LitElement {
    constructor() {
        super(...arguments);
        this.open = false;
        this.alwaysopen = false;
        this.absolute = false;
        this.left = true;
        this.right = false;
        this.shadow = false;
        this.animations = true;
        this.animationEnabled = false;
        this.resizeContentObserver = new ResizeObserver(() => {
            this.requestUpdate();
        });
    }
    get contentWidth() {
        var _a;
        let maxWidth = 100;
        (_a = [...this.headerItems, ...this.mainItems, ...this.footerItems]) === null || _a === void 0 ? void 0 : _a.forEach((el) => {
            maxWidth = Math.max(maxWidth, el.offsetWidth);
        });
        return `${maxWidth}px`;
    }
    get cssClassLeftRight() {
        if (this.right) {
            return 'right';
        }
        return 'left';
    }
    firstUpdated(_changedProperties) {
        var _a;
        super.firstUpdated(_changedProperties);
        (_a = [...this.headerItems, ...this.mainItems, ...this.footerItems]) === null || _a === void 0 ? void 0 : _a.forEach((el) => {
            this.resizeContentObserver.observe(el);
        });
        if (this.animations) {
            setTimeout(() => {
                this.animationEnabled = true;
            }, 100);
        }
    }
    connectedCallback() {
        super.connectedCallback();
    }
    disconnectedCallback() {
        // this.resizeContentObserver.unobserve([...this.headerItems, ...this.mainItems, ...this.footerItems]);
        this.resizeContentObserver.disconnect();
        super.disconnectedCallback();
    }
    render() {
        return html `
            <div class="body ${this.open || this.alwaysopen ? 'open' : ''} ${this.alwaysopen ? 'alwaysopen' : ''} ${this.absolute ? 'absolute' : ''} ${this.cssClassLeftRight} ${this.shadow ? 'shadow' : ''} ${this.animationEnabled ? 'animate' : ''}" style="width: ${this.contentWidth};">
                <kode4-column class="bodycontent" part="container">
                    <slot name="header"></slot>
                    <div style="flex: 1 0 1px; overflow: auto;">
                        <slot class="kode4-stretch"></slot>
                    </div>
                    <slot name="footer"></slot>
                </kode4-column>
            </div>
        `;
    }
};
Kode4Sidemenu.styles = [
    css `
            * {
                box-sizing: border-box;
            }
            
            :host {
                display: block;
                padding: 0;
                position: relative;
            }

            .body {
                display: block;
                position: relative;
                overflow-y: hidden;
                height: 100%;
            }
            
            .body.animate:not(.alwaysopen) {
                transition: all 0.25s linear;
            }
            
            .body.shadow.left {
                box-shadow: 2px 0 6px 0 rgba(0, 0, 0, 0.2);
            }
            
            .body.shadow.right {
                box-shadow: -2px 0 6px 0 rgba(0, 0, 0, 0.2);
            }
            
            .bodycontent {
                position: absolute;
                bottom: 0;
                top: 0;
                overflow: hidden;
            }
            
            .body.left .bodycontent {
                left: auto;
                right: 0;
            }
            
            .body.right .bodycontent {
                left: 0;
                right: auto;
            }
            
            .body.absolute {
                position: absolute;
                top: 0;
                bottom: 0;
                height: auto;
                overflow: visible;
            }

            .body.absolute.left {
                left: 0;
                right: auto;
            }

            .body.absolute.right {
                left: auto;
                right: 0;
            }

            .body:not(.open) {
                width: 0 !important;
                overflow: hidden;
            }
        `,
];
__decorate([
    queryAssignedElements({ slot: 'header' })
], Kode4Sidemenu.prototype, "headerItems", void 0);
__decorate([
    queryAssignedElements()
], Kode4Sidemenu.prototype, "mainItems", void 0);
__decorate([
    queryAssignedElements({ slot: 'footer' })
], Kode4Sidemenu.prototype, "footerItems", void 0);
__decorate([
    property({ type: Boolean, reflect: true })
], Kode4Sidemenu.prototype, "open", void 0);
__decorate([
    property({ type: Boolean, reflect: true })
], Kode4Sidemenu.prototype, "alwaysopen", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Sidemenu.prototype, "absolute", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Sidemenu.prototype, "left", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Sidemenu.prototype, "right", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Sidemenu.prototype, "shadow", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Sidemenu.prototype, "animations", void 0);
__decorate([
    state()
], Kode4Sidemenu.prototype, "animationEnabled", void 0);
__decorate([
    property({ type: Number })
], Kode4Sidemenu.prototype, "contentWidth", null);
__decorate([
    state()
], Kode4Sidemenu.prototype, "cssClassLeftRight", null);
Kode4Sidemenu = __decorate([
    customElement('kode4-sidemenu')
], Kode4Sidemenu);
export default Kode4Sidemenu;
