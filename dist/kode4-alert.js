import { __decorate } from "tslib";
// import { LitElement, html, property, customElement } from "lit";
import { LitElement, html, css } from "lit";
import { customElement } from "lit/decorators.js";
import kode4Css from "./kode4-css";
import { applyKode4Theme } from "./kode4-theme";
let Kode4Alert = class Kode4Alert extends applyKode4Theme(LitElement) {
    constructor() {
        super();
        this.kode4ThemeType = 'bg';
    }
    render() {
        return html `
            <slot></slot>
        `;
    }
};
Kode4Alert.styles = [
    kode4Css,
    css `
            :host {
                display: block;
                padding: 10px;
            }
        `
];
Kode4Alert = __decorate([
    customElement('kode4-alert')
], Kode4Alert);
export { Kode4Alert };
