import { LitElement } from "lit";
export default class Kode4Column extends LitElement {
    spacing: boolean;
    private updateCssClass;
    protected updated(changedProperties: Map<PropertyKey, unknown>): void;
    render(): import("lit-html").TemplateResult<1>;
    static styles: import("lit").CSSResult[];
}
