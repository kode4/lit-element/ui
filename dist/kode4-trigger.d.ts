import { LitElement } from "lit";
import './kode4-backdrop';
import './kode4-list';
export default class Kode4Trigger extends LitElement {
    menu?: string;
    drawer?: string;
    dialog?: string;
    mode: string;
    callerParams?: any;
    private boundToParent;
    private handleParentClick;
    connectedCallback(): void;
    disconnectedCallback(): void;
    render(): import("lit-html").TemplateResult<1>;
}
