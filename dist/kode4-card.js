import { __decorate } from "tslib";
import { LitElement, html, css } from "lit";
import { customElement, property, query, queryAssignedElements, state } from "lit/decorators.js";
let Kode4Card = class Kode4Card extends LitElement {
    constructor() {
        super(...arguments);
        this.fixedWidth = false;
        this.resizeContentObserver = new ResizeObserver(() => {
            this.requestUpdate();
        });
    }
    get contentWidth() {
        var _a;
        if (this.fixedWidth) {
            return '100%';
        }
        let maxWidth = this.offsetWidth;
        (_a = [...this.headerItems, ...this.mainItems, ...this.footerItems]) === null || _a === void 0 ? void 0 : _a.forEach((el) => {
            maxWidth = Math.max(maxWidth, el.offsetWidth);
        });
        return `${maxWidth}px`;
    }
    get contentHeight() {
        var _a;
        let maxHeight = this.offsetHeight;
        (_a = [...this.headerItems, ...this.mainItems, ...this.footerItems]) === null || _a === void 0 ? void 0 : _a.forEach((el) => {
            maxHeight = Math.max(maxHeight, el.offsetHeight);
        });
        return `${maxHeight}px`;
    }
    firstUpdated(_changedProperties) {
        var _a;
        super.firstUpdated(_changedProperties);
        (_a = [...this.headerItems, ...this.mainItems, ...this.footerItems]) === null || _a === void 0 ? void 0 : _a.forEach((el) => {
            this.resizeContentObserver.observe(el);
        });
    }
    scrollBodyTo(pos) {
        this.bodyElement.scrollTo(0, pos);
    }
    onBodyScrolled() {
        this.dispatchEvent(new CustomEvent('kode4-body-scrolled', {
            bubbles: true,
            composed: true,
            detail: {
                pos: this.bodyElement.scrollTop,
            }
        }));
    }
    connectedCallback() {
        super.connectedCallback();
    }
    disconnectedCallback() {
        this.resizeContentObserver.disconnect();
        super.disconnectedCallback();
    }
    render() {
        return html `
            <kode4-column part="container" style="width: ${this.contentWidth};">
                <slot name="header"></slot>
                <div class="body" part="body" @scroll="${this.onBodyScrolled}">
                    <slot></slot>
                </div>
                <div>
                    <slot name="footer"></slot>
                </div>
            </kode4-column>
        `;
    }
};
Kode4Card.styles = [
    css `
            * {
                box-sizing: border-box;
            }
            
            :host {
                display: block;
                padding: 0;
                margin: 0;
            }
            
            :host([stretch]) {
                height: 100%;
            }

            .body {
                flex: 0 1 100%;
                width: 100%;
                overflow-x: hidden;
                overflow-y: auto;
                //border: solid 2px #00f;
                //background-color: #ccc;
            }
        `,
];
__decorate([
    query('.body')
], Kode4Card.prototype, "bodyElement", void 0);
__decorate([
    queryAssignedElements({ slot: 'header' })
], Kode4Card.prototype, "headerItems", void 0);
__decorate([
    queryAssignedElements()
], Kode4Card.prototype, "mainItems", void 0);
__decorate([
    queryAssignedElements({ slot: 'footer' })
], Kode4Card.prototype, "footerItems", void 0);
__decorate([
    property({ type: Boolean, attribute: 'fixed-width' })
], Kode4Card.prototype, "fixedWidth", void 0);
__decorate([
    state()
], Kode4Card.prototype, "contentWidth", null);
__decorate([
    state()
], Kode4Card.prototype, "contentHeight", null);
Kode4Card = __decorate([
    customElement('kode4-card')
], Kode4Card);
export default Kode4Card;
