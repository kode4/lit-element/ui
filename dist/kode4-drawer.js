import { __decorate } from "tslib";
import { LitElement, html, css } from "lit";
import { customElement, property, queryAssignedElements } from "lit/decorators.js";
export const kode4Drawers = new Map();
let Kode4Drawer = class Kode4Drawer extends LitElement {
    constructor() {
        super(...arguments);
        // @state()
        // private isOpen:boolean = false;
        this.open = false;
        this.transparent = false;
        this.resizeContentObserver = new ResizeObserver(() => {
            this.requestUpdate();
        });
    }
    // @property({ type: Boolean, attribute: 'open', reflect: true })
    // public set open(value:boolean) {
    //     console.log('[Kode4Drawer] change open', value);
    //     if (value && !this.isOpen) {
    //         console.log('[Kode4Drawer] ... go on and show');
    //         this.show();
    //     }
    // }
    //
    // public get open():boolean {
    //     return this.isOpen;
    // }
    toggleCssClass(show, cssClass) {
        show ? this.classList.add(cssClass) : this.classList.remove(cssClass);
    }
    set left(value) {
        this.toggleCssClass(value, 'kode4-drawer-left');
    }
    get left() {
        return this.classList.contains('kode4-drawer-left');
    }
    set right(value) {
        this.toggleCssClass(value, 'kode4-drawer-right');
    }
    get right() {
        return this.classList.contains('kode4-drawer-right');
    }
    set top(value) {
        this.toggleCssClass(value, 'kode4-drawer-top');
    }
    get top() {
        return this.classList.contains('kode4-drawer-top');
    }
    set bottom(value) {
        this.toggleCssClass(value, 'kode4-drawer-bottom');
    }
    get bottom() {
        return this.classList.contains('kode4-drawer-bottom');
    }
    doShow() {
        this.open = true;
    }
    show() {
        this.open = true;
    }
    hide() {
        this.open = false;
        this.dispatchEvent(new CustomEvent('close', {
            bubbles: true,
            composed: true,
        }));
    }
    handleBackdropClick(e) {
        e.stopPropagation();
        e.preventDefault();
        this.hide();
    }
    get contentWidth() {
        var _a;
        if (this.top || this.bottom) {
            return window.innerWidth;
        }
        let maxWidth = 0;
        (_a = this.mainItems) === null || _a === void 0 ? void 0 : _a.forEach((el) => {
            maxWidth = Math.max(maxWidth, el.offsetWidth);
        });
        return maxWidth;
    }
    get contentHeight() {
        var _a;
        let maxHeight = 0;
        (_a = this.mainItems) === null || _a === void 0 ? void 0 : _a.forEach((el) => {
            maxHeight = Math.max(maxHeight, el.offsetHeight);
        });
        maxHeight = Math.min(maxHeight, window.innerHeight);
        return maxHeight;
    }
    firstUpdated(_changedProperties) {
        var _a;
        super.firstUpdated(_changedProperties);
        (_a = this.mainItems) === null || _a === void 0 ? void 0 : _a.forEach((el) => {
            this.resizeContentObserver.observe(el);
        });
        // if (this.animations) {
        //     setTimeout(() => {
        //         this.animationEnabled = true;
        //     }, 100);
        // }
    }
    connectedCallback() {
        if (this.name) {
            kode4Drawers.set(this.name, this);
        }
        super.connectedCallback();
    }
    disconnectedCallback() {
        // this.resizeContentObserver.unobserve([...this.headerItems, ...this.mainItems, ...this.footerItems]);
        this.resizeContentObserver.disconnect();
        if (this.name && kode4Drawers.has(this.name)) {
            kode4Drawers.delete(this.name);
        }
        super.disconnectedCallback();
    }
    /**
     * Automatically hide menu if a menu-item is clicked an defaultPrevented.
     *
     * @param e
     * @private
     */
    handleListClick(e) {
        if (e.defaultPrevented) {
            this.hide();
        }
    }
    render() {
        return html `
            <kode4-backdrop ?visible="${this.open}" @click="${this.handleBackdropClick}" @contextmenu="${this.handleBackdropClick}" ?transparent="${this.transparent}"></kode4-backdrop>
            <div class="drawer" style="${(this.left || this.right) && this.contentWidth > 0 ? `width: ${this.contentWidth}px;` : undefined} ${(this.top || this.bottom) && this.contentHeight > 0 ? `height: ${this.contentHeight}px;` : undefined}" @click="${this.handleListClick}">
                ${this.left || this.right ? html `
                    <kode4-column class="drawer-content">
                        <slot></slot>
                    </kode4-column>
                ` : html `
                    <div class="drawer-content" style="${this.contentHeight > 0 ? `height: ${this.contentHeight}px;` : undefined}">
                        <slot></slot>
                    </div>
                `}
            </div>
        `;
    }
};
Kode4Drawer.styles = [
    css `
            * {
                box-sizing: border-box;
            }
            
            :host {
                position: fixed;
                display: block;
                z-index: 10010;
            }

            :host(.kode4-drawer-left) {
                left: 0;
                right: auto;
                top: 0;
                bottom: 0;
                transition: top 0s linear 0s;
            }

            :host(.kode4-drawer-right) {
                left: auto;
                right: 0;
                top: 0;
                bottom: 0;
                transition: top 0s linear 0s;
            }

            :host(.kode4-drawer-top) {
                left: 0;
                right: 0;
                top: 0;
                bottom: auto;
                transition: left 0s linear 0s;
            }

            :host(.kode4-drawer-bottom) {
                left: 0;
                right: 0;
                top: auto;
                bottom: 0;
                transition: left 0s linear 0s;
            }

            :host(.kode4-drawer-left:not([open])),
            :host(.kode4-drawer-right:not([open])) {
                top: 1000vh;
                transition: top 0s linear 0.25s;
            }

            :host(.kode4-drawer-top:not([open])),
            :host(.kode4-drawer-bottom:not([open])) {
                left: 1000vw;
                transition: left 0s linear 0.25s;
            }

            .drawer {
                position: absolute;
                display: block;
            }

            :host(.kode4-drawer-left) .drawer {
                left: 0;
                right: auto;
                top: 0;
                bottom: 0;
            }
            
            :host(.kode4-drawer-right) .drawer {
                left: auto;
                right: 0;
                top: 0;
                bottom: 0;
            }
            
            :host(.kode4-drawer-top) .drawer {
                left: 0;
                right: 0;
                top: 0;
                bottom: auto;
                max-height: 100vh;
                overflow: hidden;
            }
            
            :host(.kode4-drawer-bottom) .drawer {
                left: 0;
                right: 0;
                top: auto;
                bottom: 0;
                max-height: 100vh;
                overflow: hidden;
            }
            
            .drawer-content {
                position: absolute;
                display: block;
                z-index: 10050;
                transition: all .25s linear;
                background-color: #fff;
            }
            
            :host(.kode4-drawer-left) .drawer-content {
                left: -100%;
                right: auto;
                top: 0;
                bottom: 0;
            }
            
            :host(.kode4-drawer-left[open]) .drawer-content {
                left: 0;
            }
            
            :host(.kode4-drawer-right) .drawer-content {
                left: auto;
                right: -100%;
                top: 0;
                bottom: 0;
            }
            
            :host(.kode4-drawer-right[open]) .drawer-content {
                right: 0;
            }
            
            :host(.kode4-drawer-top) .drawer-content {
                left: 0;
                right: 0;
                top: -100%;
                bottom: auto;
                max-height: 100vh;
                overflow: hidden;
            }
            
            :host(.kode4-drawer-top[open]) .drawer-content {
                top: 0;
            }
             
            :host(.kode4-drawer-bottom) .drawer-content {
                left: 0;
                right: 0;
                top: auto;
                bottom: -100%;
                max-height: 100vh;
                overflow: hidden;
            }
            
            :host(.kode4-drawer-bottom[open]) .drawer-content {
                bottom: 0;
            }
        `,
];
__decorate([
    property({ type: String, attribute: 'name' })
], Kode4Drawer.prototype, "name", void 0);
__decorate([
    property({ type: Boolean, attribute: 'open', reflect: true })
], Kode4Drawer.prototype, "open", void 0);
__decorate([
    property({ type: Boolean, attribute: 'left' })
], Kode4Drawer.prototype, "left", null);
__decorate([
    property({ type: Boolean, attribute: 'right' })
], Kode4Drawer.prototype, "right", null);
__decorate([
    property({ type: Boolean, attribute: 'top' })
], Kode4Drawer.prototype, "top", null);
__decorate([
    property({ type: Boolean, attribute: 'bottom' })
], Kode4Drawer.prototype, "bottom", null);
__decorate([
    property({ type: Boolean, attribute: true })
], Kode4Drawer.prototype, "transparent", void 0);
__decorate([
    queryAssignedElements()
], Kode4Drawer.prototype, "mainItems", void 0);
__decorate([
    property({ type: Number })
], Kode4Drawer.prototype, "contentWidth", null);
__decorate([
    property({ type: Number })
], Kode4Drawer.prototype, "contentHeight", null);
Kode4Drawer = __decorate([
    customElement('kode4-drawer')
], Kode4Drawer);
export default Kode4Drawer;
