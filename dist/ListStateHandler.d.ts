import { IApplicationPersistenceService } from "@kode4/core/persistence/ApplicationPersistenceService";
export interface IListStateObserver {
    onOrderChange?(): void;
    onFilterChange?(): void;
    onPaginationChange?(): void;
    onStateChange?(): void;
    onRestore?(): void;
    onReset?(): void;
    onRefresh?(forceReload: boolean): void;
}
export declare class ListLimitReachedError extends Error {
}
export interface IListStateObserverRequestParameters {
    orderBy?: string;
    page: number;
    pageSize: number;
    filters: Map<string, any>;
    state: Map<string, any>;
    restored: boolean;
    hash?: string;
}
export declare class ListStateHandler {
    private observers;
    private restored;
    private _orderByFields;
    private _orderBy?;
    private _orderByDir;
    protected filters: Map<string, any>;
    page: number;
    lshState: 'idle' | 'initializing' | 'busy';
    loadPage(value: string | number): void;
    pageSize: number;
    size: number;
    total: number;
    cacheable: boolean;
    cacheLease: number;
    private lastRefresh?;
    private _hash;
    get hash(): string;
    protected state: Map<string, any>;
    private storage?;
    private storageKey?;
    private _initialValues?;
    constructor(params?: {
        fields?: Array<string>;
        field?: string;
        dir?: 'asc' | 'desc';
        filters?: Map<string, any>;
        page?: number;
        pageSize?: number;
        cacheable?: boolean;
        cacheLease?: number;
        observer?: IListStateObserver;
        state?: Map<string, any>;
        storage?: IApplicationPersistenceService;
        storageKey?: string;
    });
    reset(): void;
    resetList(): void;
    refreshList(forceReload?: boolean): void;
    store(): void;
    restore(): boolean;
    private canRestore;
    private applyValues;
    private toString;
    /**
     * Please re-register observers
     *
     * @param serialized
     */
    private fromString;
    private buildHash;
    private updateHash;
    clearPagination(): void;
    get orderByFields(): Array<string>;
    set orderByFields(fields: Array<string>);
    addOrderByField(field: string): void;
    get orderBy(): string | undefined;
    set orderBy(field: string | undefined);
    get orderByDir(): 'asc' | 'desc';
    set orderByDir(dir: 'asc' | 'desc' | undefined);
    isSortable(field: string): boolean;
    getOrderByDirForField(field: string): 'asc' | 'desc' | undefined;
    activateOrderByFieldOrToggleDir(field: string): void;
    setFilter(key: string, value: any): void;
    getFilter(key: string): any;
    hasFilter(key: string): boolean;
    hasFilters(): boolean;
    deleteFilter(key: string): void;
    clearFilters(keys?: Array<string>): void;
    private nullishValues;
    hasActiveFilters(): boolean;
    resetFilters(): void;
    setState(key: string, value: any): void;
    getState(key: string): any;
    hasState(key: string): boolean;
    deleteState(key: string): void;
    clearState(): void;
    observe(observer: IListStateObserver): void;
    unobserve(observer?: IListStateObserver): void;
    private fireOrderChanged;
    private debouncedFireOrderChanged;
    private fireFilterChanged;
    private debouncedFireFilterChanged;
    private firePaginationChanged;
    private fireStateChanged;
    private debouncedFireStateChanged;
    private fireRestore;
    private fireReset;
    private fireRefresh;
    protected sanitizeFilterAndStateValues(data: Map<string, any>): Map<string, any>;
    protected sanitizeFilterAndStateValue(value: any): any;
    getRequestParameters(): IListStateObserverRequestParameters;
}
