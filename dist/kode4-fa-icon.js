import { __decorate } from "tslib";
import { LitElement, html, css } from "lit";
import { customElement, property } from "lit/decorators.js";
// @See https://fontawesome.com/icons
import { icon } from '@fortawesome/fontawesome-svg-core';
let Kode4FaIcon = class Kode4FaIcon extends LitElement {
    firstUpdated(changedProperties) {
        super.firstUpdated(changedProperties);
        const fontSize = getComputedStyle(this, null).fontSize;
        if (this.icon) {
            this.style.minWidth = (parseInt(fontSize.replace(/px/, ''), 10) / this.icon.icon[1] * this.icon.icon[0]) + 'px';
        }
        else {
            this.style.minWidth = '1.0em';
        }
    }
    render() {
        var _a;
        return this.icon ? html `${(_a = icon(this.icon)) === null || _a === void 0 ? void 0 : _a.node}` : html ``;
        // return html`<img style="height: 1.0em; line-height: inherit;" src="data:image/svg+xml;base64,${btoa(unescape(encodeURIComponent(icon(this.icon?)?.html[0])))}">`;
    }
};
Kode4FaIcon.styles = [
    css `
            :host {
                line-height: 1.0em;
                font-size: inherit;
                display: inline-block;
                vertical-align: middle;
                color: inherit;
            }

            :host(.icon-top) {
                vertical-align: top;
            }

            :host(.icon-bottom) {
                vertical-align: bottom;
            }

            :host(.icon-baseline) {
                vertical-align: baseline;
            }

            :host(.icon-xxxl) {
                font-size: 3em;
            }

            :host(.icon-xxl) {
                font-size: 2.5em;
            }

            :host(.icon-xl) {
                font-size: 2em;
            }

            :host(.icon-l) {
                font-size: 1.5em;
            }

            :host(.icon-s) {
                font-size: .75em;
            }

            :host(.icon-xs) {
                font-size: .5em;
            }

            :host(.icon-xxs) {
                font-size: .3em;
            }
            
            svg {
                max-height: 1em;
            }
        `,
];
__decorate([
    property({ type: String })
], Kode4FaIcon.prototype, "icon", void 0);
Kode4FaIcon = __decorate([
    customElement('kode4-fa-icon')
], Kode4FaIcon);
export default Kode4FaIcon;
