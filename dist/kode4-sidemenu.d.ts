import { LitElement, PropertyValues } from "lit";
export default class Kode4Sidemenu extends LitElement {
    private headerItems;
    private mainItems;
    private footerItems;
    open: boolean;
    alwaysopen: boolean;
    absolute: boolean;
    left: boolean;
    right: boolean;
    shadow: boolean;
    animations: boolean;
    private animationEnabled;
    get contentWidth(): string;
    private resizeContentObserver;
    protected get cssClassLeftRight(): string;
    protected firstUpdated(_changedProperties: PropertyValues): void;
    connectedCallback(): void;
    disconnectedCallback(): void;
    render(): import("lit-html").TemplateResult<1>;
    static styles: import("lit").CSSResult[];
}
