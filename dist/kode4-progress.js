var Kode4Progress_1;
import { __decorate } from "tslib";
import { LitElement, html, css } from "lit";
import { customElement, property } from "lit/decorators.js";
import { faCircleNotch } from '@fortawesome/free-solid-svg-icons/faCircleNotch';
import kode4Theme from './kode4-css';
let Kode4Progress = Kode4Progress_1 = class Kode4Progress extends LitElement {
    constructor() {
        super(...arguments);
        this.type = 'linear';
        this.indeterminate = false;
        this.inline = false;
        this.showPercentage = false;
        this.progress = 0;
        this.visible = true;
    }
    firstUpdated(changedProperties) {
        super.firstUpdated(changedProperties);
        this.classList.add(`kode4-progress-${this.type}`);
        if (this.inline) {
            this.classList.add('kode4-progress-inline');
        }
    }
    render() {
        switch (this.type) {
            case 'icon':
                return this.renderProgessIcon();
            case 'text':
                return this.renderProgessText();
            default:
                return this.renderProgressLinear();
        }
    }
    // --- ICON ------------------------------
    renderProgessIcon() {
        return html `
            <div class="progress-icon ${this.indeterminate ? 'progress-icon-indeterminate' : 'progress-icon-determinate'}">
                ${this.indeterminate ? this.renderProgessBarIndeterminateIcon() : this.renderProgessBarDeterminateIcon()}
            </div>
        `;
    }
    renderProgessBarIndeterminateIcon() {
        return html `
            <div class="indeterminate-icon kode4-text ${this.color ? `kode4-text-${this.color}` : ''}">
                <kode4-fa-icon .icon="${faCircleNotch}"></kode4-fa-icon>
            </div>
        `;
    }
    renderProgessBarDeterminateIcon() {
        return html `
            <div class="determinate-icon kode4-text ${this.color ? `kode4-text-${this.color}` : ''}"
                 style="-webkit-transform:rotate(${this.getAngleForProgress()}deg); -moz-transform:rotate(${this.getAngleForProgress()}deg); transform:rotate(${this.getAngleForProgress()}deg);">
                <kode4-fa-icon .icon="${faCircleNotch}"></kode4-fa-icon>
            </div>
        `;
    }
    getAngleForProgress() {
        return Math.round(360 / 100 * this.progress);
    }
    // --- TEXT ------------------------------
    renderProgessText() {
        return html `
            <div class="progress-text ${this.indeterminate ? 'proress-text-indeterminate' : ''}">
                ${this.indeterminate ? this.renderProgessBarIndeterminateText() : this.renderProgessBarDeterminateText()}
            </div>
        `;
    }
    renderProgessBarIndeterminateText() {
        return html `
            <div class="indeterminate-text kode4-text ${this.color ? `kode4-text-${this.color}` : ''}">
                |
            </div>
        `;
    }
    renderProgessBarDeterminateText() {
        return html `
            <div class="determinate-text kode4-text ${this.color ? `kode4-text-${this.color}` : ''}">
                ${this.progress} %
            </div>
        `;
    }
    // --- LINEAR ------------------------------
    renderProgressLinear() {
        return html `
            <div class="progress-linear">
                ${this.indeterminate ? this.renderProgressBarIndeterminateLinear() : this.renderProgressBarDeterminateLinear()}
            </div>
        `;
    }
    renderProgressBarIndeterminateLinear() {
        return html `
            <div class="indeterminate kode4-text ${this.color ? `kode4-text-${this.color}` : ''}"></div>
        `;
    }
    renderProgressBarDeterminateLinear() {
        return html `
            <div class="determinate kode4-text ${this.color ? `kode4-text-${this.color}` : ''}"
                 style="width: ${this.progress}%;"></div>
        `;
    }
    static getIconStyles() {
        return css `
            .progress-icon-determinate,
            .progress-icon-indeterminate {
                display: flex;
                flex-direction: row;
                min-width: 1.0em;
                height: 1.0em;
                line-height: 1.0em;
                transform-origin: center center;
            }

            .determinate-icon {
                display: inline-block;
                transition: all .3s linear;
            }

            .indeterminate-icon {
                display: inline-block;
                -webkit-animation: indeterminate-icon 1.5s linear infinite;
                -moz-animation: indeterminate-icon 1.5s linear infinite;
                animation: indeterminate-icon 1.5s linear infinite;
            }

            @-moz-keyframes indeterminate-icon {
                from {
                    -moz-transform: rotate(0deg);
                }
                to {
                    -moz-transform: rotate(360deg);
                }
            }
            @-webkit-keyframes indeterminate-icon {
                from {
                    -webkit-transform: rotate(0deg);
                }
                to {
                    -webkit-transform: rotate(360deg);
                }
            }
            @keyframes indeterminate-icon {
                from {
                    transform: rotate(0deg);
                }
                to {
                    transform: rotate(360deg);
                }
            }
        `;
    }
    static getTextStyles() {
        return css `
            .proress-text-indeterminate {
                display: block;
                min-width: 1.0em;
                height: 1.0em;
                line-height: 1.0em;
                display: block;
                text-align: center;
            }

            .indeterminate-text {
                display: inline-block;
                -webkit-animation: indeterminate-text 1.5s linear infinite;
                -moz-animation: indeterminate-text 1.5s linear infinite;
                animation: indeterminate-text 1.5s linear infinite;
            }

            @-moz-keyframes indeterminate-text {
                from {
                    -moz-transform: rotate(0deg);
                }
                to {
                    -moz-transform: rotate(360deg);
                }
            }
            @-webkit-keyframes indeterminate-text {
                from {
                    -webkit-transform: rotate(0deg);
                }
                to {
                    -webkit-transform: rotate(360deg);
                }
            }
            @keyframes indeterminate-text {
                from {
                    transform: rotate(0deg);
                }
                to {
                    transform: rotate(360deg);
                }
            }

        `;
    }
    static getLinearStyles() {
        return css `
            .progress-linear {
                position: relative;
                display: block;
                width: 100%;
                height: 100%;
                background-color: transparent;
                background-clip: padding-box;
                margin: 0;
                padding: 0;
                overflow: hidden;
            }

            .progress-linear .determinate {
                position: absolute;
                top: 0;
                bottom: 0;
                background-color: currentColor;
                transition: width .3s linear;
            }

            .progress-linear .indeterminate {
                background-color: currentColor;
            }

            .progress-linear .indeterminate:before {
                content: '';
                position: absolute;
                background-color: inherit;
                top: 0;
                left: 0;
                bottom: 0;
                will-change: left, right;
                -webkit-animation: indeterminate 2.1s cubic-bezier(0.65, 0.815, 0.735, 0.395) infinite;
                animation: indeterminate 2.1s cubic-bezier(0.65, 0.815, 0.735, 0.395) infinite;
            }

            .progress-linear .indeterminate:after {
                content: '';
                position: absolute;
                background-color: inherit;
                top: 0;
                left: 0;
                bottom: 0;
                will-change: left, right;
                -webkit-animation: indeterminate-short 2.1s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;
                animation: indeterminate-short 2.1s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;
                -webkit-animation-delay: 1.15s;
                animation-delay: 1.15s;
            }

            @-webkit-keyframes indeterminate {
                0% {
                    left: -35%;
                    right: 100%;
                }
                60% {
                    left: 100%;
                    right: -90%;
                }
                100% {
                    left: 100%;
                    right: -90%;
                }
            }

            @keyframes indeterminate {
                0% {
                    left: -35%;
                    right: 100%;
                }
                60% {
                    left: 100%;
                    right: -90%;
                }
                100% {
                    left: 100%;
                    right: -90%;
                }
            }

            @-webkit-keyframes indeterminate-short {
                0% {
                    left: -200%;
                    right: 100%;
                }
                60% {
                    left: 107%;
                    right: -8%;
                }
                100% {
                    left: 107%;
                    right: -8%;
                }
            }

            @keyframes indeterminate-short {
                0% {
                    left: -200%;
                    right: 100%;
                }
                60% {
                    left: 107%;
                    right: -8%;
                }
                100% {
                    left: 107%;
                    right: -8%;
                }
            }
        `;
    }
};
Kode4Progress.styles = [
    kode4Theme,
    css `

            :host {
                display: block;
            }

            :host(.kode4-progress-inline) {
                display: inline-block;
            }

            :host(.kode4-progress-linear) {
                height: 2px;
            }

            :host(.stretch-height) {
                height: 100%;
            }

            :host(.stretch-width) {
                width: 100%;
                display: block;
            }

        `,
    Kode4Progress_1.getLinearStyles(),
    Kode4Progress_1.getTextStyles(),
    Kode4Progress_1.getIconStyles(),
];
__decorate([
    property({ type: String })
], Kode4Progress.prototype, "type", void 0);
__decorate([
    property({ type: String })
], Kode4Progress.prototype, "color", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Progress.prototype, "indeterminate", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Progress.prototype, "inline", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Progress.prototype, "showPercentage", void 0);
__decorate([
    property({ type: Number })
], Kode4Progress.prototype, "progress", void 0);
__decorate([
    property({ type: Boolean })
], Kode4Progress.prototype, "visible", void 0);
Kode4Progress = Kode4Progress_1 = __decorate([
    customElement('kode4-progress')
], Kode4Progress);
export default Kode4Progress;
