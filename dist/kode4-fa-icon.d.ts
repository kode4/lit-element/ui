import { LitElement, PropertyValues } from "lit";
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
export default class Kode4FaIcon extends LitElement {
    icon?: IconDefinition;
    firstUpdated(changedProperties: PropertyValues): void;
    static styles: import("lit").CSSResult[];
    render(): import("lit-html").TemplateResult<1>;
}
