import { LitElement, PropertyValues } from "lit";
export declare const kode4Drawers: Map<string, Kode4Drawer>;
export default class Kode4Drawer extends LitElement {
    private name?;
    private open;
    private toggleCssClass;
    set left(value: boolean);
    get left(): boolean;
    set right(value: boolean);
    get right(): boolean;
    set top(value: boolean);
    get top(): boolean;
    set bottom(value: boolean);
    get bottom(): boolean;
    transparent: boolean;
    doShow(): void;
    show(): void;
    hide(): void;
    private handleBackdropClick;
    private mainItems;
    private resizeContentObserver;
    get contentWidth(): number;
    get contentHeight(): number;
    protected firstUpdated(_changedProperties: PropertyValues): void;
    connectedCallback(): void;
    disconnectedCallback(): void;
    /**
     * Automatically hide menu if a menu-item is clicked an defaultPrevented.
     *
     * @param e
     * @private
     */
    private handleListClick;
    render(): import("lit-html").TemplateResult<1>;
    static styles: import("lit").CSSResult[];
}
