import { LitElement } from "lit";
export default class Kode4Backdrop extends LitElement {
    private toggleCssClass;
    set visible(value: boolean);
    get visible(): boolean;
    set transparent(value: boolean);
    get transparent(): boolean;
    render(): import("lit-html").TemplateResult<1>;
    static styles: import("lit").CSSResult[];
}
