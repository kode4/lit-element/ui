import {css, CSSResultArray, CSSResultOrNative, LitElement} from "lit";
import {property} from "lit/decorators.js";

type Constructor<T = {}> = new (...args: any[]) => T;

export declare class IApplyKode4Theme {
    highlight: boolean;

    public renderHighlight(): unknown;

    public static styles: CSSResultArray | CSSResultOrNative;
}

export const applyKode4Theme = <T extends Constructor<LitElement>>(superClass: T) => {

    class ApplyKode4Theme extends superClass implements IApplyKode4Theme {
        @property() highlight = false;

        connectedCallback() {
            super.connectedCallback();
            this.doSomethingPrivate();
        }

        private doSomethingPrivate() {
            /* does not need to be part of the interface */
            console.log('.... DO SOMETHING PRIVATE');
        }

        public renderHighlight() {
            /* does not need to be part of the interface */
            console.log('.... PERFORM HIGHLIGHT');
        }

        public static styles = [
            css`
                :host {
                    border: solid 10px #00f !important;
                }
            `,
        ];
    }

    // Cast return type to the superClass type passed in
    return ApplyKode4Theme as Constructor<IApplyKode4Theme> & T;
}
