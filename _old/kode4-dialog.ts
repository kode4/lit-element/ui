// import { LitElement, PropertyValues, html, css, property, customElement } from "lit";
import { LitElement, html, css, PropertyValues } from "lit";
import { customElement, property } from "lit/decorators.js";

@customElement('kode4-dialog')
export default class Kode4Dialog extends LitElement {

    @property({ type: String })
    public class:String = '';

    @property({ type: Boolean })
    protected isOpen:Boolean = false;

    public firstUpdated(changedProperties:PropertyValues) {
        super.firstUpdated(changedProperties);
    }

    protected onClose() {
        let event = new CustomEvent("close", {
            detail: {
                open: this.open,
            }
        });

        this.dispatchEvent(event);
    }

    protected onOpen() {
        let event = new CustomEvent("open", {
            detail: {
                open: this.isOpen,
            }
        });

        this.dispatchEvent(event);
    }

    public open() {
        this.isOpen = true;
    }

    public close() {
        this.isOpen = false;
    }

    public toggle() {
        this.isOpen ? this.close() : this.open();

    }

    public render() {
        return html`
            <div class="backdrop ${this.isOpen ? 'open' : ''}" @click="${this.onClose}">
                <div class="dialog" @click="${(e:Event) => e.stopPropagation()}">
                    <slot></slot>
                </div>
            </div>
        `;
    }

    static styles = [
        css`
            :host {
                width: 0;
                height: 0;
            }

            .backdrop {
                display: none;
                visibility: hidden;
                position: fixed;
                top: 0;
                bottom: 0;
                left: 0;
                right: 0;
                background-color: rgba(0, 0, 0, 0.25);
                align-items: center;
                justify-items: center;
                z-index: 5000;

                -moz-user-select: none;
                -khtml-user-select: none;
                -webkit-user-select: none;
                user-select: none;
                /* Required to make elements draggable in old WebKit */
                -khtml-user-drag: element;
                -webkit-user-drag: element;
            }

            .dialog {
                background-color: #fff;
                max-width: 100vw;
                max-height: 100vh;
                z-index: 5010;
                margin-left: auto;
                margin-right: auto;
                padding: 0;
                border-radius: 2px;
                box-shadow: 2px 2px 20px #888;
            }

            .backdrop.open {
                display: flex;
                visibility: visible;
            }
        `,
    ];

}
