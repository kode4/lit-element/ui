import {LitElement, html, css, HTMLTemplateResult} from "lit";
import {customElement, property, queryAssignedElements, state} from "lit/decorators.js";
import kode4CssTheme from "./kode4-css";
import Kode4Tab from "./kode4-tab";

@customElement('kode4-tabs')
export default class Kode4Tabs extends LitElement {

    @queryAssignedElements()
    private tabContentElements?: Array<Kode4Tab>;

    @property({ type: Boolean, attribute: true })
    public spacing:boolean = false;

    @property({ type: Boolean, attribute: true })
    public stretch:boolean = false;

    @property({ type: Array, attribute: 'tabs' })
    public tabs:Map<string, HTMLTemplateResult> = new Map();

    @property({ type: String, attribute: true })
    public selected?:string;

    @state()
    public changeTo:string = 'right';

    @state()
    private get selectedTab() {
        if (this.selected) {
            return this.selected;
        }

        if (this.tabs.size) {
            return this.tabs.get(this.tabs.keys().next().value);
        }

        return undefined;
    }

    public selectTab(tabname:string) {
        if (!this.tabContentElements?.length) {
            return;
        }
        const oldPos = this.tabContentElements?.findIndex((el:Kode4Tab) => el.name === this.selected);
        const newPos = this.tabContentElements?.findIndex((el:Kode4Tab) => el.name === tabname);
        this.changeTo = oldPos < newPos ? 'right' : 'left';

        this.selected = tabname;
        this.tabContentElements?.forEach((el:Kode4Tab) => {
            if (el.name === this.selected) {
                el.classList.add('kode4-tabs-selected');
            } else {
                el.classList.remove('kode4-tabs-selected');
            }
        });
    }

    handleSlotchange() {
        this.tabContentElements?.forEach((el:Kode4Tab) => {
            this.tabs.set(el.name, el.tab);
            if (el.classList.contains('kode4-tabs-selected')) {
                this.selected = el.name;
            }
        });
    }

    public render() {
        return html`
            <div class="tabtitles ${this.changeTo === 'left' ? 'toLeft' : 'toRight'}" part="tab-container">
                ${[...this.tabs.entries()].map(([name, title]) => html`
                    <div class="tab ${this.selectedTab === name ? 'active' : ''}" @click=${() => this.selectTab(name)} part="tab">
                        <div class="tab-body">
                            ${title}
                        </div>
                    </div>
                `)}
            </div>
            <div class="tabcontents ${this.stretch ? 'stretch-height' : ''}" part="container">
                <slot @slotchange=${this.handleSlotchange}></slot>
            </div>
        `;
    }

    static styles= [
        kode4CssTheme,
        css`
            * {
                box-sizing: border-box;
            }

            :host {
                display: flex;
                flex-direction: column;
                flex-wrap: nowrap;
                min-height: 100%;
            }

            :host(.kode4-spacing) {
                gap: var(--kode4-toolbar-spacing);
            }
            
            .tabtitles {
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;
                justify-items: stretch;
                align-items: stretch;
                overflow-x: auto;
            }
            
            .tab-body {
                height: calc(100% - 2px);
                padding: 5px 2px;
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;
                justify-items: stretch;
                align-items: center;
            }

            .tabtitles > .tab {
                cursor: pointer;
                text-overflow: ellipsis;
                flex: 0 1 auto;
            }
 
            .tabtitles > .tab:after {
                content: "";
                display: block;
                height: 2px;
                width: 100%;
                background-color: var(--background-color-filled-primary);
                transition: transform .25s linear;
                transform: scaleX(0);
            }

            .tabtitles.toRight > .tab:after {
                transform-origin: right;
            }

            .tabtitles.toLeft > .tab:after {
                transform-origin: left;
            }

            .tabtitles > .tab.active {
            }
 
            .tabtitles > .tab.active:after {
                transform: scaleX(1);
            }
 
            .tabtitles.toRight > .tab.active:after {
                transform-origin: left;
            }
 
            .tabtitles.toLeft > .tab.active:after {
                transform-origin: right;
            }
 
            .tabcontents {
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;
                justify-items: flex-start;
                align-items: flex-start;
                overflow: hidden;
            }
            
            .tabcontents.stretch-height {
                flex: 1 0 1px;
                justify-items: stretch;
                align-items: stretch;
                overflow: auto;
            }
            
            ::slotted(*) {
                //flex: 0 1 0px;
                width: 0;
                height: 0;
                //min-height: 100%;
                padding: 0;
                margin: 0;
                border: none;
                opacity: 0;
                //transform: transla.teX(100%) scaleX(0);
                //transform: scaleX(0);
                //transform: scaleX(0);
                //transform-origin: top left;
                transition: all .15s ease-in;
                overflow:hidden;
            }

            ::slotted(.kode4-tabs-selected) {
                flex: 1 0 1px;
                left: 0;
                width: 100%;
                height: auto;
                opacity: 1;
                overflow: auto;
                //transform: translateX(0) scaleX(1);
                //transform: scaleX(1);
                //transform: scale(1, 1);
            }

            //.tabcontents:not(.stretch-height) ::slotted(*) {
            //    //transform: scaleY(0);
            //    //transform-origin: top;
            //    //opacity: 0.5;
            //}
            //
            //.tabcontents:not(.stretch-height) ::slotted(.kode4-tabs-selected) {
            //    //transform: scaleY(1);
            //    //opacity: 1;
            //}
        `
    ];

}
