export interface Kode4DraggableData {
    type?: string,
    payload?: any
}

export interface Kode4DropzoneOptions {
    accepts?: Map<string, string>,
    acceptsFiles?: Boolean,
    acceptsExternal?: Boolean,
}

export class Kode4DragDropManager {

    private static instance: Kode4DragDropManager;

    public static getInstance(): Kode4DragDropManager {
        if (!this.instance) {
        this.instance = new Kode4DragDropManager();
        }
        return this.instance;
    }

    public dropSuccessful:boolean = false;

    public dataTransfer?:Kode4DraggableData;

    public draggableElement?:HTMLElement;

}

export const dragDropManager = Kode4DragDropManager.getInstance();


// --- DRAGGABLE ----------------
export const dragstart = (e:DragEvent, data:Kode4DraggableData, renderClone?:CallableFunction|false, handler?:CallableFunction) => {
    e.stopPropagation();
    dragDropManager.dropSuccessful = false;    
    const draggableElement = <HTMLElement>(<HTMLElement>e.target).closest('[draggable],[draggable="true"]');
    dragDropManager.draggableElement = draggableElement;

    // --- custom clone ---
    // var clone:HTMLElement = <HTMLElement>draggableElement.cloneNode(true);
    // clone.style.backgroundColor = "red";
    // clone.style.position = "fixed";
    // clone.style.top = "-5000px";
    // clone.style.right = "-5000px";
    // document.body.appendChild(clone);
    if (renderClone) {
        renderClone(e);

    } else if (renderClone === false) {
        e.dataTransfer?.setDragImage(document.createElement('img'), 0, 0);

    } else {
        e.dataTransfer?.setDragImage(draggableElement, 0, 0);
    }


    dragDropManager.dataTransfer = data;
    // e.dataTransfer?.setData("text", dragDropManager.encodeDataTransfer(data));
    if (handler) {
        handler(e, dragDropManager.dataTransfer);
    }
}

export const dragend = (e:DragEvent, handler?:CallableFunction) => {
    dragDropManager.draggableElement = undefined;
    if (handler) {
        handler(e, dragDropManager.dataTransfer);
    }
    dragDropManager.dropSuccessful = false;
    dragDropManager.dataTransfer = undefined;
}


// --- DROP-ZONE ----------------
export const dragover = (e:DragEvent, options?:Kode4DropzoneOptions, handler?:CallableFunction) => {
    if (dragDropManager.dataTransfer) {
        if (dragDropManager.dataTransfer.type && options?.accepts?.has(dragDropManager.dataTransfer.type)) {
            (<HTMLElement>e.target).classList.add('kode4-dragover');
            if (e.dataTransfer) {
                e.dataTransfer.dropEffect = <'none'|'copy'|'link'|'move'>options.accepts.get(dragDropManager.dataTransfer.type);
            }
        }
        e.preventDefault();

    } else if (options?.acceptsFiles || options?.acceptsExternal) {
        (<HTMLElement>e.target).classList.add('kode4-dragover');
        if (e.dataTransfer) {
            e.dataTransfer.dropEffect = 'copy';
        }
        e.preventDefault();
    }
    if (handler) {
        handler(e, dragDropManager.dataTransfer);
    }
}

export const dragleave = (e:DragEvent, handler?:CallableFunction) => {
    (<HTMLElement>e.target).classList.remove('kode4-dragover');
    if (handler) {
        handler(e, dragDropManager.dataTransfer);
    }
}

export const drop = (e:DragEvent, handler:CallableFunction, callbackOptions?:any) => {
    e.preventDefault();
    e.stopPropagation();
    dragDropManager.dropSuccessful = true;
    (<HTMLElement>e.target).classList.remove('kode4-dragover');
    handler(e, dragDropManager.dataTransfer, callbackOptions);
}

/**
 * I tried to modify the ifDefined directive here. But
 * directives declared HERE just don't work in my project.
 * If I declare them directly in the project, they work fine.
 */
// import {AttributePart, directive, Part} from 'lit-html';

// export const draggable = directive((value: unknown) => (part: Part) => {
//     if (value === undefined && part instanceof AttributePart) {
//       if (value !== part.value) {
//         const name = part.committer.name;
//         part.committer.element.removeAttribute(name);
//       }
//     } else {
//       part.setValue(value);
//     }
//   });
