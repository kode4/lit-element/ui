import {LitElement, html, css, HTMLTemplateResult, PropertyValues} from "lit";
import {customElement, property, queryAssignedElements, state} from "lit/decorators.js";
import kode4CssTheme from "./kode4-css";
import { v4 as uuidv4 } from "uuid";

@customElement('kode4-tab')
export default class Kode4Tab extends LitElement {

    @queryAssignedElements({slot: 'title'})
    private titleElements?: Array<HTMLElement>;

    @property()
    public name:string = `kode4tab_${uuidv4()}`;

    @state()
    public get tab():HTMLTemplateResult {
        if (this.titleElements?.length) {
            return html`${this.titleElements}`;
        }
        return html`${this.name}`;
    }

    protected firstUpdated(_changedProperties: PropertyValues) {
        super.firstUpdated(_changedProperties);
    }

    public render() {
        return html`
            <div class="title">
                <slot name="title"></slot>
            </div>
                
            <slot></slot>
        `;
    }

    static styles = [
        kode4CssTheme,
        css`
            * {
                box-sizing: border-box;
            }

            :host {
                display: block;
            }
            
            .title {
                display: none;
                visibility: hidden;
                width: 0;
                height: 0;
                overflow: hidden;
            }
        `,
    ];

}
