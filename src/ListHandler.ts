import {IListStateObserver, ListLimitReachedError, ListStateHandler} from "./ListStateHandler";
import {IApplicationPersistenceService} from "@kode4/core/persistence/ApplicationPersistenceService";
import {ConvertableDynamicCustomParams} from "@kode4/core/http/HttpService";
import debounce from "lodash-es/debounce";

export interface IListObserver {
    onListChange(list:Array<unknown>):void;
    onStateChange?(state: Map<string, any>):void;
    // onRestorePosition?(position:number):void;
}

export interface IListRequest<T> {
    total:number;
    list:Array<T>;
}

export interface ISearchParams {
    [key: string]: any;
}

export abstract class ListHandler<T> implements IListStateObserver {

    public currentHash?:string;

    private observers:Array<IListObserver> = [];

    protected list:Array<any> = [];

    private lsh!: ListStateHandler;

    public storage?: IApplicationPersistenceService;

    public storageKey?: string;

    constructor(params:{
        listStateHandler: ListStateHandler,
        storage?:IApplicationPersistenceService,
        storageKey?:string,
        observer?:IListObserver
    }) {
        this.lsh = params.listStateHandler;
        this.lsh.observe(this);
        this.storage = params.storage;
        this.storageKey = params.storageKey;
        if (params.observer) {
            this.observe(params.observer);
        }
    }

    // =====[ List and Item Operations ]================================================================================
    public async loadList(forceReload:boolean = false) {
        try {
            this.lsh.lshState = 'busy';

            const params:any = {}
            const lshParams = this.lsh.getRequestParameters();

            if (forceReload || !this.currentHash || lshParams.hash !== this.currentHash) {
                if (lshParams.orderBy) {
                    params.orderBy = lshParams.orderBy;
                }
                params.limit = lshParams.pageSize;
                params.offset = lshParams.page;

                const newData:any = await this.performLoadList(params, lshParams.filters);
                if (newData.list) {
                    this.list = [
                        ...this.list,
                        ...newData.list,
                    ];
                }

                this.lsh.size = this.list.length;
                this.lsh.total = newData.total;
                this.currentHash = this.lsh.hash;
                this.lsh.store();

                this.fireListChanged();

            } else {
                this.fireListChanged();
            }

        } catch (e) {
            if (e !instanceof ListLimitReachedError) {
            }

        } finally {
            this.lsh.lshState = 'idle';
        }
    }
    private debouncedLoadList = debounce(this.loadList, 50);

    public async loadItem(searchParams:ISearchParams):Promise<T> {
        try {
            this.lsh.lshState = 'busy';
            return await this.performLoadItem(searchParams);

        } catch (e) {
            console.error(e);
            throw e;

        } finally {
            this.lsh.lshState = 'idle';
        }
    }

    public async loadItemAndUpdateInMemory(searchParams:ISearchParams) {
        this.updateItemInMemory(searchParams, await this.performLoadItem(searchParams));
    }

    public async updateItem(searchParams:ISearchParams, newData:ConvertableDynamicCustomParams, refreshList:boolean = false) {
        try {
            this.lsh.lshState = 'busy';
            await this.performUpdateItem(searchParams, newData);

            const currentItem = this.findItem(searchParams);
            if (currentItem) {
                this.loadItemAndUpdateInMemory(searchParams);
            }

        } catch (e) {
            console.error(e);
            throw e;

        } finally {
            this.lsh.lshState = 'idle';

            if (!refreshList) {
                this.fireListChanged();

            } else {
                this.lsh.refreshList();
            }
        }
    }

    public async deleteItem(searchParams:ISearchParams, refreshList:boolean = true) {
        try {
            this.lsh.lshState = 'busy';
            await this.performDeleteItem(searchParams);

        } catch (e) {
            if (e !instanceof ListLimitReachedError) {
            }

        } finally {
            this.lsh.lshState = 'idle';

            if (!refreshList) {
                this.deleteItemInMemory(searchParams);
                this.fireListChanged();

            } else {
                this.lsh.refreshList();
            }

        }
    }

    // =====[ Events ]==================================================================================================
    public observe(observer:IListObserver) {
        if (!this.observers.includes(observer)) {
            this.observers.push(observer);
            observer.onListChange(this.list);
        }
    }

    public unobserve(observer?:IListObserver) {
        if (!observer) {
            this.observers = [];
        }  else {
            this.observers = [
                ...this.observers.filter((el:IListObserver) => el !== observer)
            ];
        }
    }

    private fireListChanged = () => {
        this.observers.forEach((observer:IListObserver) => {
            observer.onListChange(this.list);
        });
    }
    // private debouncedFireListChanged = debounce(this.fireListChanged, 50);

    // =====[ IListStateObserver ]======================================================================================
    onFilterChange(): void {
        this.list = [];
        this.fireListChanged();
    }

    onOrderChange(): void {
        this.list = [];
        this.fireListChanged();
    }

    onPaginationChange(): void {
        // this.loadList();
        this.debouncedLoadList();
    }

    onRestore(): void {
        // this.loadList();
        this.debouncedLoadList();
    }

    onReset(): void {
        this.list = [];
        // this.loadList();
        this.debouncedLoadList();
    }

    onRefresh(forceReload:boolean = false): void {
        if (forceReload) {
            this.list = [];
        }
        this.loadList(forceReload);
    }

    // =====[ Helpers ]=================================================================================================
    protected itemMatchesSearchParams(item:T, searchParams:ISearchParams):boolean {
        if (!Object.keys(searchParams).length) {
            return false;
        }
        let match:boolean = false;
        for (const [key, value] of Object.entries(item)) {
            if (searchParams[key] && value == searchParams[key]) {
                match = true;
            }
        }
        return match;
    }

    public findItem(searchParams:ISearchParams):T|undefined {
        return this.list.find((item:T) => this.itemMatchesSearchParams(item, searchParams));
    }

    protected updateItemInMemory(searchParams:ISearchParams, newItem:T) {
        this.list = [
            ...this.list.map((item:T) => this.itemMatchesSearchParams(item, searchParams) ? newItem : item)
        ];
        this.fireListChanged();
    }

    protected deleteItemInMemory(searchParams:ISearchParams) {
        this.list = [
            ...this.list.filter((item:T) => !this.itemMatchesSearchParams(item, searchParams))
        ];
    }

    // =====[ Required Implementations ]================================================================================
    protected abstract performLoadList(params:{}, filters:Map<string, any>):Promise<IListRequest<T>>;

    protected abstract performLoadItem(searchParams:ISearchParams):Promise<T>;

    protected abstract performUpdateItem(searchParams:ISearchParams, newData: ConvertableDynamicCustomParams):Promise<any>;

    protected abstract performDeleteItem(searchParams:ISearchParams):Promise<any>;

}
