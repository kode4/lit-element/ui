// import { LitElement, html, property, customElement } from "lit";
import {LitElement, html, css} from "lit";
import { customElement, property } from "lit/decorators.js";
import {applyKode4Theme} from "../../kode4-theme";
import kode4Css from "../../kode4-css";

@customElement('kode4ui-demo-colordemo')
export class Kode4UiDemoColorDemo extends applyKode4Theme(LitElement) {

    @property({ type: Boolean })
    public showLabels:boolean = false;

    render() {
        return html`
            <h2>Color Test</h2>
        `;
    }

    static styles = [
        kode4Css,
        css`
            :host {
                display: block;
            }
        `
    ]

}
