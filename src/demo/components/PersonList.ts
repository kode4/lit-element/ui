import {v4 as uuidv4} from "uuid";



/* Randomize array in-place using Durstenfeld shuffle algorithm */
export const shuffleArray = (array: Array<any>) => {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}

export const sortArray = (array: Array<Kode4UiDemoPerson>, field:'firstname'|'lastname'|'email'|'id', dir:'asc'|'desc'):Array<Kode4UiDemoPerson> => {
    const toSort = [...array];
    toSort.sort((a:Kode4UiDemoPerson, b:Kode4UiDemoPerson) => {
        const va = a[field];
        const vb = b[field];

        if (va === vb) {
            return 0;
        }

        if (dir === 'asc') {
            return va < vb ? -1 : 1;
        } else {
            return va < vb ? 1 : -1;
        }
    });
    return toSort;
}

export interface Kode4UiDemoPerson {
    id: string;
    firstname: string;
    lastname: string;
    email: string;
    gender: string;
}

export const loadNames = async (type: string, count: number) => {

    const url = `https://namey.muffinlabs.com/name.json?count=${count}&type=${type}&with_surname=true&frequency=common`;

    let response: Response = await fetch(url);

    const data: Array<Kode4UiDemoPerson> = [];
    (await response.json()).forEach((name: string) => {
        data.push({
            id: uuidv4(),
            firstname: name.split(' ')[0],
            lastname: name.split(' ')[1],
            email: `${name.split(' ')[0].toLowerCase()}@${name.split(' ')[1].toLowerCase()}.com`,
            gender: type,
        });
    })

    return data;
}

export const getMorePeople = async (type:string = 'all', count:number = 10) => {
    let newFemales:Array<Kode4UiDemoPerson> = [];
    let newMales:Array<Kode4UiDemoPerson> = [];
    const sizeFemale = Math.floor(count / 2);
    const sizeMale = Math.ceil(count / 2);
    while (newFemales.length < sizeFemale) {
        const newSize = Math.min(sizeFemale - newFemales.length, 10);
        newFemales = [
            ...newFemales,
            ...await loadNames(type === 'all' ? 'female' : type, newSize)
        ];
    }
    while (newMales.length < sizeMale) {
        const newSize = Math.min(sizeMale - newMales.length, 10)
        newMales = [
            ...newMales,
            ...await loadNames(type === 'all' ? 'male' : type, newSize)
        ];
    }
    // const newMales = await loadNames(type === 'all' ? 'male' : type, Math.ceil(count / 2));

    const newPeople: Array<Kode4UiDemoPerson> = shuffleArray([
        ...newFemales,
        ...newMales,
    ]);

    return newPeople;
}
