// import { LitElement, html, property, customElement } from "lit";
import { LitElement, html } from "lit";
import { customElement, property } from "lit/decorators.js";
import * as faIconsRegular from '@fortawesome/free-regular-svg-icons';
import * as faIconsSolid from '@fortawesome/free-solid-svg-icons';
import * as faIconsBrands from '@fortawesome/free-brands-svg-icons';
import {
    faArrowRight,
    faCircleNotch,
    faArrowLeft,
    faArrowUp,
    faArrowDown,
    faArrowCircleDown,
    faTimes
} from "@fortawesome/free-solid-svg-icons";

@customElement('kode4ui-demo-all-icons')
export class Kode4UiDemoAllIcons extends LitElement {

    @property({ type: Boolean })
    public showLabels:boolean = false;

    constructor() {
        super();
    }

    createRenderRoot() {
        return this;
    }

    render() {
        return html`
            <div>
                <div style="font-size: 48px;">
                    THIS <kode4-fa-icon .icon="${faCircleNotch}"></kode4-fa-icon>
                    IS A 
                    <kode4-fa-icon .icon="${faArrowRight}"></kode4-fa-icon>
                    <kode4-fa-icon .icon="${faArrowLeft}"></kode4-fa-icon>
                    <kode4-fa-icon .icon="${faArrowUp}"></kode4-fa-icon>
                    <kode4-fa-icon .icon="${faArrowDown}"></kode4-fa-icon>
                    text with a 
                    <kode4-fa-icon .icon="${faArrowCircleDown}"></kode4-fa-icon>
                    <kode4-fa-icon .icon="${faTimes}"></kode4-fa-icon>
                    few icons
                </div>
                <div>
                    ${this.showLabels ? this.renderIconsTable() : this.renderIconsFlat()}
                </div>
            </div>
        `;
    }

    renderIconsFlat() {
        return html`
            ${Object.values(faIconsRegular).map((i:any) => this.renderIconFlat(i))}
            ${Object.values(faIconsSolid).map((i:any) => this.renderIconFlat(i))}
            ${Object.values(faIconsBrands).map((i:any) => this.renderIconFlat(i))}
        `;
    }

    renderIconFlat(icon:any) {
        if (!icon || typeof icon !== 'object' || !icon.iconName || !icon.icon) {
            return html``;
        }

        try {
            return html`
                <kode4-fa-icon .icon="${icon}"></kode4-fa-icon>
            `;
        } catch (e) {
            return html``;
        }
    }

    renderIconsTable() {
        return html`
            <table>
                <tbody>
                    ${Object.values(faIconsRegular).map((i:any) => this.renderIconTable(i))}
                    ${Object.values(faIconsSolid).map((i:any) => this.renderIconTable(i))}
                    ${Object.values(faIconsBrands).map((i:any) => this.renderIconTable(i))}
                </tbody>
            </table>
        `;
    }

    renderIconTable(icon:any) {
        if (!icon || typeof icon !== 'object' || !icon.iconName || !icon.icon) {
            return html``;
        }

        try {
            return html `
                <tr>
                    <td>
                        <kode4-fa-icon .icon="${icon}"></kode4-fa-icon>
                    </td>
                    <td>
                        ${icon.prefix} ${icon.iconName}
                    </td>
                </tr>
            `;
        } catch (e) {
            return html``;
        }
    }
}
