// import { LitElement, html, customElement, PropertyValues } from "lit";
import { LitElement, html, PropertyValues } from "lit";
import { customElement } from "lit/decorators.js";

@customElement('kode4ui-demo-toolbars')
export class Kode4UiDemoToolbars extends LitElement {

    constructor() {
        super();
    }

    public firstUpdated(changedProperties:PropertyValues) {
        super.firstUpdated(changedProperties);
    }

    createRenderRoot() {
        return this;
    }

    render() {
        return html`
            <h1>Toolbars</h1>
            <div>
                <kode4-toolbar class="kode4-spacing" style="margin-bottom: 50px; border: solid 1px #f00;">
                    <div class="kode4-vcontent">
                        <div>
                            HEADER, LOGO OR WHATEVER
                        </div>
                        <div>
                            <a href="/dashboard">Dashboard</a>
                            <a href="/veranstaltungen">Alle Veranstaltungen</a>
                            <a href="/person">Mein Profil</a>
                        </div>
                    </div>

                    <div class="kode4-stretch"></div>

                    <div>
                        LOGGED IN THING
                    </div>
                </kode4-toolbar>

                <kode4-toolbar style="margin-bottom: 50px; border: solid 1px #f00;">
                    <div class="kode4-vcontent">
                        <div>
                            HEADER, LOGO OR WHATEVER
                        </div>
                        <div>
                            <a href="/dashboard">Dashboard</a>
                            <a href="/veranstaltungen">Alle Veranstaltungen</a>
                            <a href="/person">Mein Profil</a>
                        </div>
                    </div>

                    <div class="kode4-stretch"></div>

                    <div>
                        LOGGED IN THING
                    </div>
                </kode4-toolbar>

                <kode4-toolbar class="kode4-spacing" style="margin-bottom: 50px; border: solid 1px #f00;">
                    <kode4-toolbar class="">
                        <div>
                            1
                        </div>
                        <div>
                            2
                        </div>
                        <div>
                            3
                        </div>
                        <div>
                            4
                        </div>
                        <div>
                            5
                        </div>
                    </kode4-toolbar>

                    <div>
                        Ein Element
                    </div>

                    <div class="kode4-top">
                        <div style="background-color: #0f0; margin: 10px;">
                            TOP
                        </div>
                    </div>

                    <div>
                        <div style="background-color: #0f0;">
                            CENTER
                        </div>
                    </div>

                    <div class="kode4-bottom">
                        <div style="background-color: #0f0; margin: 0 10px;">
                            BOTTOM
                        </div>
                    </div>
                    
                    <div>
                        Ein Element                                                 
                    </div>

                    <div>
                        Ein Element
                    </div>

                    <div class="kode4-stretch">
                    </div>

                    <div style="color: #f00; font-size: 1.4em; line-height: 1.2em;">
                        Red Button
                    </div>
                </kode4-toolbar>
            </div>
            <div>
                <kode4-toolbar class="kode4-spacing" style="margin-bottom: 50px; border: solid 1px #f00;">
                    <kode4-toolbar class="">
                        <div>
                            1
                        </div>
                        <div>
                            2
                        </div>
                        <div>
                            3
                        </div>
                        <div>
                            4
                        </div>
                        <div>
                            5
                        </div>
                    </kode4-toolbar>

                    <div>
                        Ein Element
                    </div>

                    <div class="kode4-top">
                        <div style="background-color: #0f0; margin: 10px;">
                            TOP
                        </div>
                    </div>

                    <div>
                        <div style="background-color: #0f0;">
                            CENTER
                        </div>
                    </div>

                    <div class="kode4-bottom">
                        <div style="background-color: #0f0; margin: 0 10px;">
                            BOTTOM
                        </div>
                    </div>
                    
                    <div>
                        Ein Element
                        <br>
                        mit zwei Zeilen
                        <div style="position: absolute; background-color: #000; color: #fff; left: 0; top: 80%; width: 100%; height: 500px;">
                            some overlaypping element
                        </div>
                    </div>

                    <div style="height: 300px;">
                        Ein Element
                        <br>
                        mit height
                    </div>

                    <div class="kode4-stretch">
                    </div>

                    <div style="color: #f00; font-size: 1.4em; line-height: 1.2em;">
                        Red Button
                    </div>
                </kode4-toolbar>
            </div>
        `;
    }

}
