// import { LitElement, html, property, customElement } from "lit";
import { LitElement, html } from "lit";
import { customElement, property } from "lit/decorators.js";
import '../../../src/css/theme.css';

@customElement('kode4ui-demo-loaders')
export class Kode4UiDemoLoaders extends LitElement {

    @property({ type: String })
    public color:string = 'primary';

    @property({ type: Boolean })
    public loader1:boolean = false;

    constructor() {
        super();
    }

    createRenderRoot() {
        return this;
    }

    private showLoader1() {
        this.loader1 = true;
        setTimeout(() => {
            this.loader1 = false;
        }, 3000);
    }

    render() {
        return html`
            <h1>Loaders</h1>
            <br><br>
            <select value="${this.color}" @change="${(e:MouseEvent) => { console.log('CHANGE: '+(<HTMLSelectElement>e.target).value); this.color = (<HTMLSelectElement>e.target).value}}">
                <option>No color</option>
                <option value="primary" selected>primary</option>
                <option value="secondary">secondary</option>
                <option value="success">success</option>
                <option value="danger">danger</option>
                <option value="warning">warning</option>
                <option value="info">info</option>
            </select>
            <br><br>
            <button @click="${this.showLoader1}">Toggle Loader 1</button>

            <kode4-modalprogress .open="${this.loader1}" color="${this.color}"></kode4-modalprogress>
        `;
    }

}
