// import { LitElement, html, property, customElement, PropertyValues } from "lit";
import {LitElement, html, PropertyValues} from "lit";
import {customElement, property, query, state} from "lit/decorators.js";
import {ifDefined} from "lit/directives/if-defined.js";
import {faMars} from "@fortawesome/free-solid-svg-icons/faMars";
import {faVenus} from "@fortawesome/free-solid-svg-icons/faVenus";
import Kode4Tabs from "../../kode4-tabs";
import {IListStateObserver, ListStateHandler} from "../../ListStateHandler";
import {faFemale} from "@fortawesome/free-solid-svg-icons/faFemale";
import {faMale} from "@fortawesome/free-solid-svg-icons/faMale";
import {getMorePeople, Kode4UiDemoPerson, sortArray} from "../components/PersonList";

@customElement('kode4ui-demo-tables-in-tabs')
export class Kode4UiDemoTablesInTabs extends LitElement implements IListStateObserver {

    onOrderChange(): void {
        this.persons = sortArray(this.persons, <'firstname'|'lastname'|'email'|'id'>this.lsh.orderBy, this.lsh.orderByDir);
        this.requestUpdate();
    }

    @state()
    public lsh: ListStateHandler = new ListStateHandler({
        observer: this,
        fields: ['firstname', 'lastname', 'email', 'id']
    });

    @query('.kode4-tabs-selected')
    public k4tabs!: Kode4Tabs;

    @query('#listEndObserver')
    public listEndObserverTrigger!: HTMLElement;

    @property({type: Boolean, attribute: true})
    public stretch: boolean = false;

    @property({type: Boolean, attribute: true})
    public persons: Array<Kode4UiDemoPerson> = [];

    private loadMore = async () => {
        try {
            if (this.persons.length >= 1000) {
                console.log('List limit reached', this.persons.length);
                return;
            }

            if (this.lsh.orderBy) {
                this.persons = sortArray([
                    ...this.persons,
                    ...await getMorePeople(),
                ], <'firstname'|'lastname'|'email'|'id'>this.lsh.orderBy, this.lsh.orderByDir);
            } else {
                this.persons = [
                    ...this.persons,
                    ...await getMorePeople(),
                ];
            }

        } catch (e) {
            console.error(e);
        }
    }

    protected toggleLshOrder(field: string) {
        this.lsh.activateOrderByFieldOrToggleDir(field);
    }

    protected async firstUpdated(_changedProperties: PropertyValues) {
        super.firstUpdated(_changedProperties);
    }

    render() {
        return html`
            <kode4-tabs style="padding: 10px;" ?stretch=${this.stretch}>
                <kode4-tab name="the list" class="kode4-tabs-selected" style="background-color: #0ff;">
                    <kode4-fa-icon .icon="${faFemale}" slot="title"></kode4-fa-icon>
                    <kode4-fa-icon .icon="${faMale}" slot="title"></kode4-fa-icon>
                    <div slot="title">
                        &nbsp;&nbsp;&nbsp;List items: ${this.persons.length}
                    </div>
                    
                    <kode4-table @kode4-table-more="${this.loadMore}" .more="${this.loadMore}" obeserver-root="${this.stretch ? 'kode4-tab' : undefined}">
                        <kode4-tableheader style="width: 30px;" slot="header"></kode4-tableheader>
                        <kode4-tableheader 
                                slot="header" 
                                ?sortable="${this.lsh.isSortable('lastname')}"  
                                order="${ifDefined(this.lsh.getOrderByDirForField('lastname'))}" 
                                @kode4-sort="${() => this.toggleLshOrder('lastname')}">
                            Last name
                        </kode4-tableheader>
                        <kode4-tableheader 
                                slot="header"
                                ?sortable="${this.lsh.isSortable('firstname')}"
                                order="${ifDefined(this.lsh.getOrderByDirForField('firstname'))}"  
                                @kode4-sort="${() => this.toggleLshOrder('firstname')}">
                            First name
                        </kode4-tableheader>
                        <kode4-tableheader
                                slot="header"
                                ?sortable="${this.lsh.isSortable('email')}"
                                order="${ifDefined(this.lsh.getOrderByDirForField('email'))}"
                                @kode4-sort="${() => this.toggleLshOrder('email')}">
                            E-Mail
                        </kode4-tableheader>
                        <kode4-tableheader 
                                slot="header" 
                                ?sortable="${this.lsh.isSortable('id')}"
                                order="${ifDefined(this.lsh.getOrderByDirForField('id'))}"  
                                @kode4-sort="${() => this.toggleLshOrder('id')}">
                            ID
                        </kode4-tableheader>
        
                        ${this.persons.map((p: Kode4UiDemoPerson) => html`
                            <kode4-tablerow>
                                <div>
                                    ${p.gender === 'female' ? html`
                                        <kode4-fa-icon .icon="${faVenus}"></kode4-fa-icon>
                                    ` : undefined}
                                    ${p.gender === 'male' ? html`
                                        <kode4-fa-icon .icon="${faMars}"></kode4-fa-icon>
                                    ` : undefined}
                                </div>
                                <div>
                                    ${p.lastname}
                                </div>
                                <div>
                                    ${p.firstname}
                                </div>
                                <div>
                                    ${p.email}
                                </div>
                                <div>
                                    <small>
                                        ${p.id}
                                    </small>
                                </div>
                            </kode4-tablerow>
                        `)}
        
                    </kode4-table>
                </kode4-tab>
            </kode4-tabs>
        `;
    }

    createRenderRoot() {
        return this;
    }
}
