// import { LitElement, html, property, customElement, PropertyValues } from "lit";
import { LitElement, html, PropertyValues } from "lit";
import { customElement, property } from "lit/decorators.js";
import Kode4Growl from "../../../src/kode4-growl";

@customElement('kode4ui-demo-tooltips')
export class Kode4UiDemoToolstips extends LitElement {

    @property()
    public growl:Kode4Growl|null = null;

    constructor() {
        super();
    }

    public firstUpdated(changedProperties:PropertyValues) {
        super.firstUpdated(changedProperties);
        this.growl = this.querySelector('kode4-growl');
    }

    createRenderRoot() {
        return this;
    }

    render() {
        return html`
            <h1>Tooltips</h1>
            <div>
                <span style="display: inline-block; border: solid 2px #f00;">
                    <kode4-tooltip x="left" y="top">
                        Tooltip
                    </kode4-tooltip>
                    Top Left
                </span>

                <span style="display: inline-block; border: solid 2px #f00;">
                    <kode4-tooltip x="center" y="top">
                        Tooltip
                    </kode4-tooltip>
                    Top Center
                </span>

                <span style="display: inline-block; border: solid 2px #f00;">
                    <kode4-tooltip x="right" y="top">
                        Tooltip
                    </kode4-tooltip>
                    Top Right
                </span>
            </div>




            <div>
                <span style="display: inline-block; border: solid 2px #f00;">
                    <kode4-tooltip x="left" y="center">
                        Tooltip
                    </kode4-tooltip>
                    Center Left
                </span>

                <span style="display: inline-block; border: solid 2px #f00;">
                    <kode4-tooltip x="center" y="center">
                        Tooltip
                    </kode4-tooltip>
                    Center Center
                </span>

                <span style="display: inline-block; border: solid 2px #f00;">
                    <kode4-tooltip x="right" y="center">
                        Tooltip
                    </kode4-tooltip>
                    Center Right
                </span>
            </div>




            <div>
                <span style="display: inline-block; border: solid 2px #f00;">
                    <kode4-tooltip x="left" y="bottom">
                        Tooltip
                    </kode4-tooltip>
                    Bottom Left
                </span>

                <span style="display: inline-block; border: solid 2px #f00;">
                    <kode4-tooltip x="center" y="bottom">
                        Tooltip
                    </kode4-tooltip>
                    <div style="background-color: #fff; filter: brightness(75%) sepia(100%) hue-rotate(-50deg) saturate(600%);">
                        Bottom Center
                    </div>
                </span>

                <span style="display: inline-block; border: solid 2px #f00;">
                    <kode4-tooltip x="right" y="bottom">
                        Tooltip
                    </kode4-tooltip>
                    Bottom Right
                </span>
            </div>






            <div style="border: solid 2px #f00; padding: 10px; background-color: #c33; margin-top: 25px; width: 80%; margin-left: 20%;">
                <kode4-tooltip x="center" y="top">
                    Ich bin auch ein Tooltip
                </kode4-tooltip>
                Ein anderer Text
                <br>
                Ein anderer Text
                <br>
                Ein anderer Text
                <br>
                Ein anderer Text
                <br>
                Ein anderer Text
                <br>
                Ein anderer Text
                <br>
                Ein anderer Text
                <br>
                Ein anderer Text
                <br>
                Ein anderer Text
                <br>
                Ein anderer Text
                <br>
            </div>

            <div style="position: relative; border: solid 2px #f00; padding: 10px; background-color: #c33; margin-top: 25px; width: 60%; margin-left: 20%;">
                <kode4-tooltip>
                    Ich bin auch ein Tooltip
                </kode4-tooltip>
                Ein anderer Text
                <br>
                Ein anderer Text
                <br>
                Ein anderer Text
                <br>
                Ein anderer Text
                <br>
                Ein anderer Text
                <br>
                Ein anderer Text
                <br>
                Ein anderer Text
                <br>
                Ein anderer Text
                <br>
                Ein anderer Text
                <br>
                Ein anderer Text
                <br>
            </div>
        `;
    }

}
