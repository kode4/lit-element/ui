// import { LitElement, html, property, customElement, PropertyValues } from "lit";
import {LitElement, html} from "lit";
import {customElement} from "lit/decorators.js";
import {getGrowlMessenger} from "../../kode4-growl";

@customElement('kode4ui-demo-growl')
export class Kode4UiDemoGrowl extends LitElement {

    constructor() {
        super();
    }

    createRenderRoot() {
        return this;
    }

    private addDangerMessage(pos:string = 'br') {
        getGrowlMessenger().alert({
            message: html`Error!<br>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta ex impedit minima mollitia necessitatibus quia sit velit. Amet at aut, beatae culpa dolor error eum molestias numquam perspiciatis porro, quo.`,
            color: 'danger',
            timeout: 5000,
            pos,
        });
    }

    private addWarningMessage(pos:string = 'br') {
        getGrowlMessenger().alert({
            message: html`Warning!<br>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus alias amet animi aspernatur consequuntur debitis delectus eos exercitationem, inventore iusto necessitatibus perspiciatis quam quia quisquam repellendus similique soluta ullam velit?`,
            color: 'warning',
            timeout: 4000,
            pos,
        });
    }

    private addSuccessMessage(pos:string = 'br') {
        getGrowlMessenger().alert({
            message: html`Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea esse iure iusto laudantium obcaecati quam saepe. Ab, beatae earum magnam nemo nostrum officia veniam! Aperiam ex quae repellat repellendus soluta?`,
            color: 'success',
            pos,
        });
    }

    private addInfoMessage(pos:string = 'br') {
        getGrowlMessenger().alert({
            message: html`Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus ea fugiat itaque, necessitatibus quasi qui repudiandae voluptates. Adipisci assumenda commodi eos est exercitationem, illum incidunt iure labore quas similique, velit?`,
            color: 'info',
            timeout: 3000,
            pos,
        });
    }

    render() {
        return html`
            ${['t', 'tl', 'tr', 'b', 'bl', 'br'].map((key:string) => html`
                <div style="padding-bottom: 25px;">
                    ${key}:
                    &nbsp;&nbsp;&nbsp;
                    <button @click="${() => this.addDangerMessage(key)}">
                        Add error
                    </button>
                    <button @click="${() => this.addWarningMessage(key)}">
                        Add warning
                    </button>
                    <button @click="${() => this.addSuccessMessage(key)}">
                        Add success
                    </button>
                    <button @click="${() => this.addInfoMessage(key)}">
                        Add info
                    </button>
                </div>
            `)}
        `;
    }

}
