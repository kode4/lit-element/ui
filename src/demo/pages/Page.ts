import {html, LitElement} from "lit";
import {customElement, property, query, state} from "lit/decorators.js";

import {faPowerOff} from "@fortawesome/free-solid-svg-icons/faPowerOff";
import {faUser} from "@fortawesome/free-solid-svg-icons/faUser";
import {faBars} from "@fortawesome/free-solid-svg-icons/faBars";
import {faCircle} from "@fortawesome/free-regular-svg-icons/faCircle";
import Kode4GrowlAreas from "../../kode4-growl-areas";
import Kode4Applayout from "../../kode4-applayout";

@customElement('kode4ui-demo-page')
export default class Kode4UiDemopage extends LitElement {

    @query('kode4-applayout')
    private appLayout!: Kode4Applayout;

    @query('kode4-growl-areas')
    private growlAreas!: Kode4GrowlAreas;

    @property()
    private hmenuOpen: boolean = false;

    @property()
    private vmenuOpen: boolean = false;

    @property()
    private isAuthenticated: boolean = window.localStorage.hasOwnProperty('KODE4_UI_AUTHENTICATED') ? window.localStorage.getItem('KODE4_UI_AUTHENTICATED') === 'true' : false;

    @property()
    private menuDrawerOpen: boolean = window.localStorage.hasOwnProperty('KODE4_UI_MENUDRAWER_OPEN') ? window.localStorage.getItem('KODE4_UI_MENUDRAWER_OPEN') === 'true' : false;

    @property()
    private busy: boolean = false;

    @property()
    private menuLeftOpen: boolean = false;

    @property()
    private menuLeft2Open: boolean = false;

    @property()
    private menuRightOpen: boolean = false;

    @property({type: String})
    public modes: Array<string> = ['tooltips', 'growl', 'loaders', 'colors', 'icons', 'progress', 'dialog', 'draggable', 'toolbars', 'tabs', 'tables', 'tables-in-tabs', 'lists', 'liststatehandler'];

    @property({type: String})
    public mode: string = window.localStorage.getItem('KODE4_UI_DEMO_MODE') || this.modes[0];

    @state()
    private mWidth: Number = 250;

    @state()
    private contentSize: string = window.localStorage.getItem('KODE4_UI_DEMO_CONTENTSIZE') || 'auto';

    private relayMessages(e: CustomEvent) {
        this.growlAreas.alert(e.detail);
    }

    private toggleContentSize() {
        this.contentSize = this.contentSize === 'auto' ? 'full' : 'auto';
        window.localStorage.setItem('KODE4_UI_DEMO_CONTENTSIZE', this.contentSize);
    }

    private scrollBody() {
        this.appLayout.scrollBodyTo(300);
    }

    private toggleMenu(menu: string, state?: boolean) {
        switch (menu) {
            case 'left' :
                this.menuLeftOpen = state === undefined ? !this.menuLeftOpen : state;
                console.log('left', this.menuLeftOpen, state);
                if (this.menuLeftOpen) {
                    this.menuLeft2Open = false;
                }
                break;

            case 'left2' :
                this.menuLeft2Open = state === undefined ? !this.menuLeft2Open : state;
                if (this.menuLeft2Open) {
                    this.menuLeftOpen = false;
                }
                console.log('left', this.menuLeft2Open, state);
                break;

            case 'right' :
                this.menuRightOpen = state === undefined ? !this.menuRightOpen : state;
                console.log('right', this.menuRightOpen, state);
                break;
        }
        this.requestUpdate();
    }

    protected signin(e: Event) {
        e.preventDefault();
        this.isAuthenticated = true;
        window.localStorage.setItem('KODE4_UI_AUTHENTICATED', String(this.isAuthenticated));
    }

    protected signout(e: Event) {
        e.preventDefault();
        if (confirm('Do you really want to sign out?')) {
            this.isAuthenticated = false;
            window.localStorage.setItem('KODE4_UI_AUTHENTICATED', String(this.isAuthenticated));
        }
    }

    protected setMenuDrawerState(open?: boolean) {
        if (open === undefined) {
            this.menuDrawerOpen = !this.menuDrawerOpen;
        } else {
            this.menuDrawerOpen = open;
        }
        window.localStorage.setItem('KODE4_UI_MENUDRAWER_OPEN', String(this.menuDrawerOpen));
    }

    protected setBusy(busy?: boolean) {
        if (busy === undefined) {
            this.busy = !this.busy;
        } else {
            this.busy = busy;
        }
        console.log("this.busy", this.busy);
    }

    setMode(mode: string) {
        window.localStorage.setItem('KODE4_UI_DEMO_MODE', mode);
        this.mode = mode;
    }

    protected render() {
        return html`
            <kode4-applayout @kode4-growl-alert="${this.relayMessages}" @kode4-body-scrolled="${(e:CustomEvent) => console.log('[Kode4UiDemopage] kode4-body-scrolled ', e.detail.pos)}">
                <!-- HEADER -->
                <kode4-row slot="header" spacing class="k4d-header">
                    <div slot="title">
                        KODE4 UI Demo
                    </div>
                    
                    <span style="color: #eee;" @click="${() => this.toggleMenu('left')}">
<!--                        <kode4-growl id="growlTl" horizontal-align="left" vertical-align="top"></kode4-growl>-->
                        <kode4-fa-icon .icon="${faBars}"></kode4-fa-icon>
                    </span>

                    <kode4-row>
                        <span class="k4d-button-inverted">
                            Some Other Button
                        </span>

                        <span class="k4d-button-inverted">
                            Sign out
                            &nbsp;
                            <kode4-fa-icon .icon="${faPowerOff}"></kode4-fa-icon>
                        </span>

                    </kode4-row>


                    <span class="kode4-spacer"></span>

                    <span class="k4d-button-inverted" @click=${() => {
                            this.setBusy(true);
                            setTimeout(() => {
                                this.setBusy(false);
                            }, 5000);
                        }}>
                            Test Busy
                            <kode4-fa-icon .icon="${faCircle}"></kode4-fa-icon>
                        </span>

                    <span>
                        ${this.isAuthenticated ? html`
                            <span class="k4d-button-inverted" @click=${this.signout}>
                                Sign out
                                &nbsp;
                                <kode4-fa-icon .icon="${faPowerOff}"></kode4-fa-icon>
                            </span>
                        ` : html`
                            <span class="k4d-button-inverted" @click=${this.signin}>
                                Sign in
                                &nbsp;
                                <kode4-fa-icon .icon="${faUser}"></kode4-fa-icon>
                            </span>
                        `}
<!--                        <kode4-growl id="growlTr" vertical-align="top"></kode4-growl>-->
                    </span>
                </kode4-row>

                <!-- BUSY -->
                ${this.busy ? html`
                    <kode4-progress class="kode4themed" color="danger" indeterminate style="height: 3px;"
                                    slot="progressbar-top"></kode4-progress>
                    <kode4-progress color="primary" indeterminate style="height: 3px;"
                                    slot="progressbar-bottom"></kode4-progress>
                ` : ''}

                <!-- TRY LEFT -->
                <span @click="${() => this.toggleMenu('left')}" class="k4d-button" slot="tray-left">
                    ${this.menuLeftOpen ? 'Close' : 'Open'}
                </span>
                <span @click="${() => this.toggleMenu('left2')}" class="k4d-button" slot="tray-left">
                    ${this.menuLeft2Open ? 'Close' : 'Open'}
                </span>
                <span @click="${() => {
                    this.mWidth = this.mWidth === 250 ? 600 : 250;
                }}" class="k4d-button" slot="tray-left">
                    Toggle Width
                </span>
                <div class="kode4-spacer" slot="tray-left"></div>
                <span class="k4d-button kode4-applayout-tray-left-norotate" slot="tray-left">
                    Button
                </span>
                <span class="k4d-button" slot="tray-left">
                    <kode4-fa-icon .icon="${faPowerOff}"></kode4-fa-icon>
                    &nbsp;
                    Button
                </span>

                <!-- MENU LEFT -->
                <kode4-sidemenu
                        shadow
                        slot="menu-left"
                        ?open=${this.menuLeftOpen}>

                    ${this.isAuthenticated ? html`
                        <div style="background-color: #e0e0e0; border-bottom: solid 1px #ddd; padding: 20px; margin-bottom: 20px; min-height: 100px;" slot="header"
                             class="kode4-center">
                            <div style="text-align: center;">
                                <div class="kode4-image">
                                    <img src="../assets/dummy-agenturlogo.png">
                                </div>
                                <strong>Username</strong>
                            </div>
                    ` : ''}
                    </div>

                    <div class="kode4-center-h" slot="header">
                        <h2>Options</h2>
                    </div>

                    <div style="padding: 10px; min-width: 200px;">
                        <nav class="kode4-menu">
                            ${this.modes.map((el: any) => html`
                                <a href="#" @click="${() => {
                                    this.setMode(el);
                                }}" class="kode4-menu-item ${this.mode == el ? 'kode4-menu-item-active' : ''}">
                                    ${el}
                                </a>
                            `)}
                        </nav>
                    </div>

                    <div slot="footer" style="padding: 5px 20px; background-color: #e0e0e0">
                        <small><el>Choose an option</el></small>
                    </div>

                </kode4-sidemenu>

                <kode4-sidemenu
                        shadow
                        slot="menu-left"
                        ?open=${this.menuLeft2Open}>

                    <div class="kode4-center-h" slot="header">
                        <h2>Second Menu Title</h2>
                    </div>
                    <div style="width: ${this.mWidth}px;">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium beatae commodi ea
                        molestias velit. Animi architecto consequuntur eum ipsam iusto labore, nobis provident
                        quidem recusandae sequi soluta, vero voluptatum? Itaque.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium beatae commodi ea
                        molestias velit. Animi architecto consequuntur eum ipsam iusto labore, nobis provident
                        quidem recusandae sequi soluta, vero voluptatum? Itaque.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium beatae commodi ea
                        molestias velit. Animi architecto consequuntur eum ipsam iusto labore, nobis provident
                        quidem recusandae sequi soluta, vero voluptatum? Itaque.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium beatae commodi ea
                        molestias velit. Animi architecto consequuntur eum ipsam iusto labore, nobis provident
                        quidem recusandae sequi soluta, vero voluptatum? Itaque.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium beatae commodi ea
                        molestias velit. Animi architecto consequuntur eum ipsam iusto labore, nobis provident
                        quidem recusandae sequi soluta, vero voluptatum? Itaque.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium beatae commodi ea
                        molestias velit. Animi architecto consequuntur eum ipsam iusto labore, nobis provident
                        quidem recusandae sequi soluta, vero voluptatum? Itaque.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium beatae commodi ea
                        molestias velit. Animi architecto consequuntur eum ipsam iusto labore, nobis provident
                        quidem recusandae sequi soluta, vero voluptatum? Itaque.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium beatae commodi ea
                        molestias velit. Animi architecto consequuntur eum ipsam iusto labore, nobis provident
                        quidem recusandae sequi soluta, vero voluptatum? Itaque.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium beatae commodi ea
                        molestias velit. Animi architecto consequuntur eum ipsam iusto labore, nobis provident
                        quidem recusandae sequi soluta, vero voluptatum? Itaque.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium beatae commodi ea
                        molestias velit. Animi architecto consequuntur eum ipsam iusto labore, nobis provident
                        quidem recusandae sequi soluta, vero voluptatum? Itaque.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium beatae commodi ea
                        molestias velit. Animi architecto consequuntur eum ipsam iusto labore, nobis provident
                        quidem recusandae sequi soluta, vero voluptatum? Itaque.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium beatae commodi ea
                        molestias velit. Animi architecto consequuntur eum ipsam iusto labore, nobis provident
                        quidem recusandae sequi soluta, vero voluptatum? Itaque.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium beatae commodi ea
                        molestias velit. Animi architecto consequuntur eum ipsam iusto labore, nobis provident
                        quidem recusandae sequi soluta, vero voluptatum? Itaque.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium beatae commodi ea
                        molestias velit. Animi architecto consequuntur eum ipsam iusto labore, nobis provident
                        quidem recusandae sequi soluta, vero voluptatum? Itaque.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium beatae commodi ea
                        molestias velit. Animi architecto consequuntur eum ipsam iusto labore, nobis provident
                        quidem recusandae sequi soluta, vero voluptatum? Itaque.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium beatae commodi ea
                        molestias velit. Animi architecto consequuntur eum ipsam iusto labore, nobis provident
                        quidem recusandae sequi soluta, vero voluptatum? Itaque.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium beatae commodi ea
                        molestias velit. Animi architecto consequuntur eum ipsam iusto labore, nobis provident
                        quidem recusandae sequi soluta, vero voluptatum? Itaque.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium beatae commodi ea
                        molestias velit. Animi architecto consequuntur eum ipsam iusto labore, nobis provident
                        quidem recusandae sequi soluta, vero voluptatum? Itaque.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium beatae commodi ea
                        molestias velit. Animi architecto consequuntur eum ipsam iusto labore, nobis provident
                        quidem recusandae sequi soluta, vero voluptatum? Itaque.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium beatae commodi ea
                        molestias velit. Animi architecto consequuntur eum ipsam iusto labore, nobis provident
                        quidem recusandae sequi soluta, vero voluptatum? Itaque.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium beatae commodi ea
                        molestias velit. Animi architecto consequuntur eum ipsam iusto labore, nobis provident
                        quidem recusandae sequi soluta, vero voluptatum? Itaque.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium beatae commodi ea
                        molestias velit. Animi architecto consequuntur eum ipsam iusto labore, nobis provident
                        quidem recusandae sequi soluta, vero voluptatum? Itaque.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium beatae commodi ea
                        molestias velit. Animi architecto consequuntur eum ipsam iusto labore, nobis provident
                        quidem recusandae sequi soluta, vero voluptatum? Itaque.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium beatae commodi ea
                        molestias velit. Animi architecto consequuntur eum ipsam iusto labore, nobis provident
                        quidem recusandae sequi soluta, vero voluptatum? Itaque.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium beatae commodi ea
                        molestias velit. Animi architecto consequuntur eum ipsam iusto labore, nobis provident
                        quidem recusandae sequi soluta, vero voluptatum? Itaque.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium beatae commodi ea
                        molestias velit. Animi architecto consequuntur eum ipsam iusto labore, nobis provident
                        quidem recusandae sequi soluta, vero voluptatum? Itaque.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium beatae commodi ea
                        molestias velit. Animi architecto consequuntur eum ipsam iusto labore, nobis provident
                        quidem recusandae sequi soluta, vero voluptatum? Itaque.
                    </div>
                
                    <div slot="footer">
                        This is a footer.
                    </div>

                </kode4-sidemenu>

                <!-- BODY -->
                <div slot="body" style="max-width: 100%; background-color: #fff; display: block; height: 100%;" class="demo-body">
                    ${this.mode === 'icons' ? this.renderIcons() : ''}
                    ${this.mode === 'progress' ? this.renderProgress() : ''}
                    ${this.mode === 'growl' ? this.renderGrowl() : ''}
                    ${this.mode === 'colors' ? this.renderColors() : ''}
                    ${this.mode === 'loaders' ? this.renderLoaders() : ''}
                    ${this.mode === 'tooltips' ? this.renderTooltips() : ''}
                    ${this.mode === 'toolbars' ? this.renderToolbars() : ''}
                    ${this.mode === 'tabs' ? this.renderTabs() : ''}
                    ${this.mode === 'tables' ? this.renderTables() : ''}
                    ${this.mode === 'tables-in-tabs' ? this.renderTablesInTabs() : ''}
                    ${this.mode === 'lists' ? this.renderLists() : ''}
                    ${this.mode === 'liststatehandler' ? this.renderListstatehandler() : ''}
                    ${this.mode === 'dialog' ? this.renderDialogs() : ''}
                </div>

                <!-- MENU RIGHT -->
                <kode4-sidemenu
                        shadow
                        absolute
                        slot="menu-right"
                        ?open=${this.menuRightOpen}
                        right>

                    <div slot="header">
                        Title
                    </div>
                    <div style="width: 400px; overflow: auto; flex: 1 0 1px; padding: 10px;" class="kode4-stretch">

                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium beatae commodi ea
                        molestias velit. Animi architecto consequuntur eum ipsam iusto labore, nobis provident
                        quidem recusandae sequi soluta, vero voluptatum? Itaque.

                    </div>
                    <div slot="footer">
                        [+] [-]
                    </div>

                </kode4-sidemenu>

                <kode4-sidemenu
                        shadow
                        slot="menu-right"
                        alwaysopen
                        right>

                    <div slot="header">
                        Title 2
                    </div>
                    <div class="kode4-stretch kode4-center-h">

                        <div class="k4d-button" @click="${this.scrollBody}">
                            Scroll to position
                        </div>

                        <div class="k4d-button" @click="${this.toggleContentSize}">
                            Content-size
                            <br>
                            ${this.hmenuOpen}
                        </div>

                    </div>
                    <div slot="footer">
                        [+] [-]
                    </div>

                </kode4-sidemenu>

                <!-- TRAY RIGHT -->
                <span @click="${() => this.toggleMenu('right')}" class="k4d-button" slot="tray-right">
                    ${this.menuRightOpen ? 'Close' : 'Open'}
                </span>
                <span class="k4d-button" slot="tray-right">
                    Button
                </span>
                <span class="k4d-button" slot="tray-right" @click="${() => {
                    this.hmenuOpen = true;
                }}">
                    H-Menu Button
                </span>
                <span class="k4d-button" slot="tray-right" @click="${() => {
                    this.vmenuOpen = true;
                }}">
                    V-Menu Button
                </span>

                <!-- FOOTER -->
                <kode4-row slot="footer" spacing class="k4d-footer">
                    <span>
<!--                        <kode4-growl id="growlBl" horizontal-align="left"></kode4-growl>-->
                        Footer
                    </span>
                    
                    <span class="kode4-stretch"></span>

                    <span>
                        &copy; KODE4
<!--                        <kode4-growl id="growlBr"></kode4-growl>-->
                    </span>

                </kode4-row>

            </kode4-applayout>

            <kode4-drawer name="left-drawer" left ?open="${this.hmenuOpen}" @close="${() => this.hmenuOpen = false}">
                <kode4-card stretch style="width: 250px;">
                    ${this.isAuthenticated ? html`
                        <div style="background-color: #e0e0e0; border-bottom: solid 1px #ddd; padding: 20px; min-height: 100px;" slot="header"
                             class="kode4-center">
                            <div style="text-align: center;">
                                <div class="kode4-image">
                                    <img src="../assets/dummy-agenturlogo.png">
                                </div>
                                <strong>Username</strong>
                            </div>
                    ` : undefined}

                    <kode4-nav href="https://www.google.de" target="_blank" @click="${(e:MouseEvent) => {
                        e.preventDefault();
                    }}">
                        Go to Google
                    </kode4-nav>
                    ${this.modes.map((el: any) => html`
                        <kode4-nav ?active="${this.mode == el}" @click="${(e:MouseEvent) => { e.preventDefault(); this.setMode(el); }}">
                            ${el}
                        </kode4-nav>
                    `)}
    
                    <div slot="footer" style="padding: 5px 20px; background-color: #e0e0e0">
                        <small><el>Choose an option</el></small>
                    </div>
                </kode4-card>
            </kode4-drawer>
            
            <kode4-drawer name="bottom-drawer" bottom ?open="${this.vmenuOpen}" @close="${() => this.vmenuOpen = false}">
                <kode4-card stretch fixed-width>
                    ${this.isAuthenticated ? html`
                        <div style="background-color: #e0e0e0; border-bottom: solid 1px #ddd; padding: 20px; margin-bottom: 20px; min-height: 100px;" slot="header"
                             class="kode4-center">
                            <div style="text-align: center;">
                                <div class="kode4-image">
                                    <img src="../assets/dummy-agenturlogo.png">
                                </div>
                                <strong>Username</strong>
                            </div>
                    ` : ''}
                    </div>

                    <div class="kode4-center-h" slot="header">
                        <h2>Options</h2>
                    </div>

                    <div style="padding: 10px; width: 250px; ">
                        <nav class="kode4-menu">
                            ${this.modes.map((el: any) => html`
                                <a href="#" @click="${() => {
                                this.setMode(el);
                            }}" class="kode4-menu-item ${this.mode == el ? 'kode4-menu-item-active' : ''}">
                                    ${el}
                                </a>
                            `)}
                        </nav>
                    </div>

                    <div slot="footer" style="padding: 5px 20px; background-color: #e0e0e0">
                        <small><el>Choose an option</el></small>
                    </div>
                </kode4-card>
            </kode4-drawer>
            
            <kode4-growl-areas></kode4-growl-areas>
        `;
    }

    protected renderIcons() {
        return html`
            <div class="kode4demo-section">
                <kode4ui-demo-all-icons></kode4ui-demo-all-icons>
            </div>

            <div class="kode4demo-section">
                <kode4ui-demo-all-icons showLabels></kode4ui-demo-all-icons>
            </div>
        `;
    }

    protected renderProgress() {
        return html`

                <kode4ui-demo-progress></kode4ui-demo-progress>

        `;
    }

    protected renderGrowl() {
        return html`

                <kode4ui-demo-growl></kode4ui-demo-growl>

        `;
    }

    protected renderColors() {
        return html`
                <kode4ui-demo-colors></kode4ui-demo-colors>
        `;
    }

    protected renderLoaders() {
        return html`

                <kode4ui-demo-loaders></kode4ui-demo-loaders>

        `;
    }

    protected renderTooltips() {
        return html`

                <kode4ui-demo-tooltips></kode4ui-demo-tooltips>

        `;
    }

    protected renderToolbars() {
        return html`

                <kode4ui-demo-toolbars></kode4ui-demo-toolbars>

        `;
    }

    protected renderTabs() {
        return html`
            <kode4ui-demo-tabs style="display: block; ${this.contentSize === 'full' ? 'height: 100%;' : 'height: auto;'}" ?stretch="${this.contentSize === 'full'}" @toggle-tab-style="${this.toggleContentSize}"></kode4ui-demo-tabs>
        `;
    }

    protected renderTables() {
        return html`
            <kode4ui-demo-tables style="display: block; ${this.contentSize === 'full' ? 'height: 100%;' : 'height: auto;'}" ?stretch="${this.contentSize === 'full'}"></kode4ui-demo-tables>
        `;
    }

    protected renderTablesInTabs() {
        return html`
            <kode4ui-demo-tables-in-tabs style="display: block; ${this.contentSize === 'full' ? 'height: 100%;' : 'height: auto;'}" ?stretch="${this.contentSize === 'full'}"></kode4ui-demo-tables-in-tabs>
        `;
    }

    protected renderLists() {
        return html`
            <kode4ui-demo-lists></kode4ui-demo-lists>
        `;
    }

    protected renderListstatehandler() {
        return html`
            <kode4ui-demo-liststatehandler></kode4ui-demo-liststatehandler>
        `;
    }

    protected renderDialogs() {
        return html`
            <kode4ui-demo-dialogs></kode4ui-demo-dialogs>
        `;
    }

    createRenderRoot() {
        return this;
    }

}
