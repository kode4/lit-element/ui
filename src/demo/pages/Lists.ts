import {LitElement, html, PropertyValues} from "lit";
import {customElement, property, query, queryAsync, state} from "lit/decorators.js";
import {IListStateObserver, ListStateHandler} from "../../ListStateHandler";
import {getMorePeople, Kode4UiDemoPerson, sortArray} from "../components/PersonList";
import Kode4Menu, {kode4Menus} from "../../kode4-menu";

@customElement('kode4ui-demo-lists')
export class Kode4UiDemoLists extends LitElement implements IListStateObserver {

    onOrderChange(): void {
        this.persons = sortArray(this.persons, <'firstname'|'lastname'|'email'|'id'>this.lsh.orderBy, this.lsh.orderByDir);
        this.requestUpdate();
    }

    @query('.k4d-movable-button')
    private movableButton!:HTMLElement;

    @state()
    public movableButtonMenuVisible:boolean = false;

    @query('.k4d-movable-button-menu')
    private movableButtonMenu!:Kode4Menu;

    @state()
    public lsh: ListStateHandler = new ListStateHandler({
        fields: ['firstname', 'lastname', 'email', 'id'],
        observer: this,
    });

    @queryAsync('kode4-menu[context-menu]')
    public contextMenu!: Kode4Menu;

    @query('#listEndObserver')
    public listEndObserverTrigger!: HTMLElement;

    @state()
    private staticListsVisible: boolean = false;

    @state()
    private staticHiddenListsVisible: boolean = false;

    @property({type: Boolean, attribute: true})
    public persons: Array<Kode4UiDemoPerson> = [];

    protected firstUpdated(_changedProperties: PropertyValues) {
        super.firstUpdated(_changedProperties);
        console.log(' >> FIRST UDPATED');
        console.log(this.contextMenu);
    }

    public loadMore = async () => {
        try {
            if (this.persons.length >= 1000) {
                console.log('List limit reached', this.persons.length);
                return;
            }

            if (this.lsh.orderBy) {
                this.persons = sortArray([
                    ...this.persons,
                    ...await getMorePeople(),
                ], <'firstname'|'lastname'|'email'|'id'>this.lsh.orderBy, this.lsh.orderByDir);
            } else {
                this.persons = [
                    ...this.persons,
                    ...await getMorePeople(),
                ];
            }

        } catch (e) {
            console.error(e);
        }
    }

    protected toggleStaticLists() {
        this.staticListsVisible = !this.staticListsVisible;
    }

    protected toggleHiddenStaticList() {
        this.staticHiddenListsVisible = !this.staticHiddenListsVisible;
    }

    protected toggleContextmenu(e:MouseEvent) {
        console.log('GO FOR CONTEXTMENU', e);
        e.preventDefault();
        this.contextMenu?.show({
            x: e.pageX,
            y: e.pageY,
        });
    }

    connectedCallback() {
        this.loadMore();
        super.connectedCallback();
    }

    @state()
    private dragOffsetX:number = 0;

    @state()
    private dragOffsetY:number = 0;

    handleDragStart(e:DragEvent) {
        this.dragOffsetX = e.offsetX;
        this.dragOffsetY = e.offsetY;
        const img = document.createElement('img');
        img.src = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
        e.dataTransfer?.setDragImage(img, 0, 0);
    }

    handleDrag(e:DragEvent) {
        if (e.y !== 0 && e.x !== 0) {
            this.movableButton.style.top = `${e.y - this.dragOffsetY}px`;
            this.movableButton.style.left = `${e.x - this.dragOffsetX}px`;
        }
    }

    handleDragEnd(e:DragEvent) {
        this.movableButton.style.top = `${e.y - this.dragOffsetY}px`;
        this.movableButton.style.left = `${e.x - this.dragOffsetX }px`;
        this.dragOffsetX = 0;
        this.dragOffsetY = 0;
    }

    showMovableButtonContextMenu(e:MouseEvent) {
        console.log('--- ignore old click handler ---', e, this.movableButtonMenu);
        // if (!e.defaultPrevented) {
        //     this.movableButtonMenu.show();
        // }
    }

    render() {
        return html`
            <div style="border: solid 2px #f00; height: 100%; position: relative;" @contextmenu="${async (e:MouseEvent) => (await this.contextMenu).onShow(e) }">
                <!--
                <kode4-list floating style="top: 50%; left: 50%;">
                    <div>Load more</div>
                    <div @click="${this.toggleStaticLists}">Toggle Static Lists</div>
                    <div @click="${this.toggleHiddenStaticList}">Toggle Hidden Static List</div>
                </kode4-list>
                -->
                
                <div>
                    <div class="k4d-button" style="margin: 25px;">
                        Menu trigger 1
                        <kode4-trigger menu="somelist" parent caller-params="1"></kode4-trigger>
                    </div>
    
                    <div class="k4d-button" style="margin: 25px;">
                        Menu trigger 2
                        <kode4-trigger menu="somelist" parent caller-params="2"></kode4-trigger>
                    </div>
    
                    <div class="k4d-button" style="margin: 25px;">
                        Menu trigger 3
                        <kode4-trigger menu="somelist" parent caller-params="3"></kode4-trigger>
                    </div>
                </div>

                <div style="margin-top: 40vh">
                    <div class="k4d-button" style="margin: 25px;">
                        Menu trigger 4
                        <kode4-trigger menu="somelist" parent caller-params="4"></kode4-trigger>
                    </div>
    
                    <div class="k4d-button" style="margin: 25px;">
                        Menu trigger 5
                        <kode4-trigger menu="somelist" parent caller-params="5"></kode4-trigger>
                    </div>
    
                    <div class="k4d-button" style="margin: 25px;">
                        Menu trigger 6
                        <kode4-trigger menu="somelist" parent caller-params="6"></kode4-trigger>
                    </div>
                </div>

                <kode4-menu name="somelist" right top cover bound>
                    <div @click="${(e:MouseEvent) => {
                        console.log('[Kode4UiDemoLists] option 1 on menu', kode4Menus.get('somelist')!.getCallerParams());
                        e.preventDefault();
                    }}">
                        Option 1
                    </div>
                    <div @click="${() => {
                        const menu = kode4Menus.get('somelist')!;
                        console.log('[Kode4UiDemoLists] option 2 on menu', menu.getCallerParams());
                        menu.hide();
                    }}">
                        Option 2
                    </div>
                    <div @click="${() => {
                        console.log('[Kode4UiDemoLists] option 3 on menu', kode4Menus.get('somelist')!.getCallerParams(), ' DO NOT CLOSE MENU!');
                    }}">
                        Option 3
                    </div>
                    <div @click="${() => {
                        const menu = kode4Menus.get('somelist')!;
                        console.log('[Kode4UiDemoLists] option 4 on menu', menu.getCallerParams());
                        menu.hide();
                    }}">
                        Option 4
                    </div>
                </kode4-menu>
    
                <kode4-menu floating context-menu backdrop>
                    ${this.persons.map((p:Kode4UiDemoPerson) => html`
                        <div @click="${(e:MouseEvent) => {
                            console.log('Handle Menu-Item Klick');
                            e.preventDefault();
                        }}">${p.firstname} ${p.lastname}</div>
                    `)}
                </kode4-menu>
                
                <div class="k4d-movable-button" style="position: fixed; z-index: 2000; top: 30%; left: 35%; border: solid 2px #ccc; background-color: #fff; padding: 10px;" @click="${this.showMovableButtonContextMenu}" draggable @dragstart="${this.handleDragStart}" @dragend="${this.handleDragEnd}" @drag="${this.handleDrag}">
                    <div style="">
                        Drag me around und click me
                        <br>
                        to open a context menu
                    </div>
                    <kode4-menu class="k4d-movable-button-menu" parent top right cover>
                        ${this.persons.map((p:Kode4UiDemoPerson) => html`
                            <div @click="${(e:MouseEvent) => {
                                console.log('Handle Menu-Item Klick');
                                e.preventDefault();
                            }}">${p.firstname} ${p.lastname}</div>
                        `)}
                    </kode4-menu>
                </div>

                ${this.staticListsVisible ? html`
                    <kode4-list inline ?hidden="${this.staticHiddenListsVisible}">
                        ${this.persons.map((p:Kode4UiDemoPerson) => html`
                            <div>${p.firstname} ${p.lastname}</div>
                        `)}
                    </kode4-list>
    
                    <kode4-list floating top right>
                        ${this.persons.map((p:Kode4UiDemoPerson) => html`
                            <div>${p.firstname} ${p.lastname}</div>
                        `)}
                    </kode4-list>
    
                    <kode4-list floating bottom right>
                        ${this.persons.map((p:Kode4UiDemoPerson) => html`
                            <div>${p.firstname} ${p.lastname}</div>
                        `)}
                    </kode4-list>
    
                    <kode4-list floating bottom left>
                        ${this.persons.map((p:Kode4UiDemoPerson) => html`
                            <div>${p.firstname} ${p.lastname}</div>
                        `)}
                    </kode4-list>
                ` : undefined}
            </div>
        `;
    }

    createRenderRoot() {
        return this;
    }
}
