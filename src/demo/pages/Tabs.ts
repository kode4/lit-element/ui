// import { LitElement, html, property, customElement, PropertyValues } from "lit";
import {LitElement, html} from "lit";
import {customElement, property} from "lit/decorators.js";

@customElement('kode4ui-demo-tabs')
export class Kode4UiDemoTabs extends LitElement {

    @property({type: Boolean, attribute: true})
    public stretch: boolean = false;

    private toggleTabStyle() {
        this.dispatchEvent(new CustomEvent("toggle-tab-style", {
            bubbles: true,
            composed: true,
        }));
    }

    render() {
        return html`
            <kode4-tabs style="padding: 10px;" ?stretch=${this.stretch}>

                <kode4-tab name="arr1-1" style="background-color: #0f0;">
                    <span slot="title">The First</span>
                    <h1>The first Tab</h1>
                    <div class="k4d-button" @click="${this.toggleTabStyle}">
                        Klick here to toggle tab style
                    </div>
                    <br>
                    <br>
                    <br>
                </kode4-tab>

                <kode4-tab name="and" class="kode4-tabs-selected" style="background-color: #0ff;">
                    <h1>ARR 2</h1>
                    Tab 2
                </kode4-tab>

                <kode4-tab style="background-color: #00f;">
                    <span slot="title">
                        <strong>ALWAYS</strong>
                        <br><em>-------------</em>
                    </span>
                    <h1>ARR 3</h1>
                    Tab 3
                </kode4-tab>

                <kode4-tab style="background-color: #f0f;">
                    <h1>ARR 4</h1>
                    Tab 4
                </kode4-tab>

                <kode4-tab name="and last" style="background-color: #ff0;">
                    <h1>ARR 4</h1>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus excepturi exercitationem facilis
                    maxime porro possimus quae. Ad delectus dolore earum in iusto laudantium mollitia, nisi odio quam
                    repudiandae tempore vero.
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo id in pariatur reiciendis?
                    Cumque dicta iusto neque nobis porro repudiandae totam? Eius optio, vero? Architecto debitis
                    doloribus ipsa libero ullam!
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto at blanditiis cum esse modi
                    molestias, reiciendis suscipit. Cumque dicta eveniet laboriosam nisi possimus quidem repudiandae
                    temporibus unde velit voluptatibus. Explicabo?
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet eum iure nihil. Aperiam at aut
                    consequatur cupiditate eum, ex fugit inventore libero nemo pariatur perferendis quas quisquam
                    repellendus. Esse, libero.
                    <br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus excepturi exercitationem facilis
                    maxime porro possimus quae. Ad delectus dolore earum in iusto laudantium mollitia, nisi odio quam
                    repudiandae tempore vero.
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo id in pariatur reiciendis?
                    Cumque dicta iusto neque nobis porro repudiandae totam? Eius optio, vero? Architecto debitis
                    doloribus ipsa libero ullam!
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto at blanditiis cum esse modi
                    molestias, reiciendis suscipit. Cumque dicta eveniet laboriosam nisi possimus quidem repudiandae
                    temporibus unde velit voluptatibus. Explicabo?
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet eum iure nihil. Aperiam at aut
                    consequatur cupiditate eum, ex fugit inventore libero nemo pariatur perferendis quas quisquam
                    repellendus. Esse, libero.
                    <br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus excepturi exercitationem facilis
                    maxime porro possimus quae. Ad delectus dolore earum in iusto laudantium mollitia, nisi odio quam
                    repudiandae tempore vero.
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo id in pariatur reiciendis?
                    Cumque dicta iusto neque nobis porro repudiandae totam? Eius optio, vero? Architecto debitis
                    doloribus ipsa libero ullam!
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto at blanditiis cum esse modi
                    molestias, reiciendis suscipit. Cumque dicta eveniet laboriosam nisi possimus quidem repudiandae
                    temporibus unde velit voluptatibus. Explicabo?
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet eum iure nihil. Aperiam at aut
                    consequatur cupiditate eum, ex fugit inventore libero nemo pariatur perferendis quas quisquam
                    repellendus. Esse, libero.
                    <br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus excepturi exercitationem facilis
                    maxime porro possimus quae. Ad delectus dolore earum in iusto laudantium mollitia, nisi odio quam
                    repudiandae tempore vero.
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo id in pariatur reiciendis?
                    Cumque dicta iusto neque nobis porro repudiandae totam? Eius optio, vero? Architecto debitis
                    doloribus ipsa libero ullam!
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto at blanditiis cum esse modi
                    molestias, reiciendis suscipit. Cumque dicta eveniet laboriosam nisi possimus quidem repudiandae
                    temporibus unde velit voluptatibus. Explicabo?
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet eum iure nihil. Aperiam at aut
                    consequatur cupiditate eum, ex fugit inventore libero nemo pariatur perferendis quas quisquam
                    repellendus. Esse, libero.
                    <br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus excepturi exercitationem facilis
                    maxime porro possimus quae. Ad delectus dolore earum in iusto laudantium mollitia, nisi odio quam
                    repudiandae tempore vero.
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo id in pariatur reiciendis?
                    Cumque dicta iusto neque nobis porro repudiandae totam? Eius optio, vero? Architecto debitis
                    doloribus ipsa libero ullam!
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto at blanditiis cum esse modi
                    molestias, reiciendis suscipit. Cumque dicta eveniet laboriosam nisi possimus quidem repudiandae
                    temporibus unde velit voluptatibus. Explicabo?
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet eum iure nihil. Aperiam at aut
                    consequatur cupiditate eum, ex fugit inventore libero nemo pariatur perferendis quas quisquam
                    repellendus. Esse, libero.
                    <br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus excepturi exercitationem facilis
                    maxime porro possimus quae. Ad delectus dolore earum in iusto laudantium mollitia, nisi odio quam
                    repudiandae tempore vero.
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo id in pariatur reiciendis?
                    Cumque dicta iusto neque nobis porro repudiandae totam? Eius optio, vero? Architecto debitis
                    doloribus ipsa libero ullam!
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto at blanditiis cum esse modi
                    molestias, reiciendis suscipit. Cumque dicta eveniet laboriosam nisi possimus quidem repudiandae
                    temporibus unde velit voluptatibus. Explicabo?
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet eum iure nihil. Aperiam at aut
                    consequatur cupiditate eum, ex fugit inventore libero nemo pariatur perferendis quas quisquam
                    repellendus. Esse, libero.
                    <br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus excepturi exercitationem facilis
                    maxime porro possimus quae. Ad delectus dolore earum in iusto laudantium mollitia, nisi odio quam
                    repudiandae tempore vero.
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo id in pariatur reiciendis?
                    Cumque dicta iusto neque nobis porro repudiandae totam? Eius optio, vero? Architecto debitis
                    doloribus ipsa libero ullam!
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto at blanditiis cum esse modi
                    molestias, reiciendis suscipit. Cumque dicta eveniet laboriosam nisi possimus quidem repudiandae
                    temporibus unde velit voluptatibus. Explicabo?
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet eum iure nihil. Aperiam at aut
                    consequatur cupiditate eum, ex fugit inventore libero nemo pariatur perferendis quas quisquam
                    repellendus. Esse, libero.
                    <br><br>
                </kode4-tab>

            </kode4-tabs>
        `;
    }

    createRenderRoot() {
        return this;
    }
}
