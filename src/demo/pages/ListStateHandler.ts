// import { LitElement, html, property, customElement, PropertyValues } from "lit";
import {LitElement, html, PropertyValues} from "lit";
import {customElement, query, state} from "lit/decorators.js";
import {getMorePeople, Kode4UiDemoPerson, sortArray} from "../components/PersonList";
import {IListStateObserver, ListStateHandler} from "../../ListStateHandler";

const lsh = new ListStateHandler();


@customElement('kode4ui-demo-liststatehandler')
export class Kode4UiDemoListStateHandler extends LitElement implements IListStateObserver {

    onOrderChange(): void {
        console.log('LIST CHANGED!');
        this.lsh.page = 0;
        this.lsh.setState('scrollTop', 0);
        this.container.scrollTop = 0;
        this.loadList();
    }

    @state()
    public persons: Array<Kode4UiDemoPerson> = [];

    @state()
    public lsh: ListStateHandler = new ListStateHandler({
        fields: ['firstname', 'lastname', 'email', 'id'],
        filters: new Map([
            ['searchterm', undefined],
            ['gender', 'all'],
        ]),
        pageSize: 20,
        observer: this,
    });

    private loadList = async () => {
        try {
            const size:number = (this.lsh.page + 1) * this.lsh.pageSize;
            this.persons = [
                ...await getMorePeople(this.lsh.getFilter('gender'), size),
            ];

            if (this.lsh.orderBy) {
                this.persons = sortArray(this.persons, <'firstname'|'lastname'|'email'|'id'>lsh.orderBy, lsh.orderByDir);
            }

        } catch (e) {
            console.error(e);
        } finally {
            this.handleAfterListLoaded();
        }
    }

    protected loadMore = async () => {
        try {
            const newPeople = await getMorePeople(this.lsh.getFilter('gender'), this.lsh.pageSize)
            this.persons = [
                ...this.persons,
                ...newPeople,
            ];

            if (this.lsh.orderBy) {
                this.persons = sortArray([
                    ...this.persons,
                ], <'firstname'|'lastname'|'email'|'id'>lsh.orderBy, lsh.orderByDir);
            }

        } catch (e) {
            console.error(e);
        } finally {
            this.handleAfterListLoaded();
        }
    }

    protected loadNextPage = async () => {
        try {
            const newPeople = await getMorePeople(this.lsh.getFilter('gender'), this.lsh.pageSize)
            this.persons = [
                ...this.persons,
                ...newPeople,
            ];

            if (this.lsh.orderBy) {
                this.persons = sortArray([
                    ...this.persons,
                ], <'firstname'|'lastname'|'email'|'id'>lsh.orderBy, lsh.orderByDir);
            }

            this.lsh.page += 1;

        } catch (e) {
            console.error(e);
        } finally {
            this.handleAfterListLoaded();
        }
    }

    private handleAfterListLoaded() {
        this.lsh.size = this.persons.length;
        // this.lsh.update();
    }

    protected firstUpdated(_changedProperties: PropertyValues) {
        super.firstUpdated(_changedProperties);
        // this.loadList();
        this.restoreLsh();
    }

    @query('div.container')
    private container!:HTMLElement;

    @query('form')
    private theForm!:HTMLFormElement;

    @query('input[name="searchterm"]')
    private fieldSearchterm!:HTMLInputElement;

    @query('select[name="gender"]')
    private fieldGender!:HTMLSelectElement;

    private searchtermChanged() {
        this.lsh.setFilter('searchterm', this.fieldSearchterm.value);
    }

    private genderChanged() {
        this.lsh.setFilter('gender', this.fieldGender.value);
    }


    private storeLsh() {
        // window.localStorage.setItem('KODE4_UI_DEMO_LSH', this.lsh.toString());
    }

    private async restoreLsh() {
        const serialized = window.localStorage.getItem('KODE4_UI_DEMO_LSH');
        if (!serialized) {
            console.warn('CANNOT RE-STORE, EMPTY STRING');
            return;
        }
        // this.lsh.fromString(serialized);
        await this.loadList();
        this.requestUpdate();
        this.theForm!.reset();
        if (this.lsh.hasState('scrollTop')) {
            this.container.scrollTop = this.lsh.getState('scrollTop');
        }
    }

    private onContainerScrollTopChanged() {
        this.lsh.setState('scrollTop', this.container.scrollTop);
    }



    /*

    store/restore über localstorage oder applicationStorage

    page size, limit statt size

     */
    render() {
        return html`
            <div class="container" style="position: relative; width: 100%; height: calc(100% - 5px); overflow: auto;" @scroll="${this.onContainerScrollTopChanged}">
                <kode4-row style="background-color: #fff; padding: 10px; border-bottom: solid 1px #ccc; position: sticky; top: 0;">
                    <form action="/" @submit="${(e:Event) => e.preventDefault()}">
                        <input autocomplete="off" type="text" name="searchterm" value="${this.lsh.getFilter('searchterm')}" @change="${this.searchtermChanged}">
                        <select name="gender" @change="${this.genderChanged}">
                            <option value="all" ?selected="${this.lsh.getFilter('gender')==='all'}">All</option>
                            <option value="female" ?selected="${this.lsh.getFilter('gender')==='female'}">Female</option>
                            <option value="male" ?selected="${this.lsh.getFilter('gender')==='male'}">Male</option>
                        </select>
                    </form>
                    <button @click="${this.loadNextPage}">Next Page</button>
                    &nbsp;&nbsp;
                    <button @click="${this.storeLsh}">Store</button>
                    <button @click="${this.restoreLsh}">Restore</button>
                    &nbsp;&nbsp;
                    <div class="kode4-stretch"></div>
                    Size: ${this.lsh.size}
                    Page: ${this.lsh.page}
                    Total: ${this.lsh.total}
                </kode4-row>
                
                <kode4-list>
                    ${this.persons.map((p:Kode4UiDemoPerson) => html`
                        <div>${p.firstname} ${p.lastname}</div>
                    `)}
                </kode4-list>
            </div>
        `;
    }

    createRenderRoot() {
        return this;
    }
}
