import {LitElement, html, PropertyValues} from "lit";
import {customElement, query, queryAsync, state} from "lit/decorators.js";
import Kode4Menu, {kode4Menus} from "../../kode4-menu";
import {kode4Dialogs} from "../../kode4-dialog";
import {getGrowlMessenger} from "../../kode4-growl";

@customElement('kode4ui-demo-dialogs')
export class Kode4UiDemoDialogs extends LitElement {

    @query('.k4d-movable-button')
    private movableButton!:HTMLElement;

    @state()
    public movableButtonMenuVisible:boolean = false;

    @query('.k4d-movable-button-menu')
    private movableButtonMenu!:Kode4Menu;

    @queryAsync('kode4-menu[context-menu]')
    public contextMenu!: Kode4Menu;

    @query('#listEndObserver')
    public listEndObserverTrigger!: HTMLElement;

    @state()
    private staticListsVisible: boolean = false;

    @state()
    private staticHiddenListsVisible: boolean = false;

    protected firstUpdated(_changedProperties: PropertyValues) {
        super.firstUpdated(_changedProperties);
        console.log(' >> FIRST UDPATED');
        console.log(this.contextMenu);
    }

    protected toggleStaticLists() {
        this.staticListsVisible = !this.staticListsVisible;
    }

    protected toggleHiddenStaticList() {
        this.staticHiddenListsVisible = !this.staticHiddenListsVisible;
    }

    protected toggleContextmenu(e:MouseEvent) {
        console.log('GO FOR CONTEXTMENU', e);
        e.preventDefault();
        this.contextMenu?.show({
            x: e.pageX,
            y: e.pageY,
        });
    }

    connectedCallback() {
        super.connectedCallback();
    }

    @state()
    private dragOffsetX:number = 0;

    @state()
    private dragOffsetY:number = 0;

    handleDragStart(e:DragEvent) {
        this.dragOffsetX = e.offsetX;
        this.dragOffsetY = e.offsetY;
        const img = document.createElement('img');
        img.src = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
        e.dataTransfer?.setDragImage(img, 0, 0);
    }

    handleDrag(e:DragEvent) {
        if (e.y !== 0 && e.x !== 0) {
            this.movableButton.style.top = `${e.y - this.dragOffsetY}px`;
            this.movableButton.style.left = `${e.x - this.dragOffsetX}px`;
        }
    }

    handleDragEnd(e:DragEvent) {
        this.movableButton.style.top = `${e.y - this.dragOffsetY}px`;
        this.movableButton.style.left = `${e.x - this.dragOffsetX }px`;
        this.dragOffsetX = 0;
        this.dragOffsetY = 0;
    }

    showMovableButtonContextMenu(e:MouseEvent) {
        console.log('--- ignore old click handler ---', e, this.movableButtonMenu);
        // if (!e.defaultPrevented) {
        //     this.movableButtonMenu.show();
        // }
    }

    render() {
        return html`
            <div style="border: solid 2px #f00; height: 100%; position: relative;">

                <!--
                <kode4-list floating style="top: 50%; left: 50%;">
                    <div>Load more</div>
                    <div @click="${this.toggleStaticLists}">Toggle Static Lists</div>
                    <div @click="${this.toggleHiddenStaticList}">Toggle Hidden Static List</div>
                </kode4-list>
                -->
                
                <div>
                    <div class="k4d-button" style="margin: 25px;">
                        Trigger Left Drawer
                        <kode4-trigger drawer="left-drawer" parent caller-params="1"></kode4-trigger>
                    </div>
    
                    <div class="k4d-button" style="margin: 25px;">
                        Trigger Bottom Drawer
                        <kode4-trigger drawer="bottom-drawer" parent caller-params="2"></kode4-trigger>
                    </div>
    
                    <div class="k4d-button" style="margin: 25px;">
                        Trigger Dialog
                        <kode4-trigger dialog="dialog-1" parent caller-params="3"></kode4-trigger>
                    </div>

                    <div class="k4d-button" style="margin: 25px;" @click="${async () => {
                        const dialog = kode4Dialogs.get('dialog-modal');
                        if (await dialog?.show()) {
                            getGrowlMessenger().alert({
                                message: 'Modal confirmed!',
                                color: 'success',
                                pos: 'bottom',
                                timeout: 2000,
                            })
                        } else {
                            getGrowlMessenger().alert({
                                message: 'Modal cancelled!',
                                color: 'danger',
                                pos: 'bottom',
                                timeout: 2000,
                            })
                        }
                    }}">
                        Trigger Modal-Dialog
                    </div>
                </div>

                <kode4-menu name="somelist" right top cover bound>
                    <div @click="${(e:MouseEvent) => {
                        console.log('[Kode4UiDemoLists] option 1 on menu', kode4Menus.get('somelist')!.getCallerParams());
                        e.preventDefault();
                    }}">
                        Option 1
                    </div>
                    <div @click="${() => {
                        const menu = kode4Menus.get('somelist')!;
                        console.log('[Kode4UiDemoLists] option 2 on menu', menu.getCallerParams());
                        menu.hide();
                    }}">
                        Option 2
                    </div>
                    <div @click="${() => {
                        console.log('[Kode4UiDemoLists] option 3 on menu', kode4Menus.get('somelist')!.getCallerParams(), ' DO NOT CLOSE MENU!');
                    }}">
                        Option 3
                    </div>
                    <div @click="${() => {
                        const menu = kode4Menus.get('somelist')!;
                        console.log('[Kode4UiDemoLists] option 4 on menu', menu.getCallerParams());
                        menu.hide();
                    }}">
                        Option 4
                    </div>
                </kode4-menu>
    
                <kode4-menu floating context-menu backdrop>
                    <div @click="${(e:MouseEvent) => {
                        console.log('Handle Menu-Item Klick');
                        e.preventDefault();
                    }}">Menu Item 1</div>
                    <div @click="${(e:MouseEvent) => {
                        console.log('Handle Menu-Item Klick');
                        e.preventDefault();
                    }}">Menu Item 2</div>
                    <div @click="${(e:MouseEvent) => {
                        console.log('Handle Menu-Item Klick');
                        e.preventDefault();
                    }}">Menu Item 3</div>
                    <div @click="${(e:MouseEvent) => {
                        console.log('Handle Menu-Item Klick');
                        e.preventDefault();
                    }}">Menu Item 4</div>
                </kode4-menu>
                
                <div class="k4d-movable-button" style="position: fixed; z-index: 2000; top: 30%; left: 35%; border: solid 2px #ccc; background-color: #fff; padding: 10px;" @click="${this.showMovableButtonContextMenu}" draggable @dragstart="${this.handleDragStart}" @dragend="${this.handleDragEnd}" @drag="${this.handleDrag}">
                    <div style="">
                        Drag me around und click me
                        <br>
                        to open a context menu
                    </div>
                    <kode4-menu class="k4d-movable-button-menu" parent top right cover>
                        <div @click="${(e:MouseEvent) => {
                            console.log('Handle Menu-Item Klick');
                            e.preventDefault();
                        }}">Menu Item 1</div>
                        <div @click="${(e:MouseEvent) => {
                            console.log('Handle Menu-Item Klick');
                            e.preventDefault();
                        }}">Menu Item 2</div>
                        <div @click="${(e:MouseEvent) => {
                            console.log('Handle Menu-Item Klick');
                            e.preventDefault();
                        }}">Menu Item 3</div>
                        <div @click="${(e:MouseEvent) => {
                            console.log('Handle Menu-Item Klick');
                            e.preventDefault();
                        }}">Menu Item 4</div>
                    </kode4-menu>
                </div>

                ${this.staticListsVisible ? html`
                    <kode4-list inline ?hidden="${this.staticHiddenListsVisible}">
                        <div @click="${(e:MouseEvent) => {
                            console.log('Handle Menu-Item Klick');
                            e.preventDefault();
                        }}">Menu Item 1</div>
                        <div @click="${(e:MouseEvent) => {
                            console.log('Handle Menu-Item Klick');
                            e.preventDefault();
                        }}">Menu Item 2</div>
                        <div @click="${(e:MouseEvent) => {
                            console.log('Handle Menu-Item Klick');
                            e.preventDefault();
                        }}">Menu Item 3</div>
                        <div @click="${(e:MouseEvent) => {
                            console.log('Handle Menu-Item Klick');
                            e.preventDefault();
                        }}">Menu Item 4</div>
                    </kode4-list>
    
                    <kode4-list floating top right>
                        <div @click="${(e:MouseEvent) => {
                            console.log('Handle Menu-Item Klick');
                            e.preventDefault();
                        }}">Menu Item 1</div>
                        <div @click="${(e:MouseEvent) => {
                            console.log('Handle Menu-Item Klick');
                            e.preventDefault();
                        }}">Menu Item 2</div>
                        <div @click="${(e:MouseEvent) => {
                            console.log('Handle Menu-Item Klick');
                            e.preventDefault();
                        }}">Menu Item 3</div>
                        <div @click="${(e:MouseEvent) => {
                            console.log('Handle Menu-Item Klick');
                            e.preventDefault();
                        }}">Menu Item 4</div>
                    </kode4-list>
    
                    <kode4-list floating bottom right>
                        <div @click="${(e:MouseEvent) => {
                            console.log('Handle Menu-Item Klick');
                            e.preventDefault();
                        }}">Menu Item 1</div>
                        <div @click="${(e:MouseEvent) => {
                            console.log('Handle Menu-Item Klick');
                            e.preventDefault();
                        }}">Menu Item 2</div>
                        <div @click="${(e:MouseEvent) => {
                            console.log('Handle Menu-Item Klick');
                            e.preventDefault();
                        }}">Menu Item 3</div>
                        <div @click="${(e:MouseEvent) => {
                            console.log('Handle Menu-Item Klick');
                            e.preventDefault();
                        }}">Menu Item 4</div>
                    </kode4-list>
    
                    <kode4-list floating bottom left>
                        <div @click="${(e:MouseEvent) => {
                            console.log('Handle Menu-Item Klick');
                            e.preventDefault();
                        }}">Menu Item 1</div>
                        <div @click="${(e:MouseEvent) => {
                            console.log('Handle Menu-Item Klick');
                            e.preventDefault();
                        }}">Menu Item 2</div>
                        <div @click="${(e:MouseEvent) => {
                            console.log('Handle Menu-Item Klick');
                            e.preventDefault();
                        }}">Menu Item 3</div>
                        <div @click="${(e:MouseEvent) => {
                            console.log('Handle Menu-Item Klick');
                            e.preventDefault();
                        }}">Menu Item 4</div>
                    </kode4-list>
                ` : undefined}
            </div>
            
            <kode4-dialog name="dialog-1" style="border: solid 5px #0ff;">
                <div style="background-color: #000; color: #fff;">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis commodi dolore dolores dolorum eligendi pariatur quisquam ratione soluta tempore. Corporis cum cumque earum fuga, iste laudantium nobis soluta totam ut.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis commodi dolore dolores dolorum eligendi pariatur quisquam ratione soluta tempore. Corporis cum cumque earum fuga, iste laudantium nobis soluta totam ut.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis commodi dolore dolores dolorum eligendi pariatur quisquam ratione soluta tempore. Corporis cum cumque earum fuga, iste laudantium nobis soluta totam ut.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis commodi dolore dolores dolorum eligendi pariatur quisquam ratione soluta tempore. Corporis cum cumque earum fuga, iste laudantium nobis soluta totam ut.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis commodi dolore dolores dolorum eligendi pariatur quisquam ratione soluta tempore. Corporis cum cumque earum fuga, iste laudantium nobis soluta totam ut.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis commodi dolore dolores dolorum eligendi pariatur quisquam ratione soluta tempore. Corporis cum cumque earum fuga, iste laudantium nobis soluta totam ut.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis commodi dolore dolores dolorum eligendi pariatur quisquam ratione soluta tempore. Corporis cum cumque earum fuga, iste laudantium nobis soluta totam ut.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis commodi dolore dolores dolorum eligendi pariatur quisquam ratione soluta tempore. Corporis cum cumque earum fuga, iste laudantium nobis soluta totam ut.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis commodi dolore dolores dolorum eligendi pariatur quisquam ratione soluta tempore. Corporis cum cumque earum fuga, iste laudantium nobis soluta totam ut.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis commodi dolore dolores dolorum eligendi pariatur quisquam ratione soluta tempore. Corporis cum cumque earum fuga, iste laudantium nobis soluta totam ut.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis commodi dolore dolores dolorum eligendi pariatur quisquam ratione soluta tempore. Corporis cum cumque earum fuga, iste laudantium nobis soluta totam ut.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis commodi dolore dolores dolorum eligendi pariatur quisquam ratione soluta tempore. Corporis cum cumque earum fuga, iste laudantium nobis soluta totam ut.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis commodi dolore dolores dolorum eligendi pariatur quisquam ratione soluta tempore. Corporis cum cumque earum fuga, iste laudantium nobis soluta totam ut.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis commodi dolore dolores dolorum eligendi pariatur quisquam ratione soluta tempore. Corporis cum cumque earum fuga, iste laudantium nobis soluta totam ut.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis commodi dolore dolores dolorum eligendi pariatur quisquam ratione soluta tempore. Corporis cum cumque earum fuga, iste laudantium nobis soluta totam ut.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis commodi dolore dolores dolorum eligendi pariatur quisquam ratione soluta tempore. Corporis cum cumque earum fuga, iste laudantium nobis soluta totam ut.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis commodi dolore dolores dolorum eligendi pariatur quisquam ratione soluta tempore. Corporis cum cumque earum fuga, iste laudantium nobis soluta totam ut.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis commodi dolore dolores dolorum eligendi pariatur quisquam ratione soluta tempore. Corporis cum cumque earum fuga, iste laudantium nobis soluta totam ut.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis commodi dolore dolores dolorum eligendi pariatur quisquam ratione soluta tempore. Corporis cum cumque earum fuga, iste laudantium nobis soluta totam ut.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, odit, perspiciatis. A amet aperiam architecto aspernatur at commodi consectetur cumque, deserunt doloribus id obcaecati praesentium quidem quod rem reprehenderit? Obcaecati.<br><br><br><br>
                </div>
            </kode4-dialog>

<!--            <kode4-dialog name="dialog-modal" modal>-->
<!--                <div style="background-color: #000; color: #fff; width: auto;">-->
<!--                    Wollen Sie das wirklich?-->
<!--                </div>-->
<!--            </kode4-dialog>-->

        `;
    }

    createRenderRoot() {
        return this;
    }
}
