// import { LitElement, html, property, customElement, PropertyValues } from "lit";
import {LitElement, html, PropertyValues} from "lit";
import {customElement, property, query, state} from "lit/decorators.js";
import {ifDefined} from "lit/directives/if-defined.js";
import {faMars} from "@fortawesome/free-solid-svg-icons/faMars";
import {faVenus} from "@fortawesome/free-solid-svg-icons/faVenus";
import Kode4Tabs from "../../kode4-tabs";
import {IListStateObserver, ListStateHandler} from "../../ListStateHandler";
import {getMorePeople, Kode4UiDemoPerson, sortArray} from "../components/PersonList";
import {cloneDeep, isEqual} from "lodash-es";

@customElement('kode4ui-demo-tables')
export class Kode4UiDemoTables extends LitElement implements IListStateObserver {

    @state()
    public lsh: ListStateHandler = new ListStateHandler({
        // observer: this,
        fields: ['firstname', 'lastname', 'email', 'id'],
    });

    onOrderChange(): void {
        console.log('onOrderChange');
        this.persons = sortArray(this.persons, <'firstname'|'lastname'|'email'|'id'>this.lsh.orderBy, this.lsh.orderByDir);
        this.requestUpdate();
    }

    @query('.kode4-tabs-selected')
    public k4tabs!: Kode4Tabs;

    @query('#listEndObserver')
    public listEndObserverTrigger!: HTMLElement;

    @property({type: Boolean, attribute: true})
    public stretch: boolean = false;

    @property({type: Boolean, attribute: true})
    public persons: Array<Kode4UiDemoPerson> = [];

    private loadMore = async () => {
        console.log('LOAD MORE');
        try {
            if (this.persons.length >= 1000) {
                console.log('List limit reached', this.persons.length);
                return;
            }

            if (this.lsh.orderBy) {
                this.persons = sortArray([
                    ...this.persons,
                    ...await getMorePeople(),
                ], <'firstname'|'lastname'|'email'|'id'>this.lsh.orderBy, this.lsh.orderByDir);
            } else {
                this.persons = [
                    ...this.persons,
                    ...await getMorePeople(),
                ];
            }

        } catch (e) {
            console.error(e);
        }
    }

    protected toggleLshOrder(field: string) {
        this.lsh.activateOrderByFieldOrToggleDir(field);
    }

    protected async firstUpdated(_changedProperties: PropertyValues) {
        const filters1:Map<string, any> = (new Map())
            .set('searchterm', '')
            .set('alter_von', undefined)
            .set('alter_bis', 78)
            .set('verzogenverstorben', false);

        const filters2:Map<string, any> = (new Map())
            .set('searchterm', '')
            .set('alter_von', undefined)
            .set('alter_bis', 78)
            .set('verzogenverstorben', false);

        const filters3:Map<string, any> = (new Map())
            .set('searchterm', '')
            .set('verzogenverstorben', false)
            .set('alter_bis', 78)
            .set('alter_von', undefined);

        const filters4:Map<string, any> = (new Map())
            .set('searchterm', 'Kassel')
            .set('alter_von', undefined)
            .set('alter_bis', 78)
            .set('verzogenverstorben', false);

        const filters5:Map<string, any> = (new Map())
            .set('searchterm', '')
            .set('alter_von', undefined)
            .set('alter_bis', 78)
            .set('verzogenverstorben', true);

        const filters6:Map<string, any> = (new Map())
            .set('searchterm', '')
            .set('alter_von', null)
            .set('alter_bis', 78)
            .set('verzogenverstorben', false);

        const filters7:Map<string, any> = cloneDeep(filters1);

        console.log('[compare] 1 und 1', isEqual(filters1, filters2));
        console.log('[compare] 1 und 3', isEqual(filters1, filters3));
        console.log('[compare] 1 und 4', isEqual(filters1, filters4));
        console.log('[compare] 1 und 5', isEqual(filters1, filters5));
        console.log('[compare] 1 und 6', isEqual(filters1, filters6));
        console.log('[compare] 1 und 7', isEqual(filters1, filters7));
        console.log('[compare] 2 und 7', isEqual(filters2, filters7));

        console.log('[compare]', filters1, filters5, filters7);

        super.firstUpdated(_changedProperties);
    }

    render() {
        return html`
            <kode4-table @kode4-table-more="${this.loadMore}" .more="${this.loadMore}">
                <kode4-tableheader primary style="width: 30px;" slot="header"></kode4-tableheader>
                <kode4-tableheader
                        primary
                        slot="header" 
                        ?sortable="${this.lsh.isSortable('lastname')}"  
                        order="${ifDefined(this.lsh.getOrderByDirForField('lastname'))}" 
                        @kode4-sort="${() => this.toggleLshOrder('lastname')}">
                    Last name
                </kode4-tableheader>
                <kode4-tableheader
                        primary
                        slot="header"
                        ?sortable="${this.lsh.isSortable('firstname')}"
                        order="${ifDefined(this.lsh.getOrderByDirForField('firstname'))}"  
                        @kode4-sort="${() => this.toggleLshOrder('firstname')}">
                    First name
                </kode4-tableheader>
                <kode4-tableheader
                    primary
                        slot="header"
                        ?sortable="${this.lsh.isSortable('email')}"
                        order="${ifDefined(this.lsh.getOrderByDirForField('email'))}"
                        @kode4-sort="${() => this.toggleLshOrder('email')}">
                    E-Mail
                </kode4-tableheader>
                <kode4-tableheader
                        primary
                        slot="header" 
                        ?sortable="${this.lsh.isSortable('id')}"
                        order="${ifDefined(this.lsh.getOrderByDirForField('id'))}"  
                        @kode4-sort="${() => this.toggleLshOrder('id')}">
                    ID
                </kode4-tableheader>

                ${this.persons.map((p: Kode4UiDemoPerson) => html`
                    <kode4-tablerow>
                        <div>
                            ${p.gender === 'female' ? html`
                                <kode4-fa-icon .icon="${faVenus}"></kode4-fa-icon>
                            ` : undefined}
                            ${p.gender === 'male' ? html`
                                <kode4-fa-icon .icon="${faMars}"></kode4-fa-icon>
                            ` : undefined}
                        </div>
                        <div>
                            ${p.lastname}
                        </div>
                        <div>
                            ${p.firstname}
                        </div>
                        <div>
                            ${p.email}
                        </div>
                        <div>
                            <small>
                                ${p.id}
                            </small>
                        </div>
                    </kode4-tablerow>
                `)}

            </kode4-table>
        `;
    }

    createRenderRoot() {
        return this;
    }
}
