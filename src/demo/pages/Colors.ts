// import { LitElement, html, property, customElement } from "lit";
import { LitElement, html } from "lit";
import { customElement, property } from "lit/decorators.js";
import '../components/ColorDemo';

@customElement('kode4ui-demo-colors')
export class Kode4UiDemoColors extends LitElement {

    @property({ type: Boolean })
    public showLabels:boolean = false;

    private colors:Array<string> = [
        'primary',
        'secondary',
        'success',
        'danger',
        'warning',
        'info',
    ]

    constructor() {
        super();
    }

    createRenderRoot() {
        return this;
    }

    render() {
        return html`
            <h1>Colors</h1>
            <br><br>
            <kode4-alert primary>Alert!</kode4-alert>
            <kode4-alert secondary>Alert!</kode4-alert>
            <kode4-alert success>Alert!</kode4-alert>
            <kode4-alert danger>Alert!</kode4-alert>
            <kode4-alert warning>Alert!</kode4-alert>
            <kode4-alert info>Alert!</kode4-alert>
            <br><br>
            <kode4ui-demo-colordemo primary></kode4ui-demo-colordemo>
            <kode4ui-demo-colordemo secondary></kode4ui-demo-colordemo>
            <kode4ui-demo-colordemo success></kode4ui-demo-colordemo>
            <kode4ui-demo-colordemo danger></kode4ui-demo-colordemo>
            <kode4ui-demo-colordemo warning></kode4ui-demo-colordemo>
            <kode4ui-demo-colordemo info></kode4ui-demo-colordemo>
            <br><br>
            <kode4ui-demo-colordemo theme="bg" primary></kode4ui-demo-colordemo>
            <kode4ui-demo-colordemo theme="bg" secondary></kode4ui-demo-colordemo>
            <kode4ui-demo-colordemo theme="bg" success></kode4ui-demo-colordemo>
            <kode4ui-demo-colordemo theme="bg" danger></kode4ui-demo-colordemo>
            <kode4ui-demo-colordemo theme="bg" warning></kode4ui-demo-colordemo>
            <kode4ui-demo-colordemo theme="bg" info></kode4ui-demo-colordemo>
            <br><br>
            <kode4ui-demo-colordemo text></kode4ui-demo-colordemo>
            <kode4ui-demo-colordemo text-primary></kode4ui-demo-colordemo>
            <kode4ui-demo-colordemo text-secondary></kode4ui-demo-colordemo>
            <kode4ui-demo-colordemo text-success></kode4ui-demo-colordemo>
            <kode4ui-demo-colordemo text-danger></kode4ui-demo-colordemo>
            <kode4ui-demo-colordemo text-warning></kode4ui-demo-colordemo>
            <kode4ui-demo-colordemo text-info></kode4ui-demo-colordemo>
            
            <kode4ui-demo-colordemo bg></kode4ui-demo-colordemo>
            <kode4ui-demo-colordemo bg-primary></kode4ui-demo-colordemo>
            <kode4ui-demo-colordemo bg-secondary></kode4ui-demo-colordemo>
            <kode4ui-demo-colordemo bg-success></kode4ui-demo-colordemo>
            <kode4ui-demo-colordemo bg-danger></kode4ui-demo-colordemo>
            <kode4ui-demo-colordemo bg-warning></kode4ui-demo-colordemo>
            <kode4ui-demo-colordemo bg-info></kode4ui-demo-colordemo>
            
            <br><br>
            <h1>Colors</h1>
            <br><br>
            <div class="kode4-text" style="margin-bottom: 10px;">Default</div>
            ${this.colors.map((el:string) => html`
                <div class="kode4-text-${el}" style="margin-bottom: 10px;">Color ${el}</div>
            `)}
            <br><br>
            <div class="kode4-text-hover" style="margin-bottom: 10px;">Default</div>
            ${this.colors.map((el:string) => html`
                <div class="kode4-text-hover kode4-text-${el}" style="margin-bottom: 10px;">Color ${el}</div>
            `)}
            <br><br>
            <hr>
            <br><br>



            <div class="kode4-bg" style="margin-bottom: 10px;">Background Default</div>
            ${this.colors.map((el:string) => html`
                <div class="kode4-bg-${el}" style="margin-bottom: 10px;">Background ${el}</div>
            `)}
            <br><br>
            <div class="kode4-bg kode4-bg-hover" style="margin-bottom: 10px;">Background Default</div>
            ${this.colors.map((el:string) => html`
                <div class="kode4-bg-hover kode4-bg-${el}" style="margin-bottom: 10px;">Background ${el}</div>
            `)}
            <br><br>
            <hr>
            <br><br>



            <div class="kode4-alert" style="margin-bottom: 10px;">Background Default</div>
            ${this.colors.map((el:string) => html`
                <div class="kode4-alert kode4-bg-${el}" style="margin-bottom: 10px;">Background ${el}</div>
            `)}
            <br><br>
            <div class="kode4-alert kode4-alert-hover" style="margin-bottom: 10px;">Background Default</div>
            ${this.colors.map((el:string) => html`
                <div class="kode4-alert kode4-alert-hover kode4-bg-${el}" style="margin-bottom: 10px;">Background ${el}</div>
            `)}
        `;
    }

}
