// import { LitElement, html, property, customElement } from "lit";
import { LitElement, html } from "lit";
import { customElement, property } from "lit/decorators.js";

@customElement('kode4ui-demo-progress')
export class Kode4UiDemoProgress extends LitElement {

    @property({ type: Boolean })
    public showLabels:boolean = false;

    @property({ type: String })
    public color:string = 'primary';

    @property({ type: Number })
    public progress:number = 70;

    constructor() {
        super();
    }

    createRenderRoot() {
        return this;
    }

    render() {
        return html`
            <select value="${this.color}" @change="${(e:MouseEvent) => this.color = (<HTMLSelectElement>e.target).value}">
                <option>No color</option>
                <option value="primary" selected>primary</option>
                <option value="secondary">secondary</option>
                <option value="success">success</option>
                <option value="danger">danger</option>
                <option value="warning">warning</option>
                <option value="info">info</option>
            </select>
            <br><br>
            <input type="range" min="0" max="100" value="${this.progress}" @input="${(e:MouseEvent) => this.progress = Number((<HTMLInputElement>e.target).value)}">
            (${this.progress} %)

            <div style="margin-top: 10px;">
                <kode4-progress indeterminate color="${this.color}"></kode4-progress>
            </div>
            <div style="margin-top: 10px;">
                <kode4-progress indeterminate color="${this.color}" style="height: 20px;"></kode4-progress>
            </div>
            <div style="margin-top: 10px; height: 25px;">
                <kode4-progress indeterminate color="${this.color}" style="height: 100%;"></kode4-progress>
            </div>
            <div style="margin-top: 10px;">
                <kode4-progress progress="${this.progress}" color="${this.color}"></kode4-progress>
            </div>
            <div style="margin-top: 10px;">
                <kode4-progress indeterminate color="${this.color}" inline style="width: 30%; border: solid 1px #000;"></kode4-progress>
                <kode4-progress progress="${this.progress}" color="${this.color}" inline style="width: 30%; margin-left: 25px; border: solid 1px #000;"></kode4-progress>
            </div>
            <div style="margin-top: 10px;">
                <kode4-progress type="icon" indeterminate color="${this.color}" inline></kode4-progress>
                <kode4-progress type="icon" progress="${this.progress}" color="${this.color}" inline></kode4-progress>
            </div>
            <div style="margin-top: 10px; font-size: 48px;">
                <kode4-progress type="icon" indeterminate color="${this.color}" inline></kode4-progress>
                <kode4-progress type="icon" progress="${this.progress}" color="${this.color}" inline></kode4-progress>
            </div>
            <div style="margin-top: 10px; font-size: 80px;">
                <kode4-progress type="icon" indeterminate color="${this.color}" inline></kode4-progress>
                <kode4-progress type="icon" progress="${this.progress}" color="${this.color}" inline></kode4-progress>
            </div>
            <div style="margin-top: 10px;">
                <kode4-progress type="text" indeterminate color="${this.color}" inline></kode4-progress>
                <kode4-progress type="text" progress="${this.progress}" color="${this.color}" inline></kode4-progress>
            </div>
        `;
    }

}
