// import { LitElement, html, property, customElement } from "lit";
import {LitElement, html} from "lit";
import {customElement, property} from "lit/decorators.js";

import {faPowerOff} from "@fortawesome/free-solid-svg-icons/faPowerOff";
import {faBars} from "@fortawesome/free-solid-svg-icons/faBars";

@customElement('kode4ui-app-layout')
export class AppLayout extends LitElement {

    @property()
    private isAuthenticated: boolean = window.localStorage.hasOwnProperty('KODE4_UI_AUTHENTICATED') ? window.localStorage.getItem('KODE4_UI_AUTHENTICATED') === 'true' : false;

    @property()
    private menuDrawerOpen: boolean = window.localStorage.hasOwnProperty('KODE4_UI_MENUDRAWER_OPEN') ? window.localStorage.getItem('KODE4_UI_MENUDRAWER_OPEN') === 'true' : false;

    @property()
    private busy: boolean = false;

    constructor() {
        super();
    }

    protected signin(e: Event) {
        e.preventDefault();
        this.isAuthenticated = true;
        window.localStorage.setItem('KODE4_UI_AUTHENTICATED', String(this.isAuthenticated));
    }

    protected signout(e: Event) {
        e.preventDefault();
        if (confirm('Do you really want to sign out?')) {
            this.isAuthenticated = false;
            window.localStorage.setItem('KODE4_UI_AUTHENTICATED', String(this.isAuthenticated));
        }
    }

    protected setMenuDrawerState(open?: boolean) {
        if (open === undefined) {
            this.menuDrawerOpen = !this.menuDrawerOpen;
        } else {
            this.menuDrawerOpen = open;
        }
        window.localStorage.setItem('KODE4_UI_MENUDRAWER_OPEN', String(this.menuDrawerOpen));
    }

    protected setBusy(busy?: boolean) {
        if (busy === undefined) {
            this.busy = !this.busy;
        } else {
            this.busy = busy;
        }
    }

    render() {
        return html`
            <header class="kode4-menubar" style="position: relative;">
                <kode4-button
                        flat
                        icon
                        @click="${() => {
                            this.setMenuDrawerState();
                        }}"
                >
                    <kode4-fa-icon .icon="${faBars}"></kode4-fa-icon>
                </kode4-button>
                <img src="../assets/dummy-agenturlogo.png" alt="Logo"
                     style="height: 40px; margin-right: 10px;">
                <div>
                    KODE4 UI Demo
                </div>
                <div class="kode4-menubar-spacer"></div>
                <span
                        flat
                        class="kode4-text kode4-text-danger"
                        @click="${this.setBusy()}"
                    >
                    Toggle Busy
                </span>
                ${this.isAuthenticated ? html`
                    <kode4-button
                            flat
                            @click="${this.signout}"
                    >
                        <kode4-fa-icon .icon="${faPowerOff}"></kode4-fa-icon>
                    </kode4-button>
                ` : html`
                    <kode4-button
                            flat
                            @click="${this.signin}"
                    >
                        Sign in
                    </kode4-button>
                `}
            </header>

            <div class="main-content">
                ${this.busy ? html`
                    <div style="position: absolute; z-index: 2000; left: 0; right: 0; top: 0; height: 3px;">
                        <kode4-progress color="primary" indeterminate
                                        style="height: 3px; opacity: 0.5"></kode4-progress>
                    </div>
                ` : ''}
                <nav class="side-menu side-menu-left l ${this.menuDrawerOpen ? 'side-menu-open' : ''}">
                    <div style="border-bottom: solid 1px #eee; padding: 20px; margin-bottom: 20px;">
                        <div style="margin-left: auto; margin-right: auto;">
                            ${this.isAuthenticated ? html`
                                <div class="kode4-image">
                                    <img src="../assets/dummy-agenturlogo.png">
                                </div>
                                <strong>Username</strong>
                            ` : html`
                                <div>Please sign in</div>
                            `}
                        </div>
                    </div>
                    <div class="side-menu-body">
                        <nav class="kode4-menu">
                            <a href="/" class="kode4-menu-item">Willkommen</a>
                            <a href="/" class="kode4-menu-item">Firmendaten ändern</a>
                            <a href="/" class="kode4-menu-item">Infos</a>
                            <a href="/" class="kode4-menu-item">Neue Einreichung
                                anlegen</a>
                            <a href="/" class="kode4-menu-item">Einreichungen in
                                Bearbeitung</a>
                            <a href="/" class="kode4-menu-item">Abgegebene
                                Einreichungen</a>
                        </nav>
                        <nav class="kode4-menu">
                            <a href="/" class="kode4-menu-item"
                               style="font-weight: bold;">Kategorien</a>
                            <a href="/admin/kategorien/new" class="kode4-sub-menu-item">Neue Kategorie
                                anlegen</a>
                            <div style="height: 20px;"></div>

                            <a href="/" class="kode4-menu-item" style="font-weight: bold;">Medienfelder</a>
                            <a href="/" class="kode4-sub-menu-item">Neues Medienfeld
                                anlegen</a>
                            <div style="height: 20px;"></div>

                            <a href="/" class="kode4-menu-item" style="font-weight: bold;">Agenturen</a>
                            <a href="/" class="kode4-sub-menu-item">Neue Agentur
                                anlegen</a>
                            <div style="height: 20px;"></div>

                            <a href="/" class="kode4-menu-item" style="font-weight: bold;">Einreichungen</a>
                            <a href="/" class="kode4-sub-menu-item">Neue
                                Einreichungen anlegen</a>
                            <div style="height: 20px;"></div>

                            <a href="/" class="kode4-menu-item"
                               style="font-weight: bold;">Benutzer</a>
                            <a href="/" class="kode4-sub-menu-item">Neuen Benutzer
                                anlegen</a>
                            <div style="height: 20px;"></div>

                            <a href="/" class="kode4-menu-item"
                               style="font-weight: bold;">Agentur-News</a>
                            <a href="/" class="kode4-sub-menu-item">Agentur-News
                                anlegen</a>
                            <div style="height: 20px;"></div>

                            <a href="/" class="kode4-menu-item"
                               style="font-weight: bold;">Preis-Stufen</a>
                            <a href="/" class="kode4-sub-menu-item">Preis-Stufe
                                anlegen</a>
                            <div style="height: 20px;"></div>

                            <a href="/" class="kode4-menu-item" style="font-weight: bold;">Druckdaten
                                Download</a>
                            <div style="height: 20px;"></div>
                        </nav>
                    </div>
                    <div>
                        <nav class="kode4-menu"
                             style="margin-top: 12px; border-top: solid 1px #eee; padding-top: 12px;">
                            <a href="" @click="${this.signout}" class="kode4-menu-item">Ausloggen</a>
                        </nav>
                    </div>
                </nav>

                <div class="main">
                    ROUTER OUTLET &lt;main&gt;&lt;/main&gt; GOES GERE
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad commodi debitis dolores eos exercitationem facere fugit ipsa ipsam iure maiores mollitia nisi quasi qui quibusdam rem, saepe sapiente, sequi vitae.
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aut, hic incidunt itaque quia similique tempora. Asperiores delectus esse expedita facilis fuga ipsa ipsam, omnis placeat, praesentium repellat repellendus veritatis?
                </div>

            </div>

            <kode4-growl></kode4-growl>
        `;
    }

    createRenderRoot() {
        return this;
    }

}
