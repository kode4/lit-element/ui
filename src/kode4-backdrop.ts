import {LitElement, html, css} from "lit";
import {customElement, property} from "lit/decorators.js";

@customElement('kode4-backdrop')
export default class Kode4Backdrop extends LitElement {

    private toggleCssClass(show:boolean, cssClass:string) {
        show ? this.classList.add(cssClass) : this.classList.remove(cssClass);
    }

    @property({ type: Boolean, attribute: 'visible' })
    public set visible(value:boolean) {
        this.toggleCssClass(value, 'kode4-backdrop-visible');
    }

    public get visible():boolean {
        return this.classList.contains('kode4-backdrop-visible');
    }

    @property({ type: Boolean, attribute: 'transparent' })
    public set transparent(value:boolean) {
        this.toggleCssClass(value, 'kode4-backdrop-transparent');
    }

    public get transparent():boolean {
        return this.classList.contains('kode4-backdrop-transparent');
    }

    public render() {
        return html`
            <slot></slot>
        `;
    }

    static styles = [
        css`
            :host {
                position: fixed;
                top: 0;
                bottom: 0;
                left: 0;
                right: 0;
                z-index: 10000;
                overflow: hidden;
                background-color: rgba(0, 0, 0, 0.25);
                transition: background 0.25s ease-out;
            }
            
            :host(.kode4-backdrop-transparent) {
                background-color: transparent;
            }

            :host(:not(.kode4-backdrop-visible)) {
                bottom: 100%;
                background-color: transparent;
                transition: all 0s linear 0.25s, background 0.25s ease-out;
            }
        `
    ]

}
