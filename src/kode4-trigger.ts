import {LitElement, html} from "lit";
import {customElement, property} from "lit/decorators.js";
import './kode4-backdrop';
import './kode4-list';
import {kode4Menus} from "./kode4-menu";
import {kode4Dialogs} from "./kode4-dialog";
import {kode4Drawers} from "./kode4-drawer";

@customElement('kode4-trigger')
export default class Kode4Trigger extends LitElement {

    @property({ type: String, attribute: true })
    public menu?:string;

    @property({ type: String, attribute: true })
    public drawer?:string;

    @property({ type: String, attribute: true })
    public dialog?:string;

    @property({ type: String, attribute: true })
    public mode:string = 'toggle';

    @property({ type: String, attribute: 'caller-params' })
    public callerParams?:any;

    // @state()
    @property({ type: Boolean, attribute: 'parent' })
    private boundToParent:boolean = false;

    // @property({ type: Boolean, attribute: 'parent' })
    // public set parent(value:boolean) {
    //     this.boundToParent = value;
    // }

    private handleParentClick = (e:MouseEvent) => {
        if (this.menu && kode4Menus.has(this.menu)) {
            e.preventDefault();
            const menu = kode4Menus.get(this.menu)!;
            menu.show({
                alignToElement: this.parentElement!,
                callerParams: this.callerParams,
            });

        } else if (this.drawer && kode4Drawers.has(this.drawer)) {
            e.preventDefault();
            const drawer = kode4Drawers.get(this.drawer)!;
            drawer.show();
            // drawer.open = true;

        } else if (this.dialog && kode4Dialogs.has(this.dialog)) {
            e.preventDefault();
            const dialog = kode4Dialogs.get(this.dialog)!;
            dialog.show();

        } else {
            console.log('[Kode4Trigger] unable to connect');
        }
    }

    connectedCallback() {
        if (this.boundToParent) {
            this.parentElement!.addEventListener('click', this.handleParentClick);
        }

        super.connectedCallback();
    }

    disconnectedCallback() {
        if (this.boundToParent) {
            this.parentElement!.removeEventListener('click', this.handleParentClick);
        }
        super.disconnectedCallback();
    }

    public render() {
        return html`
        `;
    }

}
