import debounce from  'lodash-es/debounce';
import {cloneDeep} from "lodash-es";
import {IApplicationPersistenceService} from "@kode4/core/persistence/ApplicationPersistenceService";

export interface IListStateObserver {
    onOrderChange?():void;
    onFilterChange?():void;
    onPaginationChange?():void;
    onStateChange?():void;
    onRestore?():void;
    onReset?():void;
    onRefresh?(forceReload:boolean):void;
}

export class ListLimitReachedError extends Error {}

export interface IListStateObserverRequestParameters {
    orderBy?:string;
    page:number;
    pageSize:number;
    filters:Map<string, any>;
    state:Map<string, any>;
    restored:boolean;
    hash?:string;
}

export class ListStateHandler {

    private observers:Array<IListStateObserver> = [];

    private restored:boolean = false;

    private _orderByFields: Array<string> = [];

    private _orderBy?: string;

    private _orderByDir: 'asc' | 'desc' = 'asc';

    protected filters:Map<string, any> = new Map();

    public page:number = 0;

    public lshState:'idle'|'initializing'|'busy' = 'initializing';

    public loadPage(value:string|number) {
        if (this.lshState !== 'idle') {
            return;
        }
        switch (value) {
            case 'next':
                if (this.size === -1) {
                    this.page = 0;
                } else if (this.total !== -1 && this.size >= this.total) {
                    return;
                } else {
                    const expectedPage:number = Math.max(Math.floor(this.size / this.pageSize) - 1, 0);
                    if (expectedPage < this.page) {
                        const expectedSize = (this.page + 1) * this.pageSize;
                        if (this.total !== -1 && expectedSize > this.total) {
                            this.resetList();
                            return;
                        }
                        this.size = expectedSize;
                        this.refreshList();
                        return;
                    } else {
                        this.page += 1;
                    }
                }
                this.firePaginationChanged()
                break;

            case 'prev':
            case 'previous':
                if (this.page > 0) {
                    this.page -= 1;
                    this.firePaginationChanged()
                }
                break;

            default :
                this.page = Number(value);
                this.firePaginationChanged()
        }
    }

    public pageSize:number = 0;

    public size:number = -1; // -1 means unknown

    public total:number = -1; // -1 means unknown

    public cacheable:boolean = true;

    public cacheLease:number = 60 * 10; // in seconds. default is 10 minutes

    private lastRefresh?:number; // last refresh of the list data

    private _hash:string = '';

    public get hash() {
        return this.buildHash();
        if (!this._hash) {
            this.updateHash()
        }
        return this._hash;
    }

    protected state:Map<string, any> = new Map();

    private storage?: IApplicationPersistenceService;

    private storageKey?: string;

    private _initialValues?:{
        fields?: Array<string>,
        field?: string,
        dir?: 'asc'|'desc',
        filters?:Map<string, any>
        page?:number,
        pageSize?:number,
        cacheable?:boolean,
        cacheLease?:number,
        observer?:IListStateObserver,
        state?:Map<string, any>
        storage?:IApplicationPersistenceService,
        storageKey?:string,
    };


    constructor(params:{
        fields?: Array<string>,
        field?: string,
        dir?: 'asc'|'desc',
        filters?:Map<string, any>
        page?:number,
        pageSize?:number,
        cacheable?:boolean,
        cacheLease?:number,
        observer?:IListStateObserver,
        state?:Map<string, any>
        storage?:IApplicationPersistenceService,
        storageKey?:string,
    } = {}) {
        if (params.filters) {
            params.filters = this.sanitizeFilterAndStateValues(params.filters);
        }
        if (params.state) {
            params.state = this.sanitizeFilterAndStateValues(params.state);
        }
        this._initialValues = cloneDeep(params);
        this.applyValues(params);
        if (params.observer) {
            this.observe(params.observer);
        }
        this.storage = params.storage;
        this.storageKey = params.storageKey;

        if (!this.canRestore()) {
            this.lshState = 'idle';
        } else {
            this.restore();
        }
    }

    // =====[ Internal State ]==========================================================================================
    public reset() {
        this.applyValues(this._initialValues);
    }

    public resetList() {
        this.fireReset();
    }

    public refreshList(forceReload:boolean = true) {
        this.fireRefresh(forceReload);
    }

    public store() {
        if (this.storage && this.storageKey) {
            this.storage.setItem(this.storageKey, this.toString());
        }
    }

    public restore():boolean {
        if (this.canRestore()) {
            this.fromString(this.storage!.getItem(this.storageKey!)!);
            this.fireRestore();
            return true;
        }
        return false;
    }

    private canRestore() {
        return this.storage && this.storageKey && this.storage.hasItem(this.storageKey);
    }

    private applyValues(params:{
        fields?: Array<string>,
        field?: string,
        dir?: 'asc'|'desc',
        filters?:Map<string, any>
        page?:number,
        pageSize?:number,
        cacheable?:boolean,
        cacheLease?:number,
        state?:Map<string, any>
    } = {}) {
        if (params.fields) {
            this.orderByFields = params.fields;
        }
        if (params.field) {
            this.orderBy = params.field;
        }
        if (params.dir) {
            this.orderByDir = params.dir;
        }
        if (params.filters) {
            this.filters = this.sanitizeFilterAndStateValues(cloneDeep(params.filters));
        }
        if (params.page !== undefined) {
            this.page = params.page;
        }
        if (params.pageSize !== undefined) {
            this.pageSize = params.pageSize;
        }
        if (params.cacheable !== undefined) {
            this.cacheable = params.cacheable;
        }
        if (params.cacheLease !== undefined) {
            this.cacheLease = params.cacheLease;
        }
        if (params.state) {
            this.state = this.sanitizeFilterAndStateValues(cloneDeep(params.state));
        }
    }

    private toString() {
        const serialize = {
            _orderByFields: this._orderByFields,
            _orderByField: this._orderBy,
            _orderByDir: this._orderByDir,

            filters: Array.from(this.filters.entries()),
            page: this.page,
            pageSize: this.pageSize,
            size: this.size,
            total: this.total,

            cacheable: this.cacheable,
            cacheLease: this.cacheLease,
            lastRefresh: this.lastRefresh,
            _hash: this._hash,

            state: Array.from(this.state.entries())
        };
        return JSON.stringify(serialize);
    }

    /**
     * Please re-register observers
     *
     * @param serialized
     */
    private fromString(serialized:string) {
        const unserialized = JSON.parse(serialized);

        if (unserialized._fields) {
            this._orderByFields = unserialized._fields;
        }
        if (unserialized._field) {
            this._orderBy = unserialized._field;
        }
        if (unserialized._dir) {
            this._orderByDir = unserialized._dir;
        }
        if (unserialized.filters) {
            this.filters = new Map(unserialized.filters);
        } else {
            this.filters = new Map();
        }
        if (unserialized.page) {
            this.page = unserialized.page;
        }
        if (unserialized.page) {
            this.page = unserialized.page;
        }
        if (unserialized.pageSize) {
            this.pageSize = unserialized.pageSize;
        }
        if (unserialized.size) {
            this.size = unserialized.size;
        }
        if (unserialized.total) {
            this.total = unserialized.total;
        }
        if (unserialized.cacheable) {
            this.cacheable = unserialized.cacheable;
        }
        if (unserialized.cacheLease) {
            this.cacheLease = unserialized.cacheLease;
        }
        if (unserialized.lastRefresh) {
            this.lastRefresh = unserialized.lastRefresh;
        }
        if (unserialized._hash) {
            this._hash = unserialized._hash;
        }
        if (unserialized.state) {
            this.state = new Map(unserialized.state);
        } else {
            this.state = new Map();
        }

        this.restored = true;
    }

    private buildHash():string {
        return JSON.stringify({
            orderField: this._orderBy,
            orderDir: this._orderByDir,
            filter: Array.from(this.filters),
            size: this.size,
            page: this.page,
        });
    }

    private updateHash() {
        this._hash = this.buildHash();
    }

    // =====[ Pagination ]==============================================================================================
    public clearPagination() {
        this.page = 0;
        this.size = -1;
        this.total = -1;
    }

    // =====[ Order By ]================================================================================================
    public get orderByFields(): Array<string> {
        return this._orderByFields;
    }

    public set orderByFields(fields: Array<string>) {
        this._orderByFields = fields;
    }

    public addOrderByField(field: string) {
        this.orderByFields = [
            ...this.orderByFields,
            field
        ];
    }

    public get orderBy(): string | undefined {
        return this._orderBy;
    }

    public set orderBy(field: string | undefined) {
        if (!field) {
            this._orderBy = field;
            this.clearPagination();
            this.debouncedFireOrderChanged();
            return;
        }
        if (this.orderByFields.includes(field)) {
            this._orderBy = field;
            this.clearPagination();
            this.debouncedFireOrderChanged();
        }
    }

    public get orderByDir(): 'asc' | 'desc' {
        return this._orderByDir;
    }

    public set orderByDir(dir: 'asc' | 'desc' | undefined) {
        if (dir === undefined) {
            this._orderByDir = this._orderByDir === 'asc' ? 'desc' : 'asc';
            this.debouncedFireOrderChanged();
        } else {
            this._orderByDir = dir;
            this.debouncedFireOrderChanged();
        }
    }

    public isSortable(field: string): boolean {
        return this._orderByFields.includes(field);
    }

    public getOrderByDirForField(field: string): 'asc' | 'desc' | undefined {
        return this._orderBy === field ? this.orderByDir : undefined;
    }

    public activateOrderByFieldOrToggleDir(field: string) {
        this.clearPagination();
        if (this._orderBy === field) {
            this.orderByDir = undefined;
        } else {
            this.orderBy = field;
            this.orderByDir = 'asc';
        }
    }

    // =====[ Filters ]=================================================================================================
    public setFilter(key:string, value:any) {
        if (this.filters.get(key) !== value) {
            this.filters.set(key, this.sanitizeFilterAndStateValue(value));
            this.clearPagination();
            this.debouncedFireFilterChanged();
        }
    }

    public getFilter(key:string) {
        return this.filters.get(key);
    }

    public hasFilter(key:string) {
        return this.filters.has(key);
    }

    public hasFilters() {
        let hasFilters:boolean = false;
        for (const [_key, value] of this.filters) {
            if (value) {
                hasFilters = true;
            }
        }
        return hasFilters;
    }

    public deleteFilter(key:string) {
        this.filters.delete(key);
    }

    public clearFilters(keys?:Array<string>) {
        if (keys) {
            keys.forEach((key) => {
                this.deleteFilter(key);
            });
        }
        this.filters.clear();
        this.clearPagination();
        this.debouncedFireFilterChanged();
    }

    private nullishValues = [null, undefined];

    public hasActiveFilters() {
        if (!this.filters || !this._initialValues?.filters) {
            return false;
        }

        if (this._initialValues.filters.size !== this.filters.size) {
            return true;
        }
        let filterChanged = false;
        for (const [filter, value] of this.filters.entries()) {
            const initialValue = this._initialValues.filters.get(filter);
            if (this.nullishValues.includes(value) && !this.nullishValues.includes(initialValue)) {
                filterChanged = true
            } else if (!this.nullishValues.includes(value) && value !== initialValue) {
                filterChanged = true;
            }
        }
        return filterChanged;
    }

    public resetFilters() {
        if (this._initialValues?.filters && this.hasActiveFilters()) {
            this.filters = this.sanitizeFilterAndStateValues(cloneDeep(this._initialValues.filters));
            this.clearPagination();
            this.debouncedFireFilterChanged();
        }
    }

    // =====[ State ]===================================================================================================
    public setState(key:string, value:any) {
        if (this.state.get(key) !== value) {
            this.state.set(key, this.sanitizeFilterAndStateValue(value));
            this.debouncedFireStateChanged();
        }
    }

    public getState(key:string) {
        return this.state.get(key);
    }

    public hasState(key:string) {
        return this.state.has(key);
    }

    public deleteState(key:string) {
        this.state.delete(key)
        this.debouncedFireStateChanged();
    }

    public clearState() {
        this.state.clear()
        this.debouncedFireStateChanged();
    }

    // =====[ Events ]==================================================================================================
    public observe(observer:IListStateObserver) {
        if (!this.observers.includes(observer)) {
            this.observers.push(observer);
            if (observer.onOrderChange) {
                observer.onOrderChange();
            }
            if (observer.onFilterChange) {
                observer.onFilterChange();
            }
            if (observer.onStateChange) {
                observer.onStateChange();
            }
            if (observer.onPaginationChange) {
                observer.onPaginationChange();
            }
        }
    }

    public unobserve(observer?:IListStateObserver) {
        if (!observer) {
            this.observers = [];
        }  else {
            this.observers = [
                ...this.observers.filter((el:IListStateObserver) => el !== observer)
            ];
        }
    }

    private fireOrderChanged = () => {
        this.store();
        this.observers.forEach((observer:IListStateObserver) => {
            if (observer.onOrderChange) {
                observer.onOrderChange();
            }
        });
    }
    private debouncedFireOrderChanged = debounce(this.fireOrderChanged, 50);

    private fireFilterChanged = () => {
        this.store();
        this.observers.forEach((observer:IListStateObserver) => {
            if (observer.onFilterChange) {
                observer.onFilterChange();
            }
        });
    }
    private debouncedFireFilterChanged = debounce(this.fireFilterChanged, 50);

    private firePaginationChanged = () => {
        this.store();
        this.observers.forEach((observer:IListStateObserver) => {
            if (observer.onPaginationChange) {
                observer.onPaginationChange();
            }
        });
    }
    // private debouncedFirePaginationChanged = debounce(this.firePaginationChanged, 50);

    private fireStateChanged = () => {
        this.store();
        this.observers.forEach((observer:IListStateObserver) => {
            if (observer.onStateChange) {
                observer.onStateChange();
            }
        });
    }
    private debouncedFireStateChanged = debounce(this.fireStateChanged, 50);

    private fireRestore = () => {
        this.observers.forEach((observer:IListStateObserver) => {
            if (observer.onRestore) {
                observer.onRestore();
            }
        });
    }

    private fireReset = () => {
        this.reset();
        this.store();
        this.observers.forEach((observer:IListStateObserver) => {
            if (observer.onReset) {
                observer.onReset();
            }
        });
    }

    private fireRefresh = (forceReload:boolean = false) => {
        this.lshState = 'initializing';
        this.restored = true;
        this.observers.forEach((observer:IListStateObserver) => {
            if (observer.onRefresh) {
                observer.onRefresh(forceReload);
            }
        });
    }

    // =====[ Helpers ]=================================================================================================
    protected sanitizeFilterAndStateValues(data:Map<string, any>) {
        for (const [key, value] of data) {
            data.set(key, this.sanitizeFilterAndStateValue(value));
        }
        return data;
    }

    protected sanitizeFilterAndStateValue(value:any) {
        if (value === null) {
            return undefined;
        }
        return value;
    }

    // public reloadRequired() {
    //     if (!this.cacheable) {
    //         return true;
    //
    //     } else if (this.buildHash() !== this._hash) {
    //         return true;
    //
    //     } else if (this.lastRefresh === undefined) {
    //         return true;
    //
    //     } else if (this.lastRefresh + this.cacheLease < (Date.now() / 1000 | 0)) {
    //         return true;
    //
    //     }
    //
    //     return false;
    // }

    public getRequestParameters():IListStateObserverRequestParameters {
        if (!this.restored && this.total !== -1 && this.size >= this.total) {
            throw new ListLimitReachedError(`List limit reached: ${this.total}`);
        }

        // fix broken size
        if (this.size === -1 || this.size !== this.pageSize * (this.page + 1)) {
            this.size = this.pageSize * (this.page + 1)
        }

        const params:IListStateObserverRequestParameters = {
            page: this.restored ? 0 : this.page,
            pageSize: this.restored ? this.size : this.pageSize,
            filters: new Map([...this.filters].filter(([_key, value]) => value !== undefined && value !== null && value !== '' && value !== false )),
            state: this.state,
            restored: this.restored,
            hash: this.hash,
        };

        if (this.orderBy) {
            params.orderBy = `${this.orderBy}:${this.getOrderByDirForField(this.orderBy) || 'asc'}`;
        }

        if (this.restored) {
            this.restored = false;
        }

        return params;
    }


}
