import {LitElement, html, css} from "lit";
import {customElement, property} from "lit/decorators.js";
import {ifDefined} from "lit/directives/if-defined.js";

@customElement('kode4-nav')
export default class Kode4Nav extends LitElement {

    @property({ type: String })
    public href:String|undefined;

    @property({ type: String })
    public target:String|undefined = undefined;

    private handleLinkClick(e:MouseEvent) {
        e.stopPropagation();
        this.click();
    }

    public render() {
        return this.href ? this.renderLink() : this.renderSimple();
    }

    protected renderSimple() {
        return html`
                <slot></slot>
        `;
    }

    protected renderLink() {
        return html`
            <a
                href="${this.href}"
                target="${ifDefined(this.target)}"
                @click="${this.handleLinkClick}">
                <slot></slot>
            </a>
        `;
    }

    static styles = [
        css`
            * {
                box-sizing: border-box;
            }

            :host {
                display: block;
                cursor: pointer;
                color: var(--kode4-nav-text);
                background-color: var(--kode4-nav-background);
                border-radius: var(--kode4-nav-border-radius);
                padding-top: var(--kode4-nav-padding-top);
                padding-right: var(--kode4-nav-padding-right);
                padding-bottom: var(--kode4-nav-padding-bottom);
                padding-left: var(--kode4-nav-padding-left);
                transition: all 0.15s linear
            }
            
            :host([inline]) {
                display: inline-block;
            }

            :host(:hover) {
                color: var(--kode4-nav-text-hover);
                background-color: var(--kode4-nav-background-hover);
            }

            :host([active]) {
                color: var(--kode4-nav-active-text);
                background-color: var(--kode4-nav-active-background);
            }

            :host([active]:hover) {
                color: var(--kode4-nav-active-text-hover);
                background-color: var(--kode4-nav-active-background-hover);
            }
            
            a {
                display: block;
                color: inherit;
                text-decoration: inherit;
            }
        `,
    ];

}
