import {LitElement, html, css} from "lit";
import {customElement, property} from "lit/decorators.js";
import kode4CssTheme from "./kode4-css";

@customElement('kode4-column')
export default class Kode4Column extends LitElement {

    @property({ type: Boolean, attribute: true })
    public spacing:boolean = false;

    private updateCssClass(changedProperties:Map<PropertyKey, unknown>, prop:string, value:boolean, cssClass:string) {
        if (changedProperties.has(prop)) {
            value ? this.classList.add(cssClass) : this.classList.remove(cssClass);
        }
    }

    protected updated(changedProperties:Map<PropertyKey, unknown>) {
        super.updated(changedProperties);

        this.updateCssClass(changedProperties, 'spacing', this.spacing, 'kode4-spacing');
    }

    public render() {
        return html`
            <slot></slot>
        `;
    }

    static styles = [
        kode4CssTheme,
        css`
            * {
                box-sizing: border-box;
            }

            :host {
                display: flex;
                flex-direction: column;
                flex-wrap: nowrap;
                height: 100%;
            }

            :host(.kode4-spacing) {
                gap: var(--kode4-toolbar-spacing);
            }

            ::slotted(*:not(kode4-row)) {
                position: relative;
                //align-self: stretch;
                //display: flex;
                //flex-direction: column;
                //flex-wrap: nowrap;
            }

            ::slotted(.kode4-stretch) {
                flex: 1 0 1px;
                place-self: flex-end stretch;
            }
        `,
    ];

}
