import {LitElement, html, css, TemplateResult} from "lit";
import {customElement, property} from "lit/decorators.js";
import kode4CssTheme from './kode4-css';
import {v4 as uuidv4} from "uuid";
import {faTimes} from "@fortawesome/free-solid-svg-icons/faTimes";

export interface Kode4GrowlMessage {
    id?: string,
    message: TemplateResult | string;
    color?: string;
    timeout?: number;
    pos?: string;
}

export const dispathGrowlAlert = (el: HTMLElement, position: string, message: Kode4GrowlMessage) => {
    el.dispatchEvent(new CustomEvent("kode4-growl-alert", {
        bubbles: true,
        composed: true,
        detail: {
            ...message,
            pos: position,
        }
    }));
}

export interface IKode4GrowlMessengerObserver {

    onGrowlAlert(message: Kode4GrowlMessage): void;

}

export class Kode4GrowlMessenger {

    private observers: Array<IKode4GrowlMessengerObserver> = [];

    public observe(observer: IKode4GrowlMessengerObserver) {
        if (!this.observers.includes(observer)) {
            this.observers.push(observer);
        }
    }

    public unobserve(observer?: IKode4GrowlMessengerObserver) {
        if (!observer) {
            this.observers = [];
        } else {
            this.observers = [
                ...this.observers.filter((el: IKode4GrowlMessengerObserver) => el !== observer)
            ];
        }
    }

    public alert(message: Kode4GrowlMessage) {
        this.fireGrowlAlert(message);
    }

    private fireGrowlAlert = (message: Kode4GrowlMessage) => {
        this.observers.forEach((observer: IKode4GrowlMessengerObserver) => {
            observer.onGrowlAlert(message);
        });
    }

}

const _growlMessenger: Kode4GrowlMessenger = new Kode4GrowlMessenger();

export const getGrowlMessenger = () => {
    return _growlMessenger;
}

@customElement('kode4-growl')
export default class Kode4Growl extends LitElement {

    @property()
    public messages: Array<Kode4GrowlMessage> = [];

    @property({attribute: 'horizontal-align'})
    public horizontalAlign: string = 'right';

    @property({attribute: 'vertical-align'})
    public verticalAlign: string = 'bottom';

    public alert(growlMessage: Kode4GrowlMessage) {
        if (!growlMessage.id) {
            growlMessage.id = `kode4growlmessage_${uuidv4()}`;
        }
        const id = growlMessage.id;

        this.messages = [
            ...this.messages,
            growlMessage
        ];
        if (growlMessage.timeout) {
            setTimeout(() => {
                this.closeMessage(id);
            }, growlMessage.timeout);
        }
    }

    public closeMessage(id?: String) {
        if (!id) {
            return;
        }

        this.messages = [
            ...this.messages.filter((el: Kode4GrowlMessage) => el.id !== id)
        ];
    }

    public render() {
        return html`
            <div class="container ${this.horizontalAlign} ${this.verticalAlign}" part="container">
                ${this.messages.map((msg: Kode4GrowlMessage) => html`
                    <div class="message kode4-alert ${msg.color ? `kode4-bg-${msg.color}` : ''}"
                         id="${msg.id}">
                        <div class="closeButton kode4-text-hover" @click="${(e: MouseEvent) => {
                            e.preventDefault();
                            this.closeMessage(msg.id);
                        }}">
                            <kode4-fa-icon .icon="${faTimes}"></kode4-fa-icon>
                        </div>
                        ${msg.message}
                    </div>
                `)}
            </div>
        `;
    }

    static styles = [
        kode4CssTheme,
        css`
            :host {
                display: inline-block;
                position: relative;
                width: 0;
                height: 0;
                padding: 0;
                margin: 0;
                border: none;
                z-index: 10010;
            }

            .container {
                position: absolute;
                max-height: 600px;
                bottom: 0;
                right: 0;
                z-index: 10020;
            }

            .container.bottom {
                top: auto;
                bottom: 0;
            }

            .container.top {
                top: 0;
                bottom: auto;
            }

            .container.right {
                left: auto;
                right: 0;
                width: 300px;
                max-width: 80vw;
            }

            .container.left {
                left: 0;
                right: auto;
                width: 300px;
                max-width: 80vw;
            }

            .container.stretch {
                left: 0;
                right: 0;
                width: auto;
            }

            .message {
                width: auto;
                position: relative;
            }

            .message + .message {
                margin-top: 10px;
            }

            .closeButton {
                position: absolute;
                top: 5px;
                right: 5px;
                cursor: pointer;
                opacity: 0;
                transition: all 0.15s linear;
            }

            .message:hover .closeButton {
                opacity: 1;
            }
        `,
    ];

}
