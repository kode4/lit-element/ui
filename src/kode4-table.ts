import {LitElement, html, css, PropertyValues} from "lit";
import {customElement, property, query, queryAssignedElements, state} from "lit/decorators.js";
import kode4CssTheme from "./kode4-css";

@customElement('kode4-table')
export default class Kode4Table extends LitElement {

    @query('#listEndObserver')
    public listEndObserverTrigger!:HTMLElement;

    @property({ type: 'String', attribute: 'observer-root' })
    public listEndObserverRootSelector?:string;

    @property({ type: 'String' })
    public observerRoot?:HTMLElement;

    @property({ type: Boolean, attribute: 'observer-fix' })
    public observerFix:boolean = false;

    @property({ attribute: false })
    public more?:Function;

    @property({ attribute: false })
    public changed?:Function;

    @queryAssignedElements()
    private listBodyElements?: Array<HTMLElement>;

    @state()
    private get empty():boolean {
        return !this.listBodyElements || !!this.listBodyElements.length;
    }

    private listEndObserverOptions:any = {
        rootMargin: '0px',
        threshold: 1,
    }

    private listEndObserver!:IntersectionObserver;

    protected async firstUpdated(_changedProperties: PropertyValues) {
        super.firstUpdated(_changedProperties);

        // let parent:HTMLElement|null = this;
        // let theRoot:HTMLElement|null = null;
        // while (!theRoot && parent) {
        //     const hasOverflow = getComputedStyle(parent).overflow;
        //     const hasOverflowY = getComputedStyle(parent).overflowY;
        //     console.log(' => OVERFLOW', hasOverflow, hasOverflowY, parent);
        //     if (['auto', 'scroll'].includes(hasOverflowY)) {
        //         console.log(' => FOUND THE ELEMENT!');
        //         theRoot = parent;
        //     }
        //     parent = parent.parentElement;
        // }
        // console.log('----------------------------------------')
        // console.log(theRoot);

        /*if (this.observerRoot) {
            this.listEndObserverOptions.root = this.observerRoot;
        } else*/

        if (this.listEndObserverRootSelector) {
            this.listEndObserverOptions.root = this.closest(this.listEndObserverRootSelector);
        }

        this.listEndObserver = new IntersectionObserver(async (entries:any) => {
            if (entries[0].intersectionRatio >= this.listEndObserverOptions.threshold) {
                await this.triggerObserverHandler();
                // if (this.more instanceof Function) {
                //     this.stopObserver();
                //     await this.more();
                //     this.startObserver();
                //
                // } else {
                //     this.dispatchEvent(new CustomEvent('kode4-table-more', {
                //         bubbles: true,
                //         composed: true,
                //     }));
                // }
            }
        }, this.listEndObserverOptions);

        this.startObserver();
    }

    public async triggerObserverHandler() {
        if (this.more instanceof Function) {
            // this.stopObserver();
            await this.more();
            // this.startObserver();

        } else {
            this.dispatchEvent(new CustomEvent('kode4-table-more', {
                bubbles: true,
                composed: true,
            }));
        }
    }

    @state()
    private observerIsRunning:boolean = false;

    public stopObserver() {
        if (!this.observerIsRunning) {
            return;
        }
        if (!this.currentObservedElement) {
            return;
        }
        this.listEndObserver.unobserve(this.currentObservedElement);
        this.observerIsRunning = false;
        this.currentObservedElement = undefined;
    }

    private currentObservedElement?:HTMLElement;

    public startObserver() {
        if (this.observerIsRunning) {
            return;
        }

        if (this.listBodyElements?.length) {
            this.currentObservedElement =  this.listBodyElements[this.listBodyElements.length - 1];
        } else {
            this.currentObservedElement = this.listEndObserverTrigger;
        }

        this.listEndObserver.observe(this.currentObservedElement);
        this.observerIsRunning = true;
    }

    disconnectedCallback() {
        this.listEndObserver.disconnect();
        this.observerIsRunning = false;
        super.disconnectedCallback();
    }

    handleSlotchange() {
        this.stopObserver();
        if (this.changed instanceof Function) {
            this.changed();
        }
        this.startObserver();
    }

    public render() {
        return html`
            <div class="header">
                <slot name="header"></slot>
            </div>

            <slot @slotchange=${this.handleSlotchange}></slot>

            <div class="empty ${this.empty ? 'hidden' : ''}" style="display: table-row-group !important; width:100%;">
                <slot name="empty">
                    <div class="kode4-center-h" part="empty">
                        --
                    </div>
                </slot>
            </div>

            <div id="listEndObserver" class="${this.observerFix ? 'absolute' : undefined}" part="end-observer"></div>
        `;
    }

    static styles = [
        kode4CssTheme,
        css`
            * {
                box-sizing: border-box;
            }

            :host {
                position: relative;
                display: table;
                table-layout: fixed;
                width: 100%;
                background-color: #fff;
            }
            
            .header {
                display: table-row;
                position: sticky;
                top: 0;
                background-color: inherit;
            }

            .header > ::slotted(*) {
                display: table-cell;
            }

            #listEndObserver {
                display: block;
                height: 0;
                width: 100%;
            }

            #listEndObserver.absolute {
                display: block;
                position: absolute;
                height: 0;
                width: 0;
                left: 0;
                bottom: 20px;
            }
            
            .empty:not(.hidden) {
                display: block;
                visibility: visible;
                position: absolute;
                left: 0;
                right: 0;
                top: 100%;
                width: 100%;
            }
            
            .empty.hidden {
                display: none;
                visibility: hidden;
            }
        `
    ]

}
