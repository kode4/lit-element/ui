import {LitElement, html, css, PropertyValues} from "lit";
import {customElement, property, state, queryAssignedElements} from "lit/decorators.js";

@customElement('kode4-sidemenu')
export default class Kode4Sidemenu extends LitElement {

    @queryAssignedElements({slot: 'header'})
    private headerItems!: Array<HTMLElement>;

    @queryAssignedElements()
    private mainItems!: Array<HTMLElement>;

    @queryAssignedElements({slot: 'footer'})
    private footerItems!: Array<HTMLElement>;

    @property({ type: Boolean, reflect: true })
    public open: boolean = false;

    @property({ type: Boolean, reflect: true })
    public alwaysopen: boolean = false;

    @property({ type: Boolean })
    public absolute: boolean = false;

    @property({ type: Boolean })
    public left: boolean = true;

    @property({ type: Boolean })
    public right: boolean = false;

    @property({ type: Boolean })
    public shadow: boolean = false;

    @property({ type: Boolean })
    public animations: boolean = true;

    @state()
    private animationEnabled: boolean = false;

    @property({ type: Number })
    public get contentWidth():string {
        let maxWidth:number = 100;
        [...this.headerItems, ...this.mainItems, ...this.footerItems]?.forEach((el:HTMLElement) => {
            maxWidth = Math.max(maxWidth, el.offsetWidth);
        });
        return `${maxWidth}px`;
    }

    private resizeContentObserver:ResizeObserver = new ResizeObserver(() => {
        this.requestUpdate();
    });

    @state()
    protected get cssClassLeftRight():string {
        if (this.right) {
            return 'right';
        }
        return 'left';
    }

    protected firstUpdated(_changedProperties: PropertyValues) {
        super.firstUpdated(_changedProperties);
        [...this.headerItems, ...this.mainItems, ...this.footerItems]?.forEach((el:HTMLElement) => {
            this.resizeContentObserver.observe(el);
        });

        if (this.animations) {
            setTimeout(() => {
                this.animationEnabled = true;
            }, 100);
        }
    }

    connectedCallback() {
        super.connectedCallback();
    }

    disconnectedCallback() {
        // this.resizeContentObserver.unobserve([...this.headerItems, ...this.mainItems, ...this.footerItems]);
        this.resizeContentObserver.disconnect();
        super.disconnectedCallback();
    }

    public render() {
        return html`
            <div class="body ${this.open || this.alwaysopen ? 'open' : ''} ${this.alwaysopen ? 'alwaysopen'  : ''} ${this.absolute ? 'absolute' : ''} ${this.cssClassLeftRight} ${this.shadow ? 'shadow' : ''} ${this.animationEnabled ? 'animate' : ''}" style="width: ${this.contentWidth};">
                <kode4-column class="bodycontent" part="container">
                    <slot name="header"></slot>
                    <div style="flex: 1 0 1px; overflow: auto;">
                        <slot class="kode4-stretch"></slot>
                    </div>
                    <slot name="footer"></slot>
                </kode4-column>
            </div>
        `;
    }

    static styles = [
        css`
            * {
                box-sizing: border-box;
            }
            
            :host {
                display: block;
                padding: 0;
                position: relative;
            }

            .body {
                display: block;
                position: relative;
                overflow-y: hidden;
                height: 100%;
            }
            
            .body.animate:not(.alwaysopen) {
                transition: all 0.25s linear;
            }
            
            .body.shadow.left {
                box-shadow: 2px 0 6px 0 rgba(0, 0, 0, 0.2);
            }
            
            .body.shadow.right {
                box-shadow: -2px 0 6px 0 rgba(0, 0, 0, 0.2);
            }
            
            .bodycontent {
                position: absolute;
                bottom: 0;
                top: 0;
                overflow: hidden;
            }
            
            .body.left .bodycontent {
                left: auto;
                right: 0;
            }
            
            .body.right .bodycontent {
                left: 0;
                right: auto;
            }
            
            .body.absolute {
                position: absolute;
                top: 0;
                bottom: 0;
                height: auto;
                overflow: visible;
            }

            .body.absolute.left {
                left: 0;
                right: auto;
            }

            .body.absolute.right {
                left: auto;
                right: 0;
            }

            .body:not(.open) {
                width: 0 !important;
                overflow: hidden;
            }
        `,
    ];

}
