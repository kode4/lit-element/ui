import { LitElement, html, css, PropertyValues } from "lit";
import {customElement, property} from "lit/decorators.js";
import kode4CssTheme from './kode4-css';

@customElement('kode4-tooltip')
export default class Kode4Tooltip extends LitElement {

    @property({ type: Boolean })
    public open:boolean = false;

    @property({ type: Number })
    public closeDelay:number = 150;

    @property({ type: String })
    public x:string = 'right';

    @property({ type: String })
    public y:string = 'center';

    @property({ type: Boolean })
    public mouseEntered:boolean = false;

    @property()
    public container:HTMLElement|null = null;

    private parendMouseenterEventListener:any;
    private parendMouseleaveEventListener:any;

    public connectedCallback() {
        this.parendMouseenterEventListener = this.onParentMouseover.bind(this);
        this.parendMouseleaveEventListener = this.onParentMouseleave.bind(this);
        
        this.parentElement?.addEventListener('mouseenter', this.parendMouseenterEventListener);
        this.parentElement?.addEventListener('mouseleave', this.parendMouseleaveEventListener);

        super.connectedCallback();
    }    

    public disconnectedCallback() {
        this.parentElement?.removeEventListener('mouseenter', this.parendMouseenterEventListener);
        this.parentElement?.removeEventListener('mouseleave', this.parendMouseleaveEventListener);
        super.disconnectedCallback();
    }

    public firstUpdated(changedProperties:PropertyValues) {
        super.firstUpdated(changedProperties);
        this.container = this.shadowRoot?.querySelector('.container') || null;
    }

    public onParentMouseover() {
        this.mouseEntered = true;
        // const openDelay = this.closeDelay > 1 ? this.closeDelay-1 : 0;

        setTimeout(() => {
            if (this.parentElement && this.container) {
                this.container.classList.add('opening');
                const parentRect:DOMRect = <DOMRect>this.parentElement?.getBoundingClientRect();
                const containerRect:DOMRect = <DOMRect>this.container.getBoundingClientRect();
                const rect:DOMRect = <DOMRect>this.getBoundingClientRect();
                
                // const offsetX = rect.x - parentRect.x;
                // const offsetY = rect.y - parentRect.y;

                let tooltipY;
                switch (this.y) {
                    case 'top' :
                        tooltipY = 0 - (rect.y - parentRect.y) - containerRect.height - 10;
                        break;

                    case 'bottom' :
                        tooltipY = parentRect.height + 10;
                        break;

                    case 'center' :
                    default :
                        tooltipY = parentRect.height / 2 - (rect.y - parentRect.y) - containerRect.height / 2;
                        
                }

                let tooltipX;
                switch (this.x) {
                    case 'center' :
                        tooltipX = parentRect.width / 2 - (rect.x - parentRect.x) - containerRect.width / 2;
                        break;

                    case 'left' :
                        tooltipX = 0 - (rect.x - parentRect.x) - containerRect.width - 10;
                        break;

                    case 'right' :
                    default :
                        tooltipX = parentRect.width + 10;
                        
                }

                this.container.style.top = `${tooltipY}px`;
                this.container.style.left = `${tooltipX}px`;

                this.container.classList.remove('opening');
            }

            this.open = true;
        }, this.closeDelay-10);
    }

    public onParentMouseleave() {
        this.mouseEntered = false;
        setTimeout(() => {
            if (!this.mouseEntered) {
                this.open = false;
            }
        }, this.closeDelay);
    }

    public render() {
        return html`
            <div class="container ${this.open ? 'open' : ''} ${this.arrowclass}">
                <slot></slot>
            </div>
        `;
    }

    @property()
    private get arrowclass():string {
        if (this.y === 'bottom' && this.x === 'center') {
            return 'arrowtop';
        }
        if (this.y === 'top' && this.x === 'center') {
            return 'arrowbottom';
        }
        if (this.x === 'left' && this.y === 'center') {
            return 'arrowleft';
        }
        if (this.x === 'right' && this.y === 'center') {
            return 'arrowright';
        }
        return '';
    }

    static styles = [
        kode4CssTheme,
        css`
            :host {
                position: relative;
                filter: none !important;
                background: transparent;
            }

            .container {
                display: none;
            }
            
            .container.open,
            .container.opening {
                display: block;
                position: absolute;
                z-index: 9000;
                top: 0;
                left: 0;
                padding-top: var(--kode4-tooltip-padding-top);
                padding-right: var(--kode4-tooltip-padding-right);
                padding-bottom: var(--kode4-tooltip-padding-bottom);
                padding-left: var(--kode4-tooltip-padding-left);
                filter: brightness(1) sepia(0) hue-rotate(0) saturate(100%);
                color: var(--kode4-tooltip-color);
                font-family: var(--kode4-tooltip-font-family);
                font-size: var(--kode4-tooltip-font-size);
                line-height: var(--kode4-tooltip-line-height);
                font-weight: var(--kode4-tooltip-font-weight);
                background: var(--kode4-tooltip-background-color);
                box-sizing: border-box;
                border-radius: var(--kode4-tooltip-border-radius);
            }

            .container.opening {
                visibility: hidden;
            }

            .container.open.arrowtop:before {
                content: " ";
                background: transparent;
                position: absolute;
                top: calc(var(--kode4-tooltip-arrow-size) * -1);
                left: calc(50% - var(--kode4-tooltip-arrow-size));
                display: block;
                width: 0;
                height: 0;
                border-style: solid;
                border-width: 0 var(--kode4-tooltip-arrow-size) var(--kode4-tooltip-arrow-size) var(--kode4-tooltip-arrow-size);
                border-color: transparent transparent var(--kode4-tooltip-background-color) transparent;
            }
            
            .container.open.arrowtop:before {
                content: " ";
                background: transparent;
                position: absolute;
                top: calc(var(--kode4-tooltip-arrow-size) * -1);
                left: calc(50% - var(--kode4-tooltip-arrow-size));
                display: block;
                width: 0;
                height: 0;
                border-style: solid;
                border-width: 0 var(--kode4-tooltip-arrow-size) var(--kode4-tooltip-arrow-size) var(--kode4-tooltip-arrow-size);
                border-color: transparent transparent var(--kode4-tooltip-background-color) transparent;
            }
            
            .container.open.arrowright:before {
                content: " ";
                background: transparent;
                position: absolute;
                top: calc(50% - var(--kode4-tooltip-arrow-size));
                left: calc(var(--kode4-tooltip-arrow-size) * -1);
                display: block;
                width: 0;
                height: 0;
                border-style: solid;
                border-width: var(--kode4-tooltip-arrow-size) var(--kode4-tooltip-arrow-size) var(--kode4-tooltip-arrow-size) 0;
                border-color: transparent var(--kode4-tooltip-background-color) transparent transparent;
            }
            
            .container.open.arrowbottom:before {
                content: " ";
                background: transparent;
                position: absolute;
                bottom: calc(var(--kode4-tooltip-arrow-size) * -1);
                left: calc(50% - var(--kode4-tooltip-arrow-size));
                display: block;
                width: 0;
                height: 0;
                border-style: solid;
                border-width: var(--kode4-tooltip-arrow-size) var(--kode4-tooltip-arrow-size) 0 var(--kode4-tooltip-arrow-size);
                border-color: var(--kode4-tooltip-background-color) transparent transparent transparent;
            }
            
            .container.open.arrowleft:before {
                content: " ";
                background: transparent;
                position: absolute;
                top: calc(50% - var(--kode4-tooltip-arrow-size));
                right: calc(var(--kode4-tooltip-arrow-size) * -1);
                display: block;
                width: 0;
                height: 0;
                border-style: solid;
                border-width: var(--kode4-tooltip-arrow-size) 0 var(--kode4-tooltip-arrow-size) var(--kode4-tooltip-arrow-size);
                border-color: transparent transparent transparent var(--kode4-tooltip-background-color);
            }
        `,
    ];

}
