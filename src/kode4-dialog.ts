import {LitElement, html, css, PropertyValues} from "lit";
import {customElement, property, query, queryAssignedElements, state} from "lit/decorators.js";
import Kode4Card from "./kode4-card";

export const kode4Dialogs:Map<string, Kode4Dialog> = new Map();

@customElement('kode4-dialog')
export default class Kode4Dialog extends LitElement {

    @query('kode4-card')
    protected body?:Kode4Card;

    @property({ type: String, attribute: 'name' })
    private name?:string;

    // @state()
    // private isOpen:boolean = false;

    @property({ type: Boolean, attribute: 'open', reflect: true })
    private open:boolean = false;

    // @property({ type: Boolean, attribute: 'open', reflect: true })
    // public set open(value:boolean) {
    //     console.log('[Kode4Drawer] change open', value);
    //     if (value && !this.isOpen) {
    //         console.log('[Kode4Drawer] ... go on and show');
    //         this.show();
    //     }
    // }
    //
    // public get open():boolean {
    //     return this.isOpen;
    // }

    private toggleCssClass(show:boolean, cssClass:string) {
        show ? this.classList.add(cssClass) : this.classList.remove(cssClass);
    }

    @property({ type: Boolean, attribute: 'left' })
    public set left(value:boolean) {
        this.toggleCssClass(value, 'kode4-dialog-left');
    }

    public get left():boolean {
        return this.classList.contains('kode4-dialog-left');
    }

    @property({ type: Boolean, attribute: 'right' })
    public set right(value:boolean) {
        this.toggleCssClass(value, 'kode4-dialog-right');
    }

    public get right():boolean {
        return this.classList.contains('kode4-dialog-right');
    }

    @property({ type: Boolean, attribute: 'top' })
    public set top(value:boolean) {
        this.toggleCssClass(value, 'kode4-dialog-top');
    }

    public get top():boolean {
        return this.classList.contains('kode4-dialog-top');
    }

    @property({ type: Boolean, attribute: 'bottom' })
    public set bottom(value:boolean) {
        this.toggleCssClass(value, 'kode4-dialog-bottom');
    }

    public get bottom():boolean {
        return this.classList.contains('kode4-dialog-bottom');
    }

    @property({ type: Boolean, attribute: true })
    public transparent:boolean = false;

    @property({ type: Boolean, attribute: true, reflect: true })
    public modal:boolean = false;

    @state()
    private modalResolve?:Function;

    @state()
    private modalReject?:Function;

    public async show():Promise<boolean> {
        this.open = true;
        return new Promise((resolve, reject) => {
            this.modalResolve = resolve;
            this.modalReject = reject;
        });
    }

    protected onConfirm(e:MouseEvent) {
        if (this.modalResolve) {
            e.stopPropagation();
            this.modalResolve(true);
            this.hide();
        } else {
            this.onError(e);
        }
    }

    protected onCancel(e:MouseEvent) {
        if (this.modalResolve) {
            e.stopPropagation();
            this.modalResolve(false);
            this.hide();
        } else {
            this.onError(e);
        }
    }

    protected onError(e:MouseEvent) {
        e.stopPropagation();
        if (this.modalReject) {
            this.modalReject();
            this.hide();
        } else {
            throw new Error(`No modal handlers found for Kode4Dialog: ${this.name}`);
        }
    }

    public hide() {
        this.open = false;
        this.modalResolve = undefined;
        this.modalReject = undefined;
        this.dispatchEvent(new CustomEvent('close', {
            bubbles: true,
            composed: true,
        }))
    }

    private handleBackdropClick(e:MouseEvent) {
        e.stopPropagation();
        e.preventDefault();
        if (!this.modal) {
            this.hide();
        }
    }

    // @property({ type: Number })
    // public get contentWidth():string {
    //     let maxWidth:number = 100;
    //     [...this.headerItems, ...this.mainItems, ...this.footerItems]?.forEach((el:HTMLElement) => {
    //         maxWidth = Math.max(maxWidth, el.offsetWidth);
    //     });
    //     return `${maxWidth}px`;
    // }

    @queryAssignedElements({slot: 'header'})
    private headerItems!: Array<HTMLElement>;

    @queryAssignedElements()
    private mainItems!: Array<HTMLElement>;

    @queryAssignedElements({slot: 'footer'})
    private footerItems!: Array<HTMLElement>;

    private resizeContentObserver:ResizeObserver = new ResizeObserver(() => {
        this.requestUpdate();
    });

    @property({ type: Number })
    public get contentWidth():number {
        let maxWidth:number = 0;
        [...this.headerItems, ...this.mainItems, ...this.footerItems]?.forEach((el:HTMLElement) => {
            maxWidth = Math.max(maxWidth, el.offsetWidth);
        });
        return maxWidth;
    }

    @property({ type: Number })
    public get contentHeight():number {
        let maxHeight:number = 0;
        [...this.headerItems, ...this.mainItems, ...this.footerItems]?.forEach((el:HTMLElement) => {
            maxHeight = Math.max(maxHeight, el.offsetHeight);
        });
        return maxHeight;
    }

    protected firstUpdated(_changedProperties: PropertyValues) {
        super.firstUpdated(_changedProperties);
        this.mainItems?.forEach((el:HTMLElement) => {
            this.resizeContentObserver.observe(el);
        });

        // if (this.animations) {
        //     setTimeout(() => {
        //         this.animationEnabled = true;
        //     }, 100);
        // }
    }

    connectedCallback() {
        if (this.name) {
            kode4Dialogs.set(this.name, this);
        }
        super.connectedCallback();
    }

    disconnectedCallback() {
        // this.resizeContentObserver.unobserve([...this.headerItems, ...this.mainItems, ...this.footerItems]);
        this.resizeContentObserver.disconnect();
        if (this.name && kode4Dialogs.has(this.name)) {
            kode4Dialogs.delete(this.name);
        }
        super.disconnectedCallback();
    }

    handleSlotchange() {
        console.log('Whatever');
        // this.requestUpdate();
        // console.log('[Kode4Dialog] slotchange, re-calc sizes:', this.name, this.body?.contentWidth, this.body?.contentHeight, window.innerWidth, window.innerHeight);
    }

    public __render() {
        return html`
            <kode4-backdrop ?visible="${this.open}" @click="${this.handleBackdropClick}" @contextmenu="${this.handleBackdropClick}" ?transparent="${this.transparent}"></kode4-backdrop>
            <div class="dialog">
                <kode4-column class="dialog-content" part="container" style="${this.open ? `top: calc(50vh - ${this.contentHeight / 2}px);` : undefined} ${this.open ? `left: calc(50vw - ${this.contentWidth / 2}px);` : undefined} width: ${this.contentWidth}px; height: ${this.contentHeight}px;">
                    <slot name="header" @slotchange=${this.handleSlotchange}></slot>
                    <div class="body" part="body">
                        <slot @slotchange=${this.handleSlotchange}></slot>
                    </div>
                    <div>
                        <slot name="footer" @slotchange=${this.handleSlotchange}>
                            <kode4-row slot="footer">
                                <div slot="title">XXX</div>
                                <div @click="${this.onConfirm}" style="cursor: pointer; display: block; border: solid 1px #f00; padding: 5px; background-color: #0f0;">KONFIRM</div>
                                <div class="kode4-stretch"></div>
                                <div @click="${this.onCancel}" style="cursor: pointer; display: block; border: solid 1px #f00; padding: 5px; background-color: #0f0;">KANCEL</div>
                            </kode4-row>
                        </slot>
                    </div>
                </kode4-column>
            </div>
        `;
    }
            // <div class="drawer" style="${(this.left || this.right) && this.contentWidth > 0 ? `width: ${this.contentWidth}px;`: undefined} ${(this.top || this.bottom) && this.contentHeight > 0 ? `height: ${this.contentHeight}px;`: undefined}" @click="${this.handleListClick}">

    public render() {
        return html`
            <kode4-backdrop ?visible="${this.open}" @click="${this.handleBackdropClick}" @contextmenu="${this.handleBackdropClick}" ?transparent="${this.transparent}"></kode4-backdrop>
            <div class="dialog" style="${this.contentWidth > 0 ? `left: calc(50vw - ${this.contentWidth / 2}px);`: undefined} ${this.contentHeight > 0 ? `top: calc(50vh - ${this.contentHeight / 2}px);`: undefined}">
                <div class="dialog-content">
                        
                    <div class="body">
                        <slot @slotchange=${this.handleSlotchange}></slot>
                    </div>
                        
                    <div>
                        <hr>
                        Width: ${this.contentWidth}
                        <br>
                        Height: ${this.contentHeight}
                    </div>
                </div>
            </div>
        `;
    }
            // <div class="drawer" style="${(this.left || this.right) && this.contentWidth > 0 ? `width: ${this.contentWidth}px;`: undefined} ${(this.top || this.bottom) && this.contentHeight > 0 ? `height: ${this.contentHeight}px;`: undefined}" @click="${this.handleListClick}">

    static styles = [
        css`
            * {
                box-sizing: border-box;
            }
            
            :host {
                position: fixed;
                display: block;
                z-index: 10010;
                left: 0;
                right: 0;
                top: 0;
                bottom: 0;
                //transition: left 0s linear 0s;
                border: solid 2px #0ff;
            }

            :host(:not([open])) {
                //left: 50vw;
                //right: -50vw;
                visibility: hidden;
                //transition: left 0s linear 0.1s;
            }
            
            //:host(:not([open])) {
            //    bottom: 100vh;
            //    //left: 50vw;
            //    //right: -50vw;
            //    visibility: hidden;
            //    //transition: left 0s linear 0.1s;
            //}

            .dialog {
                position: fixed;
                border: solid 2px #00f;
                //max-width: 80vw;
                background-color: #fff;
                max-width: 90vw;
                max-height: 90vh;
                z-index: 10100;
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;
            }
            
            .dialog-content {
                flex: 1 0 1px;
                border: solid 2px #f00;
                height: 100%;
                overflow: hidden;
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;
                justify-content: stretch;
                align-items: stretch;
                //height: 100%;
            }
        
            
            .body {
                flex: 1 0 1px;
                overflow: auto;
                
            }
        `,
        // css`
        //     * {
        //         box-sizing: border-box;
        //     }
        //
        //     :host {
        //         position: fixed;
        //         display: block;
        //         z-index: 10010;
        //         left: 0;
        //         //right: 0;
        //         top: 0;
        //         //bottom: auto;
        //         transition: left 0s linear 0s;
        //     }
        //
        //     :host(:not([open])) {
        //         //left: 1000vw;
        //         transition: left 0s linear 0.1s;
        //     }
        //
        //     .body {
        //         display: block;
        //         //flex: 0 1 100%;
        //         //height: auto !important;
        //         //overflow-x: hidden;
        //         //overflow-y: auto;
        //         //border: solid 2px #00f;
        //         //background-color: #ccc;
        //     }
        //
        //     .dialog {
        //         position: absolute;
        //         display: block;
        //         left: 0;
        //         //right: 0;
        //         top: 0;
        //         //bottom: auto;
        //         border: solid 2px #00f;
        //     }
        //
        //     .dialog-content {
        //         border: solid 2px #f00;
        //         position: absolute;
        //         display: block;
        //         z-index: 10050;
        //         transition: all .05s linear, left 0s linear .05s;
        //         background-color: #fff;
        //         left: 0;
        //         //right: 0;
        //         //top: -100%;
        //         //bottom: auto;
        //         //opacity: 0;
        //     }
        //
        //     :host([open]) .dialog-content {
        //         top: 0;
        //         opacity: 1;
        //         transition: all .05s linear, left 0s linear;
        //     }
        // `,
    ];

    public _render() {
        return html`
            <kode4-backdrop ?visible="${this.open}" @click="${this.handleBackdropClick}" @contextmenu="${this.handleBackdropClick}" ?transparent="${this.transparent}"></kode4-backdrop>
            <kode4-card class="dialog-content" style="${this.open ? `top: 20vh;` : undefined}">
                ${this.modal ? html`
                    <kode4-row slot="header">
                        <div slot="title">XXX</div>
                        <div @click="${this.onConfirm}" style="cursor: pointer; display: block; border: solid 1px #f00; padding: 5px; background-color: #0f0;">KONFIRM</div>
                        <div class="kode4-stretch"></div>
                        <div @click="${this.onCancel}" style="cursor: pointer; display: block; border: solid 1px #f00; padding: 5px; background-color: #0f0;">KANCEL</div>
                    </kode4-row>
                `: undefined}
                <slot @slotchange=${this.handleSlotchange}></slot>
                ${this.modal ? html`
                    <kode4-row slot="footer">
                        <div slot="title">XXX</div>
                        <div @click="${this.onConfirm}" style="cursor: pointer; display: block; border: solid 1px #f00; padding: 5px; background-color: #0f0;">KONFIRM</div>
                        <div class="kode4-stretch"></div>
                        <div @click="${this.onCancel}" style="cursor: pointer; display: block; border: solid 1px #f00; padding: 5px; background-color: #0f0;">KANCEL</div>
                    </kode4-row>
                `: undefined}
            </kode4-card>
        `;
    }
    // <div class="drawer" style="${(this.left || this.right) && this.contentWidth > 0 ? `width: ${this.contentWidth}px;`: undefined} ${(this.top || this.bottom) && this.contentHeight > 0 ? `height: ${this.contentHeight}px;`: undefined}" @click="${this.handleListClick}">

}
