import {LitElement, html, css} from "lit";
import {customElement, property, query, state} from "lit/decorators.js";
import kode4CssTheme from "./kode4-css";
import './kode4-backdrop';
import './kode4-list';

export const kode4Menus:Map<string, Kode4Menu> = new Map();

@customElement('kode4-menu')
export default class Kode4Menu extends LitElement {

    @property({ type: String, attribute: 'name' })
    private name?:string;

    @state()
    private isOpen:boolean = false;

    @property({ type: Boolean, attribute: 'open' })
    public set open(value:boolean) {
        if (value && !this.isOpen) {
            this.show();
        }
    }

    private toggleCssClass(show:boolean, cssClass:string) {
       show ? this.classList.add(cssClass) : this.classList.remove(cssClass);
    }

    @property({ type: Boolean, attribute: true })
    public transparent:boolean = false;

    @property({ type: Boolean, attribute: 'visible' })
    public set visible(value:boolean) {
        this.toggleCssClass(value, 'kode4-menu-visible');
    }

    public get visible():boolean {
        return this.classList.contains('kode4-menu-visible');
    }

    @query('kode4-list')
    private menu!:HTMLElement;

    @property({ type: Boolean, attribute: true })
    public _backdrop:boolean = false;

    @property({ type: Boolean, attribute: true })
    public get backdrop() {
        return this._backdrop;
    }

    public set backdrop(value:boolean) {
        this.toggleCssClass(value, 'kode4-list-backdrop');
        this._backdrop = value;
    }

    @query('.backdrop')
    public backdropElement?:HTMLElement;

    @property({ type: Boolean, attribute: 'floating' })
    private floating:boolean = false;


    @property({ type: Boolean, attribute: 'bound' })
    private bound:boolean = false;

    @state()
    private boundToParent:boolean = false;

    @property({ type: Boolean, attribute: 'parent' })
    public set parent(value:boolean) {
        // this.toggleCssClass(value, 'kode4-list-parent');
        this.boundToParent = value;
    }

    @property({ type: Boolean, attribute: 'left' })
    public set left(value:boolean) {
        this.toggleCssClass(value, 'kode4-list-align-left');
    }

    public get left():boolean {
        return this.classList.contains('kode4-list-align-left');
    }

    @property({ type: Boolean, attribute: 'right' })
    public set right(value:boolean) {
        this.toggleCssClass(value, 'kode4-list-align-right');
    }

    public get right():boolean {
        return this.classList.contains('kode4-list-align-right');
    }

    @property({ type: Boolean, attribute: 'top' })
    public set top(value:boolean) {
        this.toggleCssClass(value, 'kode4-list-align-top');
    }

    public get top():boolean {
        return this.classList.contains('kode4-list-align-top');
    }

    @property({ type: Boolean, attribute: 'bottom' })
    public set bottom(value:boolean) {
        this.toggleCssClass(value, 'kode4-list-align-bottom');
    }

    public get bottom():boolean {
        return this.classList.contains('kode4-list-align-bottom');
    }

    @property({ type: Boolean, attribute: 'stretch' })
    public set stretch(value:boolean) {
        this.toggleCssClass(value, 'kode4-list-align-stretch-x');
    }

    public get stretch():boolean {
        return this.classList.contains('kode4-list-align-stretch-x');
    }

    @property({ type: Boolean, attribute: 'cover' })
    private cover:boolean = false;

    @state()
    public x?:string;

    @state()
    public y?:string;

    @state()
    public minWidth?:string;

    @property({ type: String, attribute: 'w'})
    public width?:string;

    private callerParams?:any;

    public getCallerParams():any {
        return this.callerParams
    }

    public onShow(e:Event|MouseEvent, preventDefault:boolean = true) {
        if (e.defaultPrevented) {
            return;
        }
        if (preventDefault) {
            e.preventDefault();
        }
        if (e instanceof MouseEvent) {
            this.show({
                x: e.pageX,
                y: e.pageY,
            });

        } else {
            this.show();
        }
    }

    public show(params?:{
            x?:number,
            y?:number,
            alignToElement?:HTMLElement,
            callerParams?:any,
        }) {
        if (params?.callerParams) {
            this.callerParams = params.callerParams;
        }

        this.visible = true;
        this.isOpen = true;

        if (this.floating) {
            this.findFloatingPosition({
                x: params?.x,
                y: params?.y,
            });
        }
        if (this.bound || this.boundToParent) {
            this.findBoundToParentPosition(params?.alignToElement || this.parentElement!);
        }

        this.requestUpdate();
    }

    private findFloatingPosition(pos:{
            x?:number,
            y?:number,
        }) {
        const screenW = window.innerWidth;
        const screenH = window.innerHeight;

        const menuBox = this.menu!.getBoundingClientRect();

        let desiredX:number = pos?.x || 10;
        let desiredY:number = pos?.y || 10;

        if (desiredX + menuBox.width > screenW) {
            desiredX = screenW - menuBox.width;
        }
        if (desiredY + menuBox.height > screenH) {
            desiredY = screenH - menuBox.height;
        }

        this.x = `${Math.max(desiredX, 0)}px`;
        this.y = `${Math.max(desiredY, 0)}px`;
    }

    private findBoundToParentPosition(alignToElement:HTMLElement) {
        const screenW = window.innerWidth;
        const screenH = window.innerHeight;

        const parentBox = alignToElement.getBoundingClientRect();
        const menuBox = this.menu!.getBoundingClientRect();

        let desiredX:number;
        let desiredY:number;
        let desiredWidth:number = 0;

        // const correctionX = this.cover ? parentBox.width : 0;
        const correctionY = this.cover ? parentBox.height : 0;

        // align y
        switch (true) {
            case this.top :
                desiredY = parentBox.y - menuBox.height + correctionY;
                if (desiredY < 0) {
                    desiredY = parentBox.y + parentBox.height - correctionY;
                    if (desiredY + menuBox.height > screenH) {
                        desiredY = 0;
                    }
                }
                break;

            default:
            case this.bottom :
                desiredY = parentBox.y + parentBox.height - correctionY;
                if (desiredY + menuBox.height > screenH) {
                    desiredY = parentBox.y - menuBox.height + correctionY;
                    if (desiredY < 0) {
                        desiredY = screenH - menuBox.height
                    }
                }
                break;
        }

        // align x
        switch (true) {
            case menuBox.width > parentBox.width && this.left :
                desiredX = parentBox.x;
                if (desiredX + menuBox.width > screenW) {
                    desiredX = parentBox.x + parentBox.width - menuBox.width;
                    if (desiredX < 0) {
                        desiredX = screenW - menuBox.width
                    }
                }
                break;

            case menuBox.width > parentBox.width && this.right :
                desiredX = parentBox.x + parentBox.width - menuBox.width;
                if (desiredX < 0) {
                    desiredX = parentBox.x;
                    if (desiredX + menuBox.width > screenW) {
                        desiredX = 0;
                    }
                }
                break;

            default:
            case this.stretch :
                desiredX = parentBox.x;
                if (menuBox.width < parentBox.width) {
                    desiredWidth = parentBox.width;
                }
                break;
        }

        this.x = `${Math.max(desiredX, 0)}px`;
        this.y = `${Math.max(desiredY, 0)}px`;
        if (desiredWidth > 0) {
            this.minWidth = `${desiredWidth}px`;
        }
    }

    public hide() {
        this.visible = false;
        this.isOpen = false;
        this.callerParams = undefined;
        this.requestUpdate();
    }

    private handleBackdropClick(e:MouseEvent) {
        e.stopPropagation();
        e.preventDefault();
        this.hide();
    }

    /**
     * Automatically hide menu if a menu-item is clicked an defaultPrevented.
     *
     * @param e
     * @private
     */
    private handleListClick(e:MouseEvent) {
        if (e.defaultPrevented) {
            this.hide();
        }
    }

    public handleParentClick = (e:MouseEvent) => {
        if (e.defaultPrevented) {
            return;
        }
        if (this.isOpen) {
           return;
        }
        e.preventDefault();
        this.show({
            x: e.pageX,
            y: e.pageY,
        })
    }

    connectedCallback() {
        if (this.name) {
            kode4Menus.set(this.name, this);
        }
        if (this.boundToParent) {
            this.parentElement!.addEventListener('click', this.handleParentClick);
        }

        super.connectedCallback();
    }

    disconnectedCallback() {
        if (this.boundToParent) {
            this.parentElement!.removeEventListener('click', this.handleParentClick);
        }
        if (this.name && kode4Menus.has(this.name)) {
            kode4Menus.delete(this.name);
        }
        super.disconnectedCallback();
    }

    // style="top: ${this.y || 'auto'}; left: ${this.x || 'auto'}; ${this.minWidth ? `min-width: ${this.minWidth}` : undefined}; ${this.width ? `width: ${this.width};` : undefined}"

    public render() {
        return html`
            ${this.bound || this.boundToParent || this.floating ? html`
                <kode4-backdrop ?visible="${this.isOpen}" @click="${this.handleBackdropClick}" @contextmenu="${this.handleBackdropClick}" ?transparent="${this.transparent}"></kode4-backdrop>
            ` : undefined}
            <kode4-list style="left: ${this.x || 'auto'}; top: ${this.y || 'auto'}; ${this.minWidth ? `min-width: ${this.minWidth};` : undefined} ${this.width ? `width: ${this.width};` : undefined}" inline @click="${this.handleListClick}">
                <slot></slot>
            </kode4-list>
        `;
    }

    // <kode4-list style=${styleMap(this.listStyles)} inline @click="${this.handleListClick}">

    static styles = [
        kode4CssTheme,
        css`
            * {
                box-sizing: border-box;
            }
            
            :host  {
                padding: 0;
                margin: 0;
                border: none 0;
            }

            :host(:not(.kode4-menu-visible)) {
                display: none;
            }
            
            :host kode4-list,
            :host([floating]) kode4-list,
            :host([bound]) kode4-list,
            :host([parent]) kode4-list {
                position: fixed;
                z-index: 10100;
            }
        `,
    ];

}
