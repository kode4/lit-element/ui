import {LitElement, html, css, PropertyValues} from "lit";
import {customElement, property, queryAssignedElements} from "lit/decorators.js";

export const kode4Drawers:Map<string, Kode4Drawer> = new Map();

@customElement('kode4-drawer')
export default class Kode4Drawer extends LitElement {

    @property({ type: String, attribute: 'name' })
    private name?:string;

    // @state()
    // private isOpen:boolean = false;

    @property({ type: Boolean, attribute: 'open', reflect: true })
    private open:boolean = false;

    // @property({ type: Boolean, attribute: 'open', reflect: true })
    // public set open(value:boolean) {
    //     console.log('[Kode4Drawer] change open', value);
    //     if (value && !this.isOpen) {
    //         console.log('[Kode4Drawer] ... go on and show');
    //         this.show();
    //     }
    // }
    //
    // public get open():boolean {
    //     return this.isOpen;
    // }

    private toggleCssClass(show:boolean, cssClass:string) {
        show ? this.classList.add(cssClass) : this.classList.remove(cssClass);
    }

    @property({ type: Boolean, attribute: 'left' })
    public set left(value:boolean) {
        this.toggleCssClass(value, 'kode4-drawer-left');
    }

    public get left():boolean {
        return this.classList.contains('kode4-drawer-left');
    }

    @property({ type: Boolean, attribute: 'right' })
    public set right(value:boolean) {
        this.toggleCssClass(value, 'kode4-drawer-right');
    }

    public get right():boolean {
        return this.classList.contains('kode4-drawer-right');
    }

    @property({ type: Boolean, attribute: 'top' })
    public set top(value:boolean) {
        this.toggleCssClass(value, 'kode4-drawer-top');
    }

    public get top():boolean {
        return this.classList.contains('kode4-drawer-top');
    }

    @property({ type: Boolean, attribute: 'bottom' })
    public set bottom(value:boolean) {
        this.toggleCssClass(value, 'kode4-drawer-bottom');
    }

    public get bottom():boolean {
        return this.classList.contains('kode4-drawer-bottom');
    }

    @property({ type: Boolean, attribute: true })
    public transparent:boolean = false;

    public doShow() {
        this.open = true;
    }

    public show() {
        this.open = true;
    }

    public hide() {
        this.open = false;
        this.dispatchEvent(new CustomEvent('close', {
            bubbles: true,
            composed: true,
        }))
    }

    private handleBackdropClick(e:MouseEvent) {
        e.stopPropagation();
        e.preventDefault();
        this.hide();
    }

    // @property({ type: Number })
    // public get contentWidth():string {
    //     let maxWidth:number = 100;
    //     [...this.headerItems, ...this.mainItems, ...this.footerItems]?.forEach((el:HTMLElement) => {
    //         maxWidth = Math.max(maxWidth, el.offsetWidth);
    //     });
    //     return `${maxWidth}px`;
    // }

    @queryAssignedElements()
    private mainItems!: Array<HTMLElement>;

    private resizeContentObserver:ResizeObserver = new ResizeObserver(() => {
        this.requestUpdate();
    });

    @property({ type: Number })
    public get contentWidth():number {
        if (this.top || this.bottom) {
            return window.innerWidth;
        }
        let maxWidth:number = 0;
        this.mainItems?.forEach((el:HTMLElement) => {
            maxWidth = Math.max(maxWidth, el.offsetWidth);
        });
        return maxWidth;
    }

    @property({ type: Number })
    public get contentHeight():number {
        let maxHeight:number = 0;
        this.mainItems?.forEach((el:HTMLElement) => {
            maxHeight = Math.max(maxHeight, el.offsetHeight);
        });
        maxHeight = Math.min(maxHeight, window.innerHeight);
        return maxHeight;
    }
    protected firstUpdated(_changedProperties: PropertyValues) {
        super.firstUpdated(_changedProperties);
        this.mainItems?.forEach((el:HTMLElement) => {
            this.resizeContentObserver.observe(el);
        });

        // if (this.animations) {
        //     setTimeout(() => {
        //         this.animationEnabled = true;
        //     }, 100);
        // }
    }

    connectedCallback() {
        if (this.name) {
            kode4Drawers.set(this.name, this);
        }
        super.connectedCallback();
    }

    disconnectedCallback() {
        // this.resizeContentObserver.unobserve([...this.headerItems, ...this.mainItems, ...this.footerItems]);
        this.resizeContentObserver.disconnect();
        if (this.name && kode4Drawers.has(this.name)) {
            kode4Drawers.delete(this.name);
        }
        super.disconnectedCallback();
    }

    /**
     * Automatically hide menu if a menu-item is clicked an defaultPrevented.
     *
     * @param e
     * @private
     */
    private handleListClick(e:MouseEvent) {
        if (e.defaultPrevented) {
            this.hide();
        }
    }

    public render() {
        return html`
            <kode4-backdrop ?visible="${this.open}" @click="${this.handleBackdropClick}" @contextmenu="${this.handleBackdropClick}" ?transparent="${this.transparent}"></kode4-backdrop>
            <div class="drawer" style="${(this.left || this.right) && this.contentWidth > 0 ? `width: ${this.contentWidth}px;`: undefined} ${(this.top || this.bottom) && this.contentHeight > 0 ? `height: ${this.contentHeight}px;`: undefined}" @click="${this.handleListClick}">
                ${this.left || this.right ? html`
                    <kode4-column class="drawer-content">
                        <slot></slot>
                    </kode4-column>
                ` : html`
                    <div class="drawer-content" style="${this.contentHeight > 0 ? `height: ${this.contentHeight}px;`: undefined}">
                        <slot></slot>
                    </div>
                `}
            </div>
        `;
    }

    static styles = [
        css`
            * {
                box-sizing: border-box;
            }
            
            :host {
                position: fixed;
                display: block;
                z-index: 10010;
            }

            :host(.kode4-drawer-left) {
                left: 0;
                right: auto;
                top: 0;
                bottom: 0;
                transition: top 0s linear 0s;
            }

            :host(.kode4-drawer-right) {
                left: auto;
                right: 0;
                top: 0;
                bottom: 0;
                transition: top 0s linear 0s;
            }

            :host(.kode4-drawer-top) {
                left: 0;
                right: 0;
                top: 0;
                bottom: auto;
                transition: left 0s linear 0s;
            }

            :host(.kode4-drawer-bottom) {
                left: 0;
                right: 0;
                top: auto;
                bottom: 0;
                transition: left 0s linear 0s;
            }

            :host(.kode4-drawer-left:not([open])),
            :host(.kode4-drawer-right:not([open])) {
                top: 1000vh;
                transition: top 0s linear 0.25s;
            }

            :host(.kode4-drawer-top:not([open])),
            :host(.kode4-drawer-bottom:not([open])) {
                left: 1000vw;
                transition: left 0s linear 0.25s;
            }

            .drawer {
                position: absolute;
                display: block;
            }

            :host(.kode4-drawer-left) .drawer {
                left: 0;
                right: auto;
                top: 0;
                bottom: 0;
            }
            
            :host(.kode4-drawer-right) .drawer {
                left: auto;
                right: 0;
                top: 0;
                bottom: 0;
            }
            
            :host(.kode4-drawer-top) .drawer {
                left: 0;
                right: 0;
                top: 0;
                bottom: auto;
                max-height: 100vh;
                overflow: hidden;
            }
            
            :host(.kode4-drawer-bottom) .drawer {
                left: 0;
                right: 0;
                top: auto;
                bottom: 0;
                max-height: 100vh;
                overflow: hidden;
            }
            
            .drawer-content {
                position: absolute;
                display: block;
                z-index: 10050;
                transition: all .25s linear;
                background-color: #fff;
            }
            
            :host(.kode4-drawer-left) .drawer-content {
                left: -100%;
                right: auto;
                top: 0;
                bottom: 0;
            }
            
            :host(.kode4-drawer-left[open]) .drawer-content {
                left: 0;
            }
            
            :host(.kode4-drawer-right) .drawer-content {
                left: auto;
                right: -100%;
                top: 0;
                bottom: 0;
            }
            
            :host(.kode4-drawer-right[open]) .drawer-content {
                right: 0;
            }
            
            :host(.kode4-drawer-top) .drawer-content {
                left: 0;
                right: 0;
                top: -100%;
                bottom: auto;
                max-height: 100vh;
                overflow: hidden;
            }
            
            :host(.kode4-drawer-top[open]) .drawer-content {
                top: 0;
            }
             
            :host(.kode4-drawer-bottom) .drawer-content {
                left: 0;
                right: 0;
                top: auto;
                bottom: -100%;
                max-height: 100vh;
                overflow: hidden;
            }
            
            :host(.kode4-drawer-bottom[open]) .drawer-content {
                bottom: 0;
            }
        `,
    ];

}
