import { LitElement, html, css } from "lit";
import {customElement, query} from "lit/decorators.js";
import Kode4Growl, {getGrowlMessenger, IKode4GrowlMessengerObserver, Kode4GrowlMessage} from "./kode4-growl";
import kode4CssTheme from "./kode4-css";

@customElement('kode4-growl-areas')
export default class Kode4GrowlAreas extends LitElement implements IKode4GrowlMessengerObserver {

    @query('kode4-growl#growlTop')
    private growlT?: Kode4Growl;

    @query('kode4-growl#growlTopLeft')
    private growlTl?: Kode4Growl;

    @query('kode4-growl#growlTopRight')
    private growlTr?: Kode4Growl;

    @query('kode4-growl#growlBottom')
    private growlB?: Kode4Growl;

    @query('kode4-growl#growlBottomLeft')
    private growlBl?: Kode4Growl;

    @query('kode4-growl#growlBottomRight')
    private growlBr?: Kode4Growl;

    connectedCallback() {
        getGrowlMessenger().observe(this);
        super.connectedCallback();
    }

    disconnectedCallback() {
        getGrowlMessenger().unobserve(this);
        super.disconnectedCallback();
    }

    public onGrowlAlert(message:Kode4GrowlMessage) {
        this.alert(message);
    }

    public alert(message:Kode4GrowlMessage) {
        switch (message.pos) {
            case 't':
            case 'top':
                this.growlT?.alert(message);
                break;

            case 'tl':
            case 'top-left':
                this.growlTl?.alert(message);
                break;

            case 'tr':
            case 'top-right':
                this.growlTr?.alert(message);
                break;

            case 'b':
            case 'bottom':
                this.growlB?.alert(message);
                break;

            case 'bl':
            case 'bottom-left':
                this.growlBl?.alert(message);
                break;

            default:
                this.growlBr?.alert(message);
        }
    }

    public render() {
        return html`
            <div>
                
            <kode4-growl id="growlTop" horizontal-align="stretch" vertical-align="top"></kode4-growl>
            <kode4-growl id="growlTopLeft" horizontal-align="left" vertical-align="top"></kode4-growl>
            <kode4-growl id="growlTopRight" horizontal-align="right" vertical-align="top"></kode4-growl>
            <kode4-growl id="growlBottom" horizontal-align="stretch" vertical-align="bottom"></kode4-growl>
            <kode4-growl id="growlBottomLeft" horizontal-align="left" vertical-align="bottom"></kode4-growl>
            <kode4-growl id="growlBottomRight" horizontal-align="right" vertical-align="bottom"></kode4-growl>
            </div>
        `;
    }

    static styles = [
        kode4CssTheme,
        css`
            kode4-growl {
                position: fixed;
                display: block;
                z-index: 12000;
            }
            
            #growlTop {
                top: 0;
                left: 0;
                right: 0;
                width: auto;
            }
            
            #growlTopLeft {
                top: 10px;
                left: 10px;
            }
            
            #growlTopRight {
                top: 10px;
                right: 10px;
            }
            
            #growlBottom {
                bottom: 0;
                left: 0;
                right: 0;
                width: auto;
            }
            
            #growlBottomLeft {
                bottom: 10px;
                left: 10px;
            }
            
            #growlBottomRight {
                bottom: 10px;
                right: 10px;
            }
        `,
    ];

}
