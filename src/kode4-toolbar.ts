import {LitElement, html, css} from "lit";
import {customElement} from "lit/decorators.js";
import kode4CssTheme from './kode4-css';

@customElement('kode4-toolbar')
export default class Kode4Toolbar extends LitElement {

    public render() {
        return html`
            <slot></slot>
        `;
    }

    static styles = [
        kode4CssTheme,
        css`
            :host {
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;
            }

            :host(.kode4-spacing) {
                gap: var(--kode4-toolbar-spacing);
            }

            ::slotted(*) {
                position: relative;
                align-self: stretch;
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;
                align-items: center;
            }

            ::slotted(.kode4-vcenter) {
                align-self: center;
            }

            ::slotted(.kode4-vcontent) {
                position: relative;
                align-self: stretch;
                display: flex;
                flex-direction: column;
                flex-wrap: nowrap;
                align-items: flex-start;
            }

            ::slotted(.kode4-stretch) {
                flex: 1 0 1px;
            }

            ::slotted(.kode4-top) {
                align-items: flex-start;
            }

            ::slotted(.kode4-bottom) {
                align-items: flex-end;
            }

            ::slotted(.kode4-right) {
                justify-content: flex-end;
            }

            ::slotted(.kode4-center) {
                justify-content: center;
            }

        `,
    ];

}
