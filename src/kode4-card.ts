import {LitElement, html, css, PropertyValues} from "lit";
import {customElement, property, query, queryAssignedElements, state} from "lit/decorators.js";

@customElement('kode4-card')
export default class Kode4Card extends LitElement {

    @query('.body')
    private bodyElement!:HTMLElement;

    @queryAssignedElements({slot: 'header'})
    private headerItems!: Array<HTMLElement>;

    @queryAssignedElements()
    private mainItems!: Array<HTMLElement>;

    @queryAssignedElements({slot: 'footer'})
    private footerItems!: Array<HTMLElement>;

    @property({type: Boolean, attribute : 'fixed-width'})
    private fixedWidth:boolean = false;

    @state()
    public get contentWidth():string {
        if (this.fixedWidth) {
            return '100%';
        }
        let maxWidth:number = this.offsetWidth;
        [...this.headerItems, ...this.mainItems, ...this.footerItems]?.forEach((el:HTMLElement) => {
            maxWidth = Math.max(maxWidth, el.offsetWidth);
        });
        return `${maxWidth}px`;
    }

    @state()
    public get contentHeight():string {
        let maxHeight:number = this.offsetHeight;
        [...this.headerItems, ...this.mainItems, ...this.footerItems]?.forEach((el:HTMLElement) => {
            maxHeight = Math.max(maxHeight, el.offsetHeight);
        });
        return `${maxHeight}px`;
    }

    private resizeContentObserver:ResizeObserver = new ResizeObserver(() => {
        this.requestUpdate();
    });

    protected firstUpdated(_changedProperties: PropertyValues) {
        super.firstUpdated(_changedProperties);
        [...this.headerItems, ...this.mainItems, ...this.footerItems]?.forEach((el:HTMLElement) => {
            this.resizeContentObserver.observe(el);
        });
    }

    public scrollBodyTo(pos:number):void {
        this.bodyElement.scrollTo(0, pos);
    }

    public onBodyScrolled():void {
        this.dispatchEvent(new CustomEvent('kode4-body-scrolled', {
            bubbles: true,
            composed: true,
            detail: {
                pos: this.bodyElement.scrollTop,
            }
        }));
    }

    connectedCallback() {
        super.connectedCallback();
    }

    disconnectedCallback() {
        this.resizeContentObserver.disconnect();
        super.disconnectedCallback();
    }

    public render() {
        return html`
            <kode4-column part="container" style="width: ${this.contentWidth};">
                <slot name="header"></slot>
                <div class="body" part="body" @scroll="${this.onBodyScrolled}">
                    <slot></slot>
                </div>
                <div>
                    <slot name="footer"></slot>
                </div>
            </kode4-column>
        `;
    }

    static styles = [
        css`
            * {
                box-sizing: border-box;
            }
            
            :host {
                display: block;
                padding: 0;
                margin: 0;
            }
            
            :host([stretch]) {
                height: 100%;
            }

            .body {
                flex: 0 1 100%;
                width: 100%;
                overflow-x: hidden;
                overflow-y: auto;
                //border: solid 2px #00f;
                //background-color: #ccc;
            }
        `,
    ];

}
