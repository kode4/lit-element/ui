// import { LitElement, html, property, customElement } from "lit";
import {LitElement, html, css} from "lit";
import { customElement } from "lit/decorators.js";
import kode4Css from "./kode4-css";
import {applyKode4Theme} from "./kode4-theme";

@customElement('kode4-alert')
export class Kode4Alert extends applyKode4Theme(LitElement) {

    constructor() {
        super();
        this.kode4ThemeType = 'bg';
    }

    render() {
        return html`
            <slot></slot>
        `;
    }

    static styles = [
        kode4Css,
        css`
            :host {
                display: block;
                padding: 10px;
            }
        `
    ]

}
