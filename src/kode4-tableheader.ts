import {LitElement, html, css} from "lit";
import {customElement, property} from "lit/decorators.js";
import kode4CssTheme from "./kode4-css";
import {faLongArrowAltDown} from "@fortawesome/free-solid-svg-icons/faLongArrowAltDown";

@customElement('kode4-tableheader')
export default class Kode4Tableheader extends LitElement {

    @property({ type: Boolean, attribute: true })
    private sortable:boolean = false;

    @property({ type:String, attribute: 'order', converter: {
        fromAttribute: (value:string):string|undefined => {
            if (!value || !['asc', 'desc'].includes(value.toLowerCase())) {
                return undefined;
            }
            return value.toLowerCase();
        },
    }})
    private order?:string;

    private handleSortingChanged(e:PointerEvent) {
        e.preventDefault();

        this.dispatchEvent(new CustomEvent('kode4-sort', {
            bubbles: true,
            composed: true,
            detail: {
                currentOrder: this.order,
            }
        }))
        // if (!this.sortable) {
        //     return;
        // }
        //
        // if (!this.order) {
        //     this.order = 'asc';
        //     return;
        // }
        //
        // this.order = this.order === 'desc' ? 'asc' : 'desc';
    }

    public render() {
        return html`
            <div class="layout ${this.sortable ? 'sortable' : undefined}" @click="${this.handleSortingChanged}">
                <div class="body">
                    <slot></slot>
                </div>
                ${this.sortable && this.order ? html`
                    <div class="sorting ${this.order}">
                        <slot name="sorting">
                            <kode4-fa-icon .icon="${faLongArrowAltDown}"></kode4-fa-icon>
                        </slot>
                    </div>
                `: undefined}
            </div>
        `;
    }

    static styles = [
        kode4CssTheme,
        css`
            * {
                box-sizing: border-box;
            }

            :host {
                display: table-cell;
                padding-top: var(--kode4-tablehead-padding-top);
                padding-right: var(--kode4-tablehead-padding-right);
                padding-bottom: var(--kode4-tablehead-padding-bottom);
                padding-left: var(--kode4-tablehead-padding-left);
                color: var(--kode4-tablehead-color);
                background-color: var(--kode4-tablehead-background);
                border-bottom: var(--kode4-tablehead-border-bottom);
            }
            
            :host(:first-child) {
                padding-left: var(--kode4-tablehead-first-padding-left);
            }

            :host(:last-child) {
                padding-right: var(--kode4-tablehead-last-padding-right);
            }
            
            .layout {
                width: 100%;
                display: inline-flex;
                flex-direction: row;
                flex-wrap: nowrap;
            }
            
            .sortable {
                cursor: pointer;
            }
            
            .body {
                flex: 1 0 1px;
            }
            
            .sorting {
                padding-right: var(--kode4-tablehead-sort-padding-left);
                padding-left: var(--kode4-tablehead-sort-padding-right);
                transition: transform .25s linear;
            }

            .desc {
                transform: rotate(-180deg);
            }
        `
    ]

}
