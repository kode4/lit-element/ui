import {LitElement, html, css} from "lit";
import {customElement} from "lit/decorators.js";
// import {ifDefined} from "lit/directives/if-defined.js";
import kode4CssTheme from "./kode4-css";

@customElement('kode4-list')
export default class Kode4List extends LitElement {

    public render() {
        return html`
            <slot></slot>
        `;
    }

    static styles = [
        kode4CssTheme,
        css`
            * {
                box-sizing: border-box;
            }

            :host {
                border: solid 1px #eee;
                display: block;
            }

            :host([inline]) {
                display: inline-block;
            }

            ::slotted(*) {
                display: list-item;
                list-style: none;
                padding: 0.5em 10px;
                cursor: pointer;
                border-bottom: solid 1px #eee;
                background-color: #fff;
            }

            ::slotted(*:hover) {
                background-color: #f0f0f0;
            }

            ::slotted(*:last-child) {
                border-bottom: none;
            }
        `,
    ];

}
