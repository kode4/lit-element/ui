import {CSSResult, LitElement} from "lit";
import {property} from "lit/decorators.js";

type Constructor<T = {}> = new (...args: any[]) => T;

export declare class IApplyKode4Theme {
    public kode4ThemeType:string;

    public primary:boolean;
    public secondary:boolean;
    public success:boolean;
    public danger:boolean;
    public warning:boolean;
    public info:boolean;

    public text:boolean;
    public textPrimary:boolean;
    public textSecondary:boolean;
    public textSuccess:boolean;
    public textDanger:boolean;
    public textWarning:boolean;
    public textInfo:boolean;

    public bg:boolean;
    public bgPrimary:boolean;
    public bgSecondary:boolean;
    public bgSuccess:boolean;
    public bgDanger:boolean;
    public bgWarning:boolean;
    public bgInfo:boolean;

    public static kode4Css:CSSResult;
}

export const applyKode4Theme = <T extends Constructor<LitElement>>(superClass: T) => {

    class ApplyKode4Theme extends superClass implements IApplyKode4Theme {

        @property({ type: String, attribute: 'theme' })
        public kode4ThemeType:string = 'text';

        private toggleCssClass(value:boolean, cssClass:string) {
            value ? this.classList.add(cssClass) : this.classList.remove(cssClass);
        }

        @property({ type: Boolean, attribute: 'primary' })
        public set primary(value:boolean) {
            this.toggleCssClass(value, `kode4-${this.kode4ThemeType}-primary`);
        }

        public get primary():boolean {
            return this.classList.contains(`kode4-${this.kode4ThemeType}-primary`);
        }

        @property({ type: Boolean, attribute: 'secondary' })
        public set secondary(value:boolean) {
            this.toggleCssClass(value, `kode4-${this.kode4ThemeType}-secondary`);
        }

        public get secondary():boolean {
            return this.classList.contains(`kode4-${this.kode4ThemeType}-secondary`);
        }

        @property({ type: Boolean, attribute: 'success' })
        public set success(value:boolean) {
            this.toggleCssClass(value, `kode4-${this.kode4ThemeType}-success`);
        }

        public get success():boolean {
            return this.classList.contains(`kode4-${this.kode4ThemeType}-success`);
        }

        @property({ type: Boolean, attribute: 'danger' })
        public set danger(value:boolean) {
            this.toggleCssClass(value, `kode4-${this.kode4ThemeType}-danger`);
        }

        public get danger():boolean {
            return this.classList.contains(`kode4-${this.kode4ThemeType}-danger`);
        }

        @property({ type: Boolean, attribute: 'warning' })
        public set warning(value:boolean) {
            this.toggleCssClass(value, `kode4-${this.kode4ThemeType}-warning`);
        }

        public get warning():boolean {
            return this.classList.contains(`kode4-${this.kode4ThemeType}-warning`);
        }

        @property({ type: Boolean, attribute: 'info' })
        public set info(value:boolean) {
            this.toggleCssClass(value, `kode4-${this.kode4ThemeType}-info`);
        }

        public get info():boolean {
            return this.classList.contains(`kode4-${this.kode4ThemeType}-info`);
        }

        @property({ type: Boolean, attribute: 'text' })
        public set text(value:boolean) {
            this.toggleCssClass(value, 'kode4-text');
        }

        public get text():boolean {
            return this.classList.contains('kode4-text');
        }

        @property({ type: Boolean, attribute: 'text-primary' })
        public set textPrimary(value:boolean) {
            this.toggleCssClass(value, 'kode4-text-primary');
        }

        public get textPrimary():boolean {
            return this.classList.contains('kode4-text-primary');
        }

        @property({ type: Boolean, attribute: 'text-secondary' })
        public set textSecondary(value:boolean) {
            this.toggleCssClass(value, 'kode4-text-secondary');
        }

        public get textSecondary():boolean {
            return this.classList.contains('kode4-text-secondary');
        }

        @property({ type: Boolean, attribute: 'text-success' })
        public set textSuccess(value:boolean) {
            this.toggleCssClass(value, 'kode4-text-success');
        }

        public get textSuccess():boolean {
            return this.classList.contains('kode4-text-success');
        }

        @property({ type: Boolean, attribute: 'text-danger' })
        public set textDanger(value:boolean) {
            this.toggleCssClass(value, 'kode4-text-danger');
        }

        public get textDanger():boolean {
            return this.classList.contains('kode4-text-danger');
        }

        @property({ type: Boolean, attribute: 'text-warning' })
        public set textWarning(value:boolean) {
            this.toggleCssClass(value, 'kode4-text-warning');
        }

        public get textWarning():boolean {
            return this.classList.contains('kode4-text-warning');
        }

        @property({ type: Boolean, attribute: 'text-info' })
        public set textInfo(value:boolean) {
            this.toggleCssClass(value, 'kode4-text-info');
        }

        public get textInfo():boolean {
            return this.classList.contains('kode4-text-info');
        }

        @property({ type: Boolean, attribute: 'bg' })
        public set bg(value:boolean) {
            this.toggleCssClass(value, 'kode4-bg');
        }

        public get bg():boolean {
            return this.classList.contains('kode4-bg');
        }

        @property({ type: Boolean, attribute: 'bg-primary' })
        public set bgPrimary(value:boolean) {
            this.toggleCssClass(value, 'kode4-bg-primary');
        }

        public get bgPrimary():boolean {
            return this.classList.contains('kode4-bg-primary');
        }

        @property({ type: Boolean, attribute: 'bg-secondary' })
        public set bgSecondary(value:boolean) {
            this.toggleCssClass(value, 'kode4-bg-secondary');
        }

        public get bgSecondary():boolean {
            return this.classList.contains('kode4-bg-secondary');
        }

        @property({ type: Boolean, attribute: 'bg-success' })
        public set bgSuccess(value:boolean) {
            this.toggleCssClass(value, 'kode4-bg-success');
        }

        public get bgSuccess():boolean {
            return this.classList.contains('kode4-bg-success');
        }

        @property({ type: Boolean, attribute: 'bg-danger' })
        public set bgDanger(value:boolean) {
            this.toggleCssClass(value, 'kode4-bg-danger');
        }

        public get bgDanger():boolean {
            return this.classList.contains('kode4-bg-danger');
        }

        @property({ type: Boolean, attribute: 'bg-warning' })
        public set bgWarning(value:boolean) {
            this.toggleCssClass(value, 'kode4-bg-warning');
        }

        public get bgWarning():boolean {
            return this.classList.contains('kode4-bg-warning');
        }

        @property({ type: Boolean, attribute: 'bg-info' })
        public set bgInfo(value:boolean) {
            this.toggleCssClass(value, 'kode4-bg-info');
        }

        public get bgInfo():boolean {
            return this.classList.contains('kode4-bg-info');
        }
    }

    // Cast return type to the superClass type passed in
    return ApplyKode4Theme as Constructor<IApplyKode4Theme> & T;
}
